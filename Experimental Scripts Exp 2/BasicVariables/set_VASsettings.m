function VASsettings = set_VASsettings

global winCenter
global debug
VASsettings.nBands      = 10;
%get the width of each of the grey bands. Calculate 5 bands on each side +
%a little margin with the width of 2 bands
if debug
VASsettings.bandWidth   = round(winCenter.y/7);   
else
    VASsettings.bandWidth   = round(winCenter.y/10);
end
VASsettings.height      = 20; 

VASsettings.colorLight      = round(1/2 * 255 * [1 1 1]);
VASsettings.colorDark        = 255 * [1 1 1];

VASsettings.labels      = {'Unsicher', 'Sicher'};
VASsettings.Question    = 'Wie sicher sind Sie?';

end