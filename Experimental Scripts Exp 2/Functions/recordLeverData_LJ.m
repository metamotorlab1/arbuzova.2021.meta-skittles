function [streamTime, leverData,lastAngle, lastTouch] = recordLeverData_LJ(leverData, leverDataIndex, dev, streamTime, visualBlock,recordResponse)
%recordLeverData: Stores the Labjack lever data in the array, smoothens the
%data
global  SkitSet;

lastTouch = nan;
[leverRead,backlog]=dev.streamRead;
dataAv=true;
while dataAv
    for read = 1 : dev.scansPerRead
        streamTime=streamTime+1/dev.StreamRate;
        
        %Define the index to store the data in such a way that, if you go over the
        %preallocated length, you start again from 1
        leverDataIndex = mod( leverDataIndex, SkitSet.leverDataPreallocateLength ) + 1;
        
        %FIRST argument has to be time stamp
        leverData( leverDataIndex, 1: 4) = [streamTime leverRead(1,read) leverRead(2,read) 0];
        t = leverData( leverDataIndex, 1 );
        
        %take the average of last 6 frames (reduce jitter)
        if leverDataIndex>5 && t>5*1/SkitSet.FrameRate && ~visualBlock
            leverData(leverDataIndex,3)= mean(leverData( leverDataIndex-5:leverDataIndex, 3 ));
        elseif t>5*1/SkitSet.FrameRate &&~visualBlock
            leverData(leverDataIndex,3)=mean([leverData( SkitSet.leverDataPreallocateLength+leverDataIndex-5:SkitSet.leverDataPreallocateLength, 3 ); leverData(1:leverDataIndex,3)]);
        end
        %only do the ball calculation in during the throwing task and not in
        %the type1+2 response
        if ~recordResponse
            update_environment_1ms_LJ;
        elseif leverRead(1,read)>dev.gripThreshold
            lastTouch = streamTime;
        end
    end
    
    if backlog<=dev.scansPerRead+2
        dataAv=false;
    else
        [leverRead,backlog]=dev.streamRead;
    end
end
lastAngle = leverData(leverDataIndex,2);
end