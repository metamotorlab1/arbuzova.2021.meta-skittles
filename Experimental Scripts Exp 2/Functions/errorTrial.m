function bool= errorTrial()
    [~,~,keys]=KbCheck();
    bool=keys(KbName('space'));
    if bool 
        WaitSecs(0.2);
    end
end