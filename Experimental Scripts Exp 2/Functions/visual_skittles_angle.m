% Visual condition: Angles version
function visual_skittles_angle(mode, feedback, aStepsize, dev)
global useDaq;
global SkitSet;
global resultDataAngle;
global visualBlock;
global status;
global block;
global win winCenter;
global leverData leverDataIndex;    %LeverDataIndex says where in the leverData array the datapoint is to be stored
global trial;
global t_trials;
global c_trials
global frameCount;
global BRP BRP_alt;                 % ball release parameters (actual and alternative)


status.task.motor=0;
status.task.visual=1;

movementData=resultDataAngle.block(block).movementData(resultDataAngle.block(block).blindTrials(:)==0); %use only movement Data from non-blind trials
v_trials=length(movementData); %number of trials is the same as the number of non-blind trials in the motor task

randOrder=randperm(v_trials);
movementData=movementData(randOrder);
% resultDataAngle.visual.movementData(block)=movementData;

resultDataAngle.visual.movementData(block).order =randOrder;

% Initialize variable for storing movement data during the visual trials.
leverDataVisual = nan(SkitSet.leverDataPreallocateLength,4);

for trial=1:v_trials
    visualBlock=1;
    %zeroSpeed=0;
    if useDaq
        movementIndex =1;
        % For recording angle during visual trials
        dev.streamStart;
        streamTime=0;
    else
        movementIndex=600-36;
    end % set start moint for display
    
    % if the trial was to short start at a point when the trial
    % started
    while movementData(v_trials).BRP.t0 + movementIndex *1/SkitSet.FrameRate <= 0.601; movementIndex=movementIndex+1; end;
    
    lastFlip=0;
    
    while status.task.visual
        Screen( 'BeginOpenGL', win );
        glClear;
         
        if ~useDaq
            if movementIndex<=length(movementData(trial).angles)
                event.Angle      = movementData(trial).angles(movementIndex);
                event.Buttons    = movementData(trial).contact(movementIndex);
                event.TimeStamps = movementData(trial).time(movementIndex);
            else
                event.TimeStamps = event.TimeStamps+1/SkitSet.FrameRate;
            end
            get_leverData_1ms_mouse_Angle(event);
            movementIndex=movementIndex+1;
             
        else
            if movementIndex<=length(movementData(trial).angles)
                dataAvailable=1;
            else
                dataAvailable=0;
                event.TimeStamps = event.TimeStamps+timeSinceFlip;
                get_leverData_1ms_LJ_Angle(event);
            end


            while dataAvailable
                event.Angle      = movementData(trial).angles(movementIndex);
                event.Grip       = movementData(trial).contact(movementIndex);
                event.TimeStamps = movementData(trial).time(movementIndex);
                get_leverData_1ms_LJ_Angle(event);

                if movementIndex==length(movementData(trial).angles)|| movementData(trial).displayed(movementIndex)==1
                    dataAvailable=0;
                   % event.TimeStamps = event.TimeStamps+timeSinceFlip;
                end
                movementIndex=movementIndex+1;
            end
             
        end
         
        %% display the skittles scene
         
        angle = leverData(leverDataIndex, 3 );
        t     = leverData( leverDataIndex, 1 );
        
        showSkittlesSceneAngle(angle, t, 0, mode);
        Screen( 'EndOpenGL', win );
         
        % Show rendered image at next vertical retrace
         
        penultimateFlip=lastFlip;
        lastFlip=Screen( 'Flip', win, [], [], 0 );
        timeSinceFlip=lastFlip-penultimateFlip;
        frameCount=frameCount+1;

        %% Define states
        if t > BRP.t0 + SkitSet.max_fligth_time && status.ball.thrown
            %if leverData( leverDataIndex, 1 ) > BRP.t0 + SkitSet.max_fligth_time && status.ball.thrown
            status.task.visual = 0;
            status.task.type1 = 1;
            resultDataAngle.block(block).ballCollision(trial)=status.ball.hitTarget;
        end
    end
     

    %
    %% do type 1 task
    %
    if ~useDaq
        streamTime = 1; % just to avoid error message in debugging mode
    end
    
    status = getType1ResponseBigLevers(status, BRP, BRP_alt, trial, resultDataAngle.visual.type1(block).leverPosition, feedback, c_trials, dev, streamTime);
    
    % For movement recording during visual trials
    if useDaq
        [streamTime, leverDataVisual] = recordLeverData_LJ(leverDataVisual, 1, dev,streamTime, 0,1);
    end
    
        if ~resultDataAngle.visual.type1(block).errorTrials(trial) % &&  ((resultDataAngle.visual.type1(block).unequalPosthit(trial))||t_trials) %~zeroSpeed  &&
            %
            resultDataAngle.stairs(3) = staircaseTrial(1, resultDataAngle.stairs(3), resultDataAngle.visual.type1(block).correct(trial));
            resultDataAngle.stairs(3) = staircaseUpdate(1, resultDataAngle.stairs(3), -aStepsize);
        end
        
        % For recording angle during visual trials: Stop the stream
    if useDaq
        dev.streamStop;
        pause( 0.0005 )
    end
    
    
    status.task.type1 = 0;

    
    
    %% do type 2 task
    if c_trials
        status.task.type2 = 1;
        resultDataAngle = ConfidenceScaleDiscrete(win, SkitSet, winCenter, resultDataAngle, feedback, trial, block); % instead of getType2Response function from the original Skittles experiment
        
        status.task.type2 = 0;
    end
    resultDataAngle = close_trial_visual(trial, resultDataAngle, leverDataVisual);                        %reset all statuses

%     if ~(block==blocks)
        visualBlock=0;
        %
        % %     if breaksBetweenBlocks
        % %         status.task.break=1;
        % %         [breaks, pauseTime]= breakScreen(win, breaks,subjID,resultPath,feedback);
        % %         status.task.break=0;
        % %         resultDataAngle.breakTime(block)=pauseTime; % a break if the flag is set to 1 and not the last block
        % %     end
        %     status.task.motor=1;
%     end
end
end