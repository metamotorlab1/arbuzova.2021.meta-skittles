function showSkittlesSceneAngle(angle, t, hideBall, mode)

global SkitSetAngle
global GL
global useSound
global status
global t_trials

% Draw lever
if ~hideBall 
glPushMatrix;
glTranslatef( SkitSetAngle.xLever, SkitSetAngle.yLever, 0 );
glRotatef( -angle, 0.0, 0.0, 1.0 );                                         % Unlike x and y, z-axis goes into the screen. 
                                                                            % So rotating an object around it keeps the object 
                                                                                % on the plane of the screen


glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.6 0.1 0.0 0 ] );
glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, [ 0.6 0.1 0.0 0 ] );
glRectf( -SkitSetAngle.LeverLength, -SkitSetAngle.LeverWidth / 2, 0, SkitSetAngle.LeverWidth / 2 );
glPopMatrix;
end

%change cone color in blind trials   
if hideBall 
    coneColor=SkitSetAngle.coneColor(SkitSetAngle.blindTrialConeColor,:);
else
    coneColor=SkitSetAngle.coneColor(mod(SkitSetAngle.blindTrialConeColor,size(SkitSetAngle.coneColor,1))+1,:);
end;   

% Draw ball
[ x, y, status ] = ball_position( angle, t, status);
%if ~((hideBall && status.ball.thrown) || (BRP.t_posthit+BRP.t0)<t) % but not if the obstacle was hit or in the blind trials
    ball_color1=[ 0.1 0.6 0.0 0 ];
    ball_color2=[ 0.0 0.7 0.0 0 ];
% else
%     ball_color1=[ 0.0 0.0 0.0 0 ];
%     ball_color2=ball_color1;

    glPushMatrix;
%   [ x, y, status ] = ball_position( angle, t, status);
    glTranslatef( x, y, 0 );
    glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, ball_color1 );
    glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, ball_color2 );
    glutSolidSphere( SkitSetAngle.BallRadius, 100, 100 );
    glPopMatrix;
%end   


% Draw target
%if t_trials||~status.ball.thrown
if t_trials || mode == 'p'
    %if ~status.ball.thrown
    glPushMatrix;
    [ x, y, status ] = target_position( t, status );
    glTranslatef( x, y, 0 );
    glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.6 0.1 0.0 0 ] );
    glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, [ 0.7 0.0 0.0 0 ] );
    glutSolidSphere( SkitSetAngle.BallRadius, 100, 100 );
    glPopMatrix;
elseif ~status.ball.thrown
    glPushMatrix;
    [ x, y, status ] = target_position( t, status );
    glTranslatef( x, y, 0 );
    glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.6 0.1 0.0 0 ] );
    glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, [ 0.7 0.0 0.0 0 ] );
    glutSolidSphere( SkitSetAngle.BallRadius, 100, 100 );
    glPopMatrix;
end

% Draw center post
glPushMatrix;
glTranslatef( SkitSetAngle.xCenter, SkitSetAngle.yCenter, 0 );
glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, coneColor );
glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, coneColor );

glutSolidCone( SkitSetAngle.CenterRadius, 0.5, 100, 100 );
glPopMatrix;



%Play sound
if useSound
    if status.ball.hitTarget == 1 && status.soundPlayed.targetCollision == 0
        PsychPortAudio( 'Start', pahandle_target, 1, 0, 1 );
        status.soundPlayed.targetCollision = 1;
    end
    if status.ball.hitPost == 1 && status.soundPlayed.centerBallCollision== 0
        PsychPortAudio( 'Start', pahandle_center, 1, 0, 1 );
        status.soundPlayed.centerBallCollision = 1;
    end
    if status.ball.posthit_target == 1 && status.soundPlayed.centerTargetCollision == 0
        PsychPortAudio( 'Start', pahandle_center, 1, 0, 1 );
        status.soundPlayed.centerTargetCollision  = 1;
    end
    if status.ball.posthit_after_collision == 1 && status.soundPlayed.centerBallCollision_2 == 0
        PsychPortAudio( 'Start', pahandle_center, 1, 0, 1 );
        status.soundPlayed.centerBallCollision_2 = 1;
    end
end
end