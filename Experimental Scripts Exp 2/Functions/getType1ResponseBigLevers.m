function [status] = getType1ResponseBigLevers(status, BRP, BRP_alt, trial, leverPosition, feedback, c_trials, dev, streamTime)
% For the SkittlesParameters: PointOfRelease:
% - no trajectories shown
% - show two levers, one with the real ball point of release, another one - with
% adjusted point of release

global SkitSetAngle
global GL
global win winCenter
global resultDataAngle
global visualBlock
global block
global leverData leverDataIndex
global SkitSet
global useDaq

t2wait = 10;

color(1,:) = [1 1 1];
color(2,:) = [1 1 1];

text_y_offset = 210; % adjust the position of the response letter keys to the position of levers
text_x_offset = 250;

Screen('BeginOpenGL', win );
glClear;

% Get the array for the lever displacement
displaceActualCoeff = round(leverPosition.actualAngle-1.5);
displaceAlternCoeff = displaceActualCoeff*(-1);

% Draw the real lever
glPushMatrix;
glTranslatef(SkitSetAngle.xLever+(SkitSetAngle.xLeverOffset*displaceActualCoeff(trial)), SkitSetAngle.yCenter-0.3, 0 );
glRotatef(-BRP.angle, 0.0, 0.0, 1.0 );
glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.6 0.1 0.0 0 ] );
glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, [ 0.6 0.1 0.0 0 ] );
glRectf( -SkitSetAngle.LeverLength*2, -SkitSetAngle.LeverWidth, 0, SkitSetAngle.LeverWidth);
glPopMatrix;

% Draw the alternative lever
glPushMatrix;
glTranslatef( SkitSetAngle.xLever+(SkitSetAngle.xLeverOffset*displaceAlternCoeff(trial)), SkitSetAngle.yCenter-0.3, 0 );
glRotatef( -BRP_alt.angle, 0.0, 0.0, 1.0 );
glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.6 0.1 0.0 0 ] );
glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, [ 0.6 0.1 0.0 0 ] );
glRectf( -SkitSetAngle.LeverLength*2, -SkitSetAngle.LeverWidth, 0, SkitSetAngle.LeverWidth);
glPopMatrix;


Screen( 'EndOpenGL', win );

%fixed mapping between letter and colour

Screen('DrawText', win, SkitSetAngle.responseKeyNames(1), winCenter.x-text_x_offset, winCenter.y+text_y_offset, 255*color(1,:)); % +620 in y if not centered
Screen('DrawText', win, SkitSetAngle.responseKeyNames(2), winCenter.x+text_x_offset-15, winCenter.y+text_y_offset, 255*color(2,:)); % +620

displayTime_2ACF = Screen( 'Flip', win, [], [], 0 );

keyIsDown = 0;
timedout = false;

keyCode=1;
while ~timedout
    % For recording the lever data before type1 response
    %read from the stream
    if useDaq
        if ~visualBlock
            [leverRead,backlog]=dev.streamRead;
            dataAv=true;
            while dataAv
                event.Angle=leverRead(2,1);
                event.Grip=leverRead(1,1);
                streamTime=streamTime+1/dev.StreamRate;
                event.TimeStamps=streamTime;
                get_leverData_1ms_LJ_Angle(event);
                
                if backlog<=dev.scansPerRead*2
                    dataAv=false;
                else
                    [leverRead,backlog]=dev.streamRead;
                end
            end
        end
        
        [keyIsDown, RTsecs, keyCode] = KbCheck(kbNum);
        if find(keyCode)==SkitSetAngle.responseKeys(1)
            break;
        end
        if find(keyCode)==SkitSetAngle.responseKeys(2)
            break;
        end
        
        if((GetSecs - displayTime_2ACF) > t2wait)
            timedout = true;
            keyIsDown = true;
            RTsecs = GetSecs; % not a real RT, but need in for further lever data extraction
            keyCode = 10; % arbitrary number
        end
    else
        [RTsecs, keyCode] = KbPressWait(kbNum);
        if find(keyCode)==SkitSetAngle.responseKeys(1)
            break;
        end
        if find(keyCode)==SkitSetAngle.responseKeys(2)
            break;
        end
    end
end


% Visual block
if visualBlock
    resultDataAngle.visual.type1(block).response{trial}    = KbName(keyCode);
    resultDataAngle.visual.type1(block).RT(trial)           = RTsecs - displayTime_2ACF;
    if ~timedout
        
        resultDataAngle.visual.type1(block).chosenLeverPosition(trial) = find(find(keyCode) == SkitSetAngle.responseKeys);
        
        if resultDataAngle.visual.type1(block).chosenLeverPosition(trial) == leverPosition.actualAngle(trial)
            resultDataAngle.visual.type1(block).correct(trial)  = 1;
        elseif resultDataAngle.visual.type1(block).chosenLeverPosition(trial) == leverPosition.alternativeAngle(trial)
            resultDataAngle.visual.type1(block).correct(trial)  = 0;
        else
            resultDataAngle.visual.type1(block).correct(trial)  = NaN;
        end
    else
        resultDataAngle.visual.type1(block).correct(trial)  = NaN; % too long trials are treated as incorrect response
        resultDataAngle.visual.type1(block).errorTrials(trial) = 1;
    end
else
    
    resultDataAngle.type1(block).response{trial}    = KbName(keyCode);
    resultDataAngle.type1(block).RT(trial)           = RTsecs - displayTime_2ACF;
    if ~timedout
        
        resultDataAngle.type1(block).chosenLeverPosition(trial) = find(find(keyCode) == SkitSetAngle.responseKeys);
        
        if resultDataAngle.type1(block).chosenLeverPosition(trial) == leverPosition.actualAngle(trial)
            resultDataAngle.type1(block).correct(trial)  = 1;
        elseif resultDataAngle.type1(block).chosenLeverPosition(trial) == leverPosition.alternativeAngle(trial)
            resultDataAngle.type1(block).correct(trial)  = 0;
        else
            resultDataAngle.type1(block).correct(trial)  = NaN;
        end
    else
        resultDataAngle.type1(block).correct(trial)  = NaN; % too long trials are treated as incorrect response
        resultDataAngle.type1(block).errorTrials(trial) = 1;
    end
    
    
end

% Give feedback if needed

if feedback && ~c_trials
    feedbackGreen = 255 * [0 1 0];  %green
    feedbackRed = 255 * [1 0 0];  %red
    
    % Show Skittles Scene again
    Screen('BeginOpenGL', win );
    glClear;
    
    % Get the array for the lever displacement
    displaceActualCoeff = round(leverPosition.actualAngle-1.5);
    displaceAlternCoeff = displaceActualCoeff*(-1);
    
    % Draw the real lever
    glPushMatrix;
    glTranslatef( SkitSetAngle.xLever+(SkitSetAngle.xLeverOffset*displaceActualCoeff(trial)), SkitSetAngle.yCenter-0.3, 0 );
    glRotatef( -BRP.angle, 0.0, 0.0, 1.0 );
    glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.6 0.1 0.0 0 ] );
    glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, [ 0.6 0.1 0.0 0 ] );
    %glRectf( -SkitSetAngle.LeverLength, -SkitSetAngle.LeverWidth / 2, 0, SkitSetAngle.LeverWidth / 2 );
    glRectf( -SkitSetAngle.LeverLength*2, -SkitSetAngle.LeverWidth, 0, SkitSetAngle.LeverWidth);
    glPopMatrix;
    
    % Draw the alternative lever
    glPushMatrix;
    glTranslatef( SkitSetAngle.xLever+(SkitSetAngle.xLeverOffset*displaceAlternCoeff(trial)), SkitSetAngle.yCenter-0.3, 0 );
    glRotatef( -BRP_alt.angle, 0.0, 0.0, 1.0 );
    glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.6 0.1 0.0 0 ] );
    glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, [ 0.6 0.1 0.0 0 ] );
    %glRectf( -SkitSetAngle.LeverLength, -SkitSetAngle.LeverWidth / 2, 0, SkitSetAngle.LeverWidth / 2 );
    glRectf( -SkitSetAngle.LeverLength*2, -SkitSetAngle.LeverWidth, 0, SkitSetAngle.LeverWidth);
    glPopMatrix;
    
    Screen( 'EndOpenGL', win );
    
    
    
    % Display feedback
    if (find(keyCode) == SkitSetAngle.responseKeys(1))
        if resultDataAngle.type1(block).correct(trial) == 1
            Screen('DrawText', win, SkitSetAngle.responseKeyNames(1), winCenter.x-text_x_offset, winCenter.y+text_y_offset,  255*feedbackGreen);
            Screen('DrawText', win, SkitSetAngle.responseKeyNames(2), winCenter.x+text_x_offset-15, winCenter.y+text_y_offset, 255*color(1,:));
        else
            Screen('DrawText', win, SkitSetAngle.responseKeyNames(1), winCenter.x-text_x_offset, winCenter.y+text_y_offset, 255*feedbackRed);
            Screen('DrawText', win, SkitSetAngle.responseKeyNames(2), winCenter.x+text_x_offset-15, winCenter.y+text_y_offset, 255*color(1,:));
        end
    elseif (find(keyCode) == SkitSetAngle.responseKeys(2))
        if resultDataAngle.type1(block).correct(trial) == 1
            Screen('DrawText', win, SkitSetAngle.responseKeyNames(1), winCenter.x-text_x_offset, winCenter.y+text_y_offset,  255*color(1,:));
            Screen('DrawText', win, SkitSetAngle.responseKeyNames(2), winCenter.x+text_x_offset-15, winCenter.y+text_y_offset, 255*feedbackGreen);
        else
            Screen('DrawText', win, SkitSetAngle.responseKeyNames(1), winCenter.x-text_x_offset, winCenter.y+text_y_offset,  255*color(1,:));
            Screen('DrawText', win, SkitSetAngle.responseKeyNames(2), winCenter.x+text_x_offset-15, winCenter.y+text_y_offset, 255*feedbackRed);
        end
    else % for the trials that are too long and don't have Type1 response
        Screen('DrawText', win, SkitSetAngle.responseKeyNames(1), winCenter.x-text_x_offset, winCenter.y+text_y_offset,  255*color(1,:));
        Screen('DrawText', win, SkitSetAngle.responseKeyNames(2), winCenter.x+text_x_offset-15, winCenter.y+text_y_offset, 255*color(1,:));
    end
    
    %draw the text again
    Screen('Flip',win);
    
    WaitSecs(.5);
end


end