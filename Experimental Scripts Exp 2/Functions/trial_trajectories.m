%% Trials loop for the Skittles Trajectories
function trial_trajectories(n_trials, mode, feedback, vStepsize, dev)
%% set globals
global useDaq;
global SkitSet;
global status;
global BRP BRP_alt;                 % ball release parameters (actual and alternative)
global leverData leverDataIndex;    %LeverDataIndex says where in the leverData array the datapoint is to be stored
global win winCenter winRect;               %window handle for PTB (necessary for e.g. showMovingBall)
global trial;
global resultDataTraj;
global t_trials;
global c_trials
global frameCount;
global block;
global elbowYcoordsPixels;

% Initialize variable for storing movement data during the visual trials.
leverDataVisual = nan(SkitSet.leverDataPreallocateLength,4);

for trial = 1 : n_trials
    %     if (resultData.targetPos(trial))
    %         SkitSet.xTarget=SkitSet.xTarget_alt1;
    %     else
    %         SkitSet.xTarget=SkitSet.xTarget_alt2;
    %     end
    %startTime = GetSecs;
    %zeroSpeed=0; % this flag indicates if the release speed was 0 (if the two trajectories point in different directions)
    
    if useDaq
        dev.streamStart;
        %streamTime=GetSecs-startTime;
        streamTime=0;
        % tDiff=0;
    else
        startTime=GetSecs;
    end
    
    status.task.motor = 1;
    
    while status.task.motor
        
        Screen( 'BeginOpenGL', win );
        glClear;
        
        if ~useDaq
            [mouse.x, mouse.y, buttons] = GetMouse(win);
            
            dx = (winRect(3)/2 - mouse.x);
            dy = (elbowYcoordsPixels - mouse.y);
            %We call these event.etc to emulate the data acquisition output
            event.Angle      = atan2d(dy,dx);
            event.Buttons    = any(buttons);
            
            streamTime =GetSecs-startTime;
            event.TimeStamps = streamTime;
            
            
            get_leverData_1ms_mouse_Traj(event);
            
            
        else
            [leverRead,backlog]=dev.streamRead;
            dataAv=true;
            while dataAv
                event.Angle=leverRead(2,1);
                event.Grip=leverRead(1,1);
                streamTime=streamTime+1/dev.StreamRate;
                event.TimeStamps=streamTime;
                % event.TimeStamps=leverRead(3,1)-tDiff;
                
                get_leverData_1ms_LJ_Traj(event);
                
                if backlog<=dev.scansPerRead*2
                    dataAv=false;
                else
                    [leverRead,backlog]=dev.streamRead;
                end
            end
        end
        
        %% display the skittles scene
        
        angle = leverData(leverDataIndex, 3 );
        t     = leverData( leverDataIndex, 1 );
        leverData(leverDataIndex,4)=1;
        
        showSkittlesSceneTraj(angle, t, resultDataTraj.block(block).blindTrials(trial), mode)
        
        Screen( 'EndOpenGL', win );
        % Text offset for 'V'
        text_x_offset = 8;
        text_y_offset = -640;
        
        % Show rendered image at next vertical retrace
        if ~resultDataTraj.block(block).blindTrials(trial)
            % Draw the V if the ball is visible
            Screen('DrawText', win, 'V', winCenter.x-text_x_offset, winCenter.y-text_y_offset, [1 1 1]);
        end
        Screen( 'Flip', win, [], [], 0 );
        frameCount=frameCount+1;
        
        %% Define states
        if t > BRP.t0 + SkitSet.max_fligth_time && status.ball.thrown
            %if leverData( leverDataIndex, 1 ) > BRP.t0 + SkitSet.max_fligth_time && status.ball.thrown
            status.task.motor = 0;
            status.task.type1 = 1;
            
            resultDataTraj.block(block).ballCollision(trial)=status.ball.hitTarget;
        end
        
    end
    
    %% do type 1 task
    %
    if mode ~= 'p'
        
        status = getType1Response(angle, t, status, BRP, BRP_alt, trial, feedback, c_trials, dev, streamTime);
        
        % For movement recording during Type1 response
        if useDaq
            [streamTime, leverDataVisual] = recordLeverData_LJ(leverDataVisual, 1, dev, streamTime, 0,1);
        end
                
        
        %TODO error trials remove!
        if ~resultDataTraj.type1(block).errorTrials(trial) &&  (~resultDataTraj.type1(block).TrajectoriesDifferentDirections(trial)||t_trials) && (~resultDataTraj.type1(block).unequalPosthit(trial) ||resultDataTraj.block(block).blindTrials(trial)|| t_trials)%~zeroSpeed &&
            %Determine which staircase you're gonna use
            resultDataTraj.stairs(resultDataTraj.type1(block).whichStair(trial)) = staircaseTrial(1, resultDataTraj.stairs(resultDataTraj.type1(block).whichStair(trial)), resultDataTraj.type1(block).correct(trial));
            resultDataTraj.stairs(resultDataTraj.type1(block).whichStair(trial)) = staircaseUpdate(1, resultDataTraj.stairs(resultDataTraj.type1(block).whichStair(trial)), -vStepsize);
        end
    end
    % Stop the angle recording after the Type1 response
    if useDaq
        dev.streamStop;
        pause( 0.0005 )
    end
    status.task.type1 = 0;
    
    
    
    %% do type 2 task
    %
    if mode ~= 'p'
        if c_trials
            status.task.type2 = 1;
            resultDataTraj = ConfidenceScaleDiscrete(win, SkitSet, winCenter, resultDataTraj, feedback, trial, block); % instead of getType2Response function from the original Skittles experiment
            
            status.task.type2 = 0;
        end
    end
    resultDataTraj = close_trial(trial, resultDataTraj, mode);                        %reset all statuses
    
    % Plot angles data after closing the trial
    %plot(resultDataTraj.block(block).movementDataExtra(trial).angles)
    
    
    
end
