function [vStepsize] = staircase_settings_skittles_traj(mode, resultPath, subjID, task_folder, blocks)

global resultDataTraj
global startingblock
global vid_or_act


%% Initialize and pseudorandomize parameters(s) for the type1 question

%will have to move the lever to realistic min and max speeds for the staircase
vDiff_max = 1.96;

if mode == 'n'
    %if ~t_trials
        if exist([resultPath filesep subjID filesep task_folder '/SkittlesTrainingResult_' subjID,'.mat'],'file')
            
            resultTrainingTraj = load([resultPath filesep subjID filesep task_folder '/SkittlesTrainingResult_' subjID,'.mat'], 'resultDataTraj');
            
            vdiff_vm_staircase = resultTrainingTraj.resultDataTraj.type1.vdiff((find(resultTrainingTraj.resultDataTraj.type1.whichStair==1)));
            vdiff_m_staircase = resultTrainingTraj.resultDataTraj.type1.vdiff((find(resultTrainingTraj.resultDataTraj.type1.whichStair==2)));
            
           
            vDiff_start_vm = abs(vdiff_vm_staircase(end));
            vDiff_start_m = abs(vdiff_m_staircase(end));
            vDiff_start_v = abs(vdiff_vm_staircase(end)); % for the first megablock, use the values of the VM condition

        else
            warning('No training data found!! Set staircase start to default. Hit space to continue.')
            KbWait;
            
            
            vDiff_start_vm = 0.76;
            vDiff_start_m = 0.76;
            vDiff_start_v = 0.76;
            
        end
%     else
%         %addpath(genpath([resultPath filesep subjID '/SkittlesResult_' subjID]));
%         if exist([resultPath filesep subjID filesep task_folder '/SkittlesResult_' subjID, '_Traj_megablock_' num2str(megablock-1), '.mat'],'file')
%             
%             resultTrainingTraj = load([resultPath filesep subjID filesep task_folder '/SkittlesResult_' subjID, '_Traj_megablock_' num2str(megablock-1), '.mat']);   
%             
%             vdiff_vm_staircase = resultTrainingTraj.resultDataTraj.type1(blocks).vdiff((find(resultTrainingTraj.resultDataTraj.type1(blocks).whichStair==1)));
%             vdiff_m_staircase = resultTrainingTraj.resultDataTraj.type1(blocks).vdiff((find(resultTrainingTraj.resultDataTraj.type1(blocks).whichStair==2)));
%             vdiff_v_staircase = resultTrainingTraj.resultDataTraj.visual.type1(blocks).vdiff;
%                         
%             vDiff_start_vm = abs(vdiff_vm_staircase(end));
%             vDiff_start_m = abs(vdiff_m_staircase(end));
%             vDiff_start_v = abs(vdiff_v_staircase(end));
%             
%         else
%             warning('No training data found!! Set staircase start to default. Hit space to continue.')
%             KbWait;
%             
%             
%             vDiff_start_vm = 0.76;
%             vDiff_start_m = 0.76;
%             vDiff_start_v = 0.76;
% 
%         end
%     end
   
    
    %This staircases the initial ball release speed (v) parameter.
    %Set up two staircases. One will start off ascending; the other
    %descending. This is to prevent hysteresis and expectation
    vStepsize = .05;
    vDiff_min = 0.01;
    
    % resultData.type1.whichStair   = resultData.blindTrials+1; %use one stairCase for blind tials the other one for non-blind trials
    
    %Generate a series of choices for the staircase and randomize them so that
    %half the trials are covered by each staircase
    % whichStair  = repmat([1;length(stairs)],n_trials/2,1);
    % randomOrder = randperm(n_trials);
    % whichStair  = whichStair(randomOrder);
    stairs_traj(1) =staircaseSetup(1,vDiff_start_vm,[vDiff_min vDiff_max], [2 1]); %vm
    stairs_traj(2) = staircaseSetup(1, vDiff_start_m, [vDiff_min vDiff_max], [2 1]); %m
    stairs_traj(3) =staircaseSetup(1,vDiff_start_v,[vDiff_min vDiff_max], [2 1]); %visual task
    
else
    vStepsize = .15;
    vDiff_min = 0.21;
    vDiff_start_vm = 0.76;
    vDiff_start_m = 0.76;
    
    stairs_traj(1) =staircaseSetup(1,vDiff_start_vm,[vDiff_min vDiff_max], [2 1]); %vm
    stairs_traj(2) = staircaseSetup(1, vDiff_start_m, [vDiff_min vDiff_max], [2 1]); %m
    stairs_traj(3) =staircaseSetup(1,vDiff_start_vm,[vDiff_min vDiff_max], [2 1]); %visual task
    
end

if startingblock==1 && strcmp(vid_or_act,'a')
    for b=1:blocks
        resultDataTraj.type1(b).whichStair   = resultDataTraj.block(b).blindTrials+1; %use one stairCase for blind tials the other one for non-blind trials
    end
    resultDataTraj.stairs = stairs_traj;
end

end 

