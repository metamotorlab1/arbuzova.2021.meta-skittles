function read_lever_angle(dev, streamTime)
global leverData leverDataIndex
global SkitSet
global visualBlock

%read from the stream
    [leverRead,backlog]=dev.streamRead;
    dataAv=true;
    while dataAv
        event.Angle=leverRead(2,1);
        event.Grip=leverRead(1,1);
        streamTime=streamTime+1/dev.StreamRate;
        event.TimeStamps=streamTime;
        % event.TimeStamps=leverRead(3,1)-tDiff;
         
        get_leverData_1ms_LJ_Angle(event);
         
        if backlog<=dev.scansPerRead*2
            dataAv=false;
        else
            [leverRead,backlog]=dev.streamRead;
        end
    end
     
    %Define the index to store the data in such a way that, if you go over the
    %preallocated length, you start again from 1
    leverDataIndex = mod( leverDataIndex, SkitSet.leverDataPreallocateLength ) + 1;
     
    %FIRST argument has to be time stamp
    leverData( leverDataIndex, 1: 3) = [event.TimeStamps event.Grip event.Angle];
    leverData( leverDataIndex, 4 ) = 0;
    t = leverData( leverDataIndex, 1 );
     
    %take the average of last 6 frames (reduce jitter)
    if leverDataIndex>5 && t>5*1/SkitSet.FrameRate && ~visualBlock
        angle = mean(leverData( leverDataIndex-5:leverDataIndex, 3 ));
        leverData(leverDataIndex,3)=angle;
    elseif t>5*1/SkitSet.FrameRate &&~visualBlock
        angle = mean([leverData( SkitSet.leverDataPreallocateLength+leverDataIndex-5:SkitSet.leverDataPreallocateLength, 3 ); leverData(1:leverDataIndex,3)]);
        %    angle = mean([leverData(mod(leverDataIndex-5, SkitSet.leverDataPreallocateLength):SkitSet.leverDataPreallocateLength, 3 ); leverData(1:leverDataIndex,3)]); %lets see if this works (if not use line above)
        leverData(leverDataIndex,3)=angle;
    else
        angle=leverData(leverDataIndex,3);
    end