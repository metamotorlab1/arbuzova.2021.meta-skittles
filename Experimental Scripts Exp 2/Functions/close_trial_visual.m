function [resultData] = close_trial_visual(trial, resultData, leverDataVisual)

%global leverData;
global BRP BRP_alt;
global status;
global BCP;
global TRP;
global block;
global SkitSet;
global visualBlock;

%save( file_name, 'BRP', 'time', 'contact', 'angles' )
%Store other stuff
resultData.visual.movementData(block).BRP(trial)     = BRP;
resultData.visual.movementData(block).BRP_alt(trial) = BRP_alt;
resultData.visual.movementData(block).BCP(trial)     = BCP;
resultData.visual.movementData(block).TRP(trial)     = TRP;

% % For recording angle during visual trials
% 
before_release = 600;
after_release = (SkitSet.max_fligth_time*1000) + round(resultData.visual.type1(block).RT(trial)*1000) + 100; % Ball flying time + RT of Type1 and 100ms buffer


if ~visualBlock
    resultData.visual.block(block).movementDataExtra(trial).time       = leverDataVisual((1:before_release+after_release),1);            %TODO Not saving these - check what heiko was using them for!
    resultData.visual.block(block).movementDataExtra(trial).contact    = leverDataVisual((1:before_release+after_release),2);         % can be used for visual-only condition
    resultData.visual.block(block).movementDataExtra(trial).angles     = leverDataVisual((1:before_release+after_release),3);
end

%Reset variables
status  = set_status;
status.task.motor=0;
status.task.visual=1;

BRP     = set_BallReleaseParameters;
BRP_alt = set_BallReleaseParameters;
BCP     = set_BallCollisionParameters;
TRP     = set_TargetReleaseParameters;