% draw the VAS in PTB
function [leftEdge, rightEdge] = drawConfidenceVAS
    
    global win winCenter
    global VASsettings

    
    
    %Bands should be 4 x n matrices with the rows corresponding to: left
    %edge, top edge, right edge, bottom edge. 
    leftEdge  = winCenter.x + ( (0:(VASsettings.nBands -1)) - (VASsettings.nBands/2) ) * (VASsettings.bandWidth);
    rightEdge = winCenter.x + ( (1:(VASsettings.nBands   )) - (VASsettings.nBands/2) ) * (VASsettings.bandWidth);
    topEdge   = repmat(winCenter.y - VASsettings.height/2, 1, VASsettings.nBands); 
    lowEdge   = repmat(winCenter.y + VASsettings.height/2, 1, VASsettings.nBands);
    
    bands  = [leftEdge; topEdge; rightEdge; lowEdge]; 
    colors = repmat([VASsettings.colorDark' VASsettings.colorLight'], 1, VASsettings.nBands/2);
    
    
    Screen('FillRect', win, colors, bands);
    DrawFormattedText(win, VASsettings.Question, 'center', winCenter.y - 200, 255 * [1 1 1]);
    DrawFormattedText(win, VASsettings.labels{1}, leftEdge(1) - 50,    lowEdge(1)  + 30, 255 * [1 1 1]);
    DrawFormattedText(win, VASsettings.labels{2}, rightEdge(end) - 40, lowEdge(end)+ 30, 255 * [1 1 1]); 
    
    % Screen('Flip', win) %Don't flip here, just flip the cursor in the
    % other function
end

