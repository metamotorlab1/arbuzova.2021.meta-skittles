% Adapted from Steve Fleming's Meta_Dots experiment from his GitHub
function resultData=ConfidenceScaleDiscrete(window, SkitSet, winCenter, resultData, feedback, trial, block)
global visualBlock

curWindow = window;

center = [winCenter.x winCenter.y];

p.stim.VASwidth_inPixels = 300;
p.stim.VASoffset_inPixels = 100;
p.stim.arrowWidth_inPixels = 10;

p.times.confDuration_inSecs = 10;
p.times.confFBDuration_inSecs = 0.5;

%% Initialise VAS scale
VASwidth = p.stim.VASwidth_inPixels;
VASoffset = p.stim.VASoffset_inPixels;
arrowwidth = p.stim.arrowWidth_inPixels;
arrowheight = arrowwidth*1.2;
l = VASwidth/2;
deadline = 0;

% Collect rating
start_time = GetSecs;
secs = start_time;
steps_x = linspace(-l, l, 6);

% For version with no time pressure
keyCode=1;
while ~(find(keyCode)==SkitSet.responseKeys(3) || find(keyCode)==SkitSet.responseKeys(4) || find(keyCode)==SkitSet.responseKeys(5) || find(keyCode)==SkitSet.responseKeys(6) ...
        || find(keyCode)==SkitSet.responseKeys(7) || find(keyCode)==SkitSet.responseKeys(8) || find(keyCode)==SkitSet.responseKeys(9)) % Key 3 is for space bar
    % Draw line
    Screen('DrawLine',curWindow,[255 255 255],center(1)-VASwidth/2,center(2)+VASoffset,center(1)+VASwidth/2,center(2)+VASoffset);
    % Draw left major tick
    Screen('DrawLine',curWindow,[255 255 255],center(1)-VASwidth/2,center(2)+VASoffset+20,center(1)-VASwidth/2,center(2)+VASoffset);
    % Draw right major tick
    Screen('DrawLine',curWindow,[255 255 255],center(1)+VASwidth/2,center(2)+VASoffset+20,center(1)+VASwidth/2,center(2)+VASoffset);
    
    % % Draw minor ticks
    tickMark = center(1) + linspace(-VASwidth/2,VASwidth/2,6);
    Screen('TextSize', curWindow, 24);
    tickLabels = {'1','2','3','4','5','6'};
    
    for tick = 1:length(tickLabels)
        Screen('DrawLine',curWindow,[255 255 255],tickMark(tick),center(2)+VASoffset+10,tickMark(tick),center(2)+VASoffset);
        DrawFormattedText(curWindow,tickLabels{tick},tickMark(tick)-10,center(2)+VASoffset-30,[255 255 255]);
    end
    DrawFormattedText(curWindow,'Confidence?','center',center(2)+VASoffset+75,[255 255 255]);
    %    Screen('FillPoly',curWindow,[255 255 255],arrowPoints);
    Screen('Flip', curWindow);
    
    
    [RTsecs, keyCode]                   = KbPressWait;
    deadline = 1;
    
    
end

if deadline == 0
    conf = NaN;
    RT = NaN;
    % Draw confidence text
    DrawFormattedText(curWindow,'Too late!','center',center(2)+VASoffset+75,[255 255 255]);
    Screen('Flip', curWindow);
    pause(p.times.confFBDuration_inSecs);
    
elseif deadline == 1
    
    % if space is pressed-> error trial, abort
    if (find(keyCode) == SkitSet.responseKeys(3))
        if visualBlock
            resultData.visual.type1(block).errorTrials(trial)=1;
            conf = NaN;
            RT = NaN;
        else
            resultData.type1(block).errorTrials(trial)=1;
            conf = NaN;
            RT = NaN;
        end
        % Draw confidence text
        DrawFormattedText(curWindow,'Error trial / Falsche Eingabe','center',center(2)+VASoffset+75,[255 255 255]);
        Screen('Flip', curWindow);
        pause(p.times.confFBDuration_inSecs);
        
    else
        
        % keyCode - returns an array with 256 columns with 1 in the respective code column
        conf = find(keyCode)-29;
        RT = secs - start_time;
        
        %% Change the color of the arrow if feedback is given
        if feedback
            if resultData.type1(block).correct(trial)==1
                arrowColor = 255 * [0 1 0];  %green
            elseif resultData.type1(block).correct(trial) == 0
                arrowColor = 255 * [1 0 0];  %red
            else % for too long trials
                arrowColor = [255 255 255];
            end
        else
            arrowColor = [255 255 255];
        end
        
        %% Show confirmation arrow
        
        % Draw line
        Screen('DrawLine',curWindow,[255 255 255],center(1)-VASwidth/2,center(2)+VASoffset,center(1)+VASwidth/2,center(2)+VASoffset);
        % Draw left major tick
        Screen('DrawLine',curWindow,[255 255 255],center(1)-VASwidth/2,center(2)+VASoffset+20,center(1)-VASwidth/2,center(2)+VASoffset);
        % Draw right major tick
        Screen('DrawLine',curWindow,[255 255 255],center(1)+VASwidth/2,center(2)+VASoffset+20,center(1)+VASwidth/2,center(2)+VASoffset);
        
        % % Draw minor ticks
        tickMark = center(1) + linspace(-VASwidth/2,VASwidth/2,6);
        Screen('TextSize', curWindow, 24);
        tickLabels = {'1','2','3','4','5','6'};
        for tick = 1:length(tickLabels)
            Screen('DrawLine',curWindow,[255 255 255],tickMark(tick),center(2)+VASoffset+10,tickMark(tick),center(2)+VASoffset);
            DrawFormattedText(curWindow,tickLabels{tick},tickMark(tick)-10,center(2)+VASoffset-30,[255 255 255]);
        end
        DrawFormattedText(curWindow,'Confidence?','center',center(2)+VASoffset+75,[255 255 255]);
        
        % Show arrow
        xpos = center(1) + steps_x(conf);
        arrowPoints = [([-0.5 0 0.5]'.*arrowwidth)+xpos ([1 0 1]'.*arrowheight)+center(2)+VASoffset];
        Screen('FillPoly',curWindow,arrowColor,arrowPoints);
        Screen('Flip', curWindow);
        pause(p.times.confFBDuration_inSecs);
    end
    
    % Store the answer
    if ~visualBlock
        resultData.type2(block).RT(trial)        = RTsecs - start_time;
        resultData.type2(block).conf(trial)      = conf;
    else
        resultData.visual.type2(block).RT(trial)        = RTsecs - start_time;
        resultData.visual.type2(block).conf(trial)      = conf;
    end
    
end
end
