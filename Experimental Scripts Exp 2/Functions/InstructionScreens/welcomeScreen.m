function time= welcomeScreen()
%% function that draws the welcome screen

global win;
global t_trials;
global SkitSet;
global debug;

%Screen('Preference','TextEncodingLocale','UTF-8');
%Screen('Preference','TextEncodingLocale', 'de_DE.UTF-8')
text_size = 32;
welcomeMessage=['Welcome! If you have questions about the task, please ask the experimenter. \n'...
    'When you are ready, please press space bar to start. \n'...
    ' \n \n Herzlich wilkommen! Falls Sie noch Fragen ueber die Aufgabe haben, bitte fragen Sie den/die StudienleiterIn. \n'...
    ' \n Wenn Sie bereit sind, bitte die Leertaste druecken um zu starten.'];
% if ~t_trials
%     switch SkitSet.blindTrialConeColor
%         case 1
%             hidden = 'ROTES';
%             displayed = 'BLAUES';
%         case 2
%            hidden = 'BLAUES';
%            displayed = 'ROTES';
%         otherwise
%     end
%     welcomeMessage=[welcomeMessage 'Zum Schluss geben Sie bitte mit Hilfe der Maus an, wie sicher Sie sich bei dieser Wahl waren. Wie sicher Sie bei ihrer Auswahl waren, kann beliebig angegeben werden (sehr sicher, sehr unsicher, aber auch alles dazwischen). \n'...
%         'Ein ' displayed ' Hindernis in der Mitte zeigt an, dass die Flugbahn des gruenen Balls normal dargestellt wird. '...
%         'Ein ' hidden ' Hindernis zeigt Durchlaeufe an, bei denen nach dem Ballwurf der gruene Ball ausgeblendet wird (blinde Durchlaeufe). '];
%      
% end
if debug;wrapat= 60; vSpace=1;else;wrapat= 110;vSpace=2;end

Screen('TextSize', win, text_size); 
DrawFormattedText(win, welcomeMessage,'center','center',SkitSet.textColor, wrapat,0,0,vSpace);
%Screen('DrawText',win,welcomeMessage);
startTic=tic;
Screen( 'Flip', win, [], [], 0 );
KbWait;
time=toc(startTic);
%WaitSecs(.5);
end