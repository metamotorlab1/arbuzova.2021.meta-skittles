function pause_time = visualInstruction(win)
%Visual task instructions display 
global SkitSet;
global debug
global frameCount

frameCount=frameCount+1;

message=['Video Trials. Please press space bar to continue.\n '...
    '\n Videoversuche. Bitte die Leertaste druecken um zu fortfahren.'];
pauseTic=tic;

if debug;wrapat= 60;else;wrapat= 110;end
DrawFormattedText(win, message,'center','center',SkitSet.textColor,wrapat,0,0,2);
Screen( 'Flip', win, [], [], 0 );
KbWait;
pause_time=toc(pauseTic);
WaitSecs(.5);
end

