function [ breaks,pause_time ] = breakScreen(resultData, win,breaks,subjID,resultPath, task_folder, f_trials)
%BREAKSCREEN display 
global SkitSet;
global t_trials
global frameCount;
global debug
global winCenter;
global block;


text_size = 32;
breaks=breaks+1;
frameCount=frameCount+1;
if ~exist([resultPath filesep subjID filesep task_folder '/Savepoints'],'dir')
    mkdir([resultPath filesep subjID filesep task_folder '/Savepoints'])
end

if ~t_trials
    if f_trials
        save([resultPath filesep subjID filesep task_folder '/Savepoints/SkittlesResultFeedback_' subjID, '_break_' int2str(breaks) '.mat'], 'resultData')
    else
    save([resultPath filesep subjID filesep task_folder '/Savepoints/SkittlesResult_' subjID, '_break_' int2str(breaks) '.mat'], 'resultData')
    end
   % save(['./Results/SkittlesEverything_' subjName, '_' subjNum '.mat'])
else
    save([resultPath filesep subjID filesep task_folder '/Savepoints/SkittlesTrainingResult_' subjID, '_break_' int2str(breaks) '.mat'], 'resultData')
end
message_de = ['Pause. Aufgabenblock ' num2str(block) ' erfolgreich abgeschlossen. Es folgt die Aufgabe bei der der Ball geworfen werden muss. Bitte die Leertaste druecken zum Fortfahren...'];
message_en = ['Break. Block ' num2str(block) ' successfully completed. The following task is with throwing the ball. Please press space bar to continue...'];
pauseTic=tic;
if debug;wrapat= 60;else;wrapat= 110;end

%Screen('DrawText',win,message,winCenter.x,winCenter.y);
Screen('TextSize', win, text_size); 
DrawFormattedText(win, message_en,'center', 'center' ,SkitSet.textColor,wrapat);
DrawFormattedText(win, message_de,'center',winCenter.y+200 ,SkitSet.textColor,wrapat);
Screen( 'Flip', win, [], [], 0 );
WaitSecs(.7);
KbWait;
pause_time=toc(pauseTic);

end

