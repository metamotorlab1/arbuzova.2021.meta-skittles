
%% Init sound

    InitializePsychSound;
    
    [y, freq]       = psychwavread( ['.' filesep 'SoundFiles' filesep 'ball_collision.wav'] );
    wavedata        = y';
    wavedata        = [ wavedata; wavedata ];
    nrchannels      = 2;
    pahandle_target = PsychPortAudio( 'Open', [], [], 0, freq, nrchannels );
    PsychPortAudio( 'FillBuffer', pahandle_target, wavedata );
    
    [y, freq]       = psychwavread([ '.' filesep 'SoundFiles' filesep 'center_collision.wav' ]);
    wavedata        = y';
    wavedata        = [ wavedata; wavedata ];
    nrchannels      = 2;
    pahandle_center = PsychPortAudio( 'Open', [], [], 0, freq, nrchannels );
    PsychPortAudio( 'FillBuffer', pahandle_center, wavedata );
