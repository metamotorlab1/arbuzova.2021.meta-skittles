function get_leverData_1ms_mouse(event, resultData)
global leverData;
global leverDataIndex;
global status;
%global resultData;
global SkitSet;
global BRP BRP_alt;
global block
global visualBlock
%would be better to have trial as param but this should follow the form of
%the callback function for the DAQ. So get_leverData_etc should only depend
%on (event)
global trial

%Define the index to store the data in such a way that, if you go over the
%preallocated length, you start again from 1
leverDataIndex = mod( leverDataIndex, SkitSet.leverDataPreallocateLength ) + 1;
leverData( leverDataIndex, 1:3 ) = [event.TimeStamps, event.Buttons, event.Angle];
leverData( leverDataIndex, 4 ) = 0;

if status.ball.grabbed == 0
    if leverData( leverDataIndex, 2 ) == 1   %so if there's a button held down
        status.ball.grabbed = 1;
    end
end

if status.ball.grabbed == 1 && leverData( leverDataIndex, 2 ) == 0      % if ball was held and now is released
    status.ball.thrown = 1;
end

if status.ball.thrown == 1 && status.ball.flightPredicted == 0
    %calculate velocity by getting the angular difference over the last
    %(SkitSet.dataPointsForVelocity) datapoints (set to 6 initially)
    idx_start = leverDataIndex - SkitSet.dataPointsForVelocity;
    if idx_start < 1
        idx_start = mod( idx_start, SkitSet.leverDataPreallocateLength ) + 1;
    end
    
    %calculate distance traveled in m/sec. So we need length of circular
    %segment s. Wiki confirms that s = alpha/180 * pi * radius ->
    %distance in meters. This is exactly what this formula calculates.
    %Then, we need time and that's the second term: it calculates secs as
    %(Frames/secs)/frames needed to travel the section s
    v = pi * SkitSet.LeverLength * ( leverData( leverDataIndex, 3 ) - leverData( idx_start, 3 ) ) / 180 * SkitSet.FrameRate / SkitSet.dataPointsForVelocity;
    %Store Initial release parameters (i.e. the actual release parameters at t0)
    RP_t0.t0    = leverData( leverDataIndex, 1 );
    RP_t0.alpha = leverData( leverDataIndex, 3 );
    RP_t0.v     = v;
    RP_t0.leverDataIndex = leverDataIndex;
   
    %resultData.RP_t0 = RP_t0; <- PROBABLY UNNECCESSARY
    
    
    BRP = calculate_release_parameters(RP_t0.t0, RP_t0.alpha, RP_t0.v, RP_t0.leverDataIndex, BRP);
    
    %The first trajectory is given by BRP. Now get the second (alternative)
    %trajectory with staircased differences in the relevant parameter of
    %interest.
    if visualBlock
        resultData.visual.movementData(block).RP_t0 = RP_t0;
        directionAlternative=resultData.visual.type1(block).directionAlternative(trial);
        whichStair=3;
    else
        resultData.block(block).RP_t0 = RP_t0;
        whichStair=resultData.type1(block).whichStair(trial);
        directionAlternative=resultData.type1(block).directionAlternative(trial);
    end
    vdiff   = resultData.stairs(whichStair).Signal *directionAlternative ; %Decide if +ve or -ve difference    BRP_alt = calculate_release_parameters(RP_t0.t0, RP_t0.alpha, RP_t0.v + vdiff, RP_t0.leverDataIndex, BRP_alt);
    
    BRP_alt = calculate_release_parameters(RP_t0.t0, RP_t0.alpha, RP_t0.v + vdiff, RP_t0.leverDataIndex, BRP_alt);
    
    %     % change vdiff to the other direction in the following cases:
    %     % (1) one trajectory hits the pole, but the other does not
    %     % (2) one trajectory goes to the left, the other to the right
    %     % (3) one trajectory hit the pole after crossing behind it, the other
    %     %     hits it directly
    %
    %     if (RP_t0.v> 0&& RP_t0.v+vdiff<0)||(RP_t0.v<0 && RP_t0.v+vdiff>0)
    %      vdiff= -vdiff;
    %      BRP_alt = calculate_release_parameters(RP_t0.t0, RP_t0.alpha, RP_t0.v + vdiff, RP_t0.leverDataIndex, BRP_alt);
    %     end
    %
    if (BRP.posthitBeforeCross && ~BRP_alt.posthitBeforeCross )|| (BRP_alt.posthitBeforeCross && ~BRP.posthitBeforeCross )||(RP_t0.v> 0&& RP_t0.v+vdiff<0)||(RP_t0.v<0 && RP_t0.v+vdiff>0)
        %(BRP_alt.posthit && ~BRP_alt.posthitAfterCross && BRP.posthitAfterCross)||(BRP.posthit && ~BRP.posthitAfterCross && BRP_alt.posthitAfterCross)
        %         vdiff= -vdiff;
        %
        %         % if we still have the same problem set v to 0
        %         if (RP_t0.v> 0&& RP_t0.v+vdiff<0)||(RP_t0.v<0 && RP_t0.v+vdiff>0)
        %             if abs(RP_t0.v)> abs(vdiff)/3 % to counter cases that could get very hard due to slow release speed, trajectories in both directions have to ba accepted
        %                 vdiff=-RP_t0.v;
        %                 zeroSpeed=1;
        %             else
        if visualBlock
            resultData.visual.type1(block).TrajectoriesDifferentDirections(trial)=1;
        else
            resultData.type1(block).TrajectoriesDifferentDirections(trial)=1;
        end
        %             end
        %         end
        %         BRP_alt = calculate_release_parameters(RP_t0.t0, RP_t0.alpha, RP_t0.v + vdiff, RP_t0.leverDataIndex, BRP_alt);
    end
    
    if visualBlock
        resultData.visual.type1(block).vdiff(trial) = vdiff;
        resultData.visual.movementData(block).releaseSpeed(trial)=v;
        resultData.visual.movementData(block).releaseAngle(trial)=RP_t0.alpha;
    else
        resultData.type1(block).vdiff(trial) = vdiff;
        resultData.block(block).releaseSpeed(trial)=v;
        resultData.block(block).releaseAngle(trial)=RP_t0.alpha;
    end
    
    status.ball_released = 1;
    status.ball.flightPredicted = 1;
    
end


