function [resultData] = close_trial(trial, resultData, mode)

global leverData;
global BRP BRP_alt;
global status;
global BCP;
global TRP;
global block;
global SkitSet;


before_release = 600;
after_release = 400;

% If throwing timepoint is >600ms, take the data from the time until the
% throw
if BRP.idx0 < (before_release-1)
    tmp = leverData( [ mod( BRP.idx0 - before_release-1 , SkitSet.leverDataPreallocateLength ) : SkitSet.leverDataPreallocateLength, 1 : BRP.idx0 + after_release ], : );
elseif BRP.idx0 > (SkitSet.leverDataPreallocateLength-after_release)
    tmp = leverData( [ BRP.idx0 - before_release-1 : SkitSet.leverDataPreallocateLength, 1 : mod( BRP.idx0 + after_release, SkitSet.leverDataPreallocateLength ) ], : );
elseif BRP.idx0==(before_release-1)
    tmp=leverData([SkitSet.leverDataPreallocateLength,1:(before_release+after_release-1)],:);
else
    tmp = leverData( BRP.idx0 - (before_release-1) : BRP.idx0 + after_release, : );
end

resultData.block(block).movementData(trial).time       = tmp(:,1);            %TODO Not saving these - check what heiko was using them for!
resultData.block(block).movementData(trial).contact    = tmp(:,2);          % can be used for visual-only condition
resultData.block(block).movementData(trial).angles     = tmp(:,3);
resultData.block(block).movementData(trial).displayed  = tmp(:,4);
resultData.block(block).movementData(trial).BRP        = BRP;

%Store other stuff
resultData.block(block).BRP(trial)     = BRP;
resultData.block(block).BRP_alt(trial) = BRP_alt;
resultData.block(block).BCP(trial)     = BCP;
resultData.block(block).TRP(trial)     = TRP;

if mode ~= 'p' % don't do it in pre-training trials
    %Store extra movement information
    
    before_release = 600;
    after_release = (SkitSet.max_fligth_time*1000) + round(resultData.type1(block).RT(trial)*1000) + 100; % Ball flying time + RT of Type1 and 100ms buffer
    
    if BRP.idx0 < (before_release-1)
        tmp = leverData( [ mod( BRP.idx0 - before_release-1 , SkitSet.leverDataPreallocateLength ) : SkitSet.leverDataPreallocateLength, 1 : BRP.idx0 + after_release ], : );
    elseif BRP.idx0 > (SkitSet.leverDataPreallocateLength-after_release)
        tmp = leverData( [ BRP.idx0 - before_release-1 : SkitSet.leverDataPreallocateLength, 1 : mod( BRP.idx0 + after_release, SkitSet.leverDataPreallocateLength ) ], : );
    elseif BRP.idx0==(before_release-1)
        tmp=leverData([SkitSet.leverDataPreallocateLength,1:(before_release+after_release-1)],:);
    else
        tmp = leverData( BRP.idx0 - (before_release-1) : BRP.idx0 + after_release, : );
    end
    
    resultData.block(block).movementDataExtra(trial).time       = tmp(:,1);            %TODO Not saving these - check what heiko was using them for!
    resultData.block(block).movementDataExtra(trial).contact    = tmp(:,2);          % can be used for visual-only condition
    resultData.block(block).movementDataExtra(trial).angles     = tmp(:,3);
    
end

%Reset variables
status  = set_status;
BRP     = set_BallReleaseParameters;
BRP_alt = set_BallReleaseParameters;
BCP     = set_BallCollisionParameters;
TRP     = set_TargetReleaseParameters;

