function showSkittlesScene(angle, t, hideBall, mode)

global SkitSet
global GL
global useSound
global status
global t_trials
global BRP

% Draw lever
glPushMatrix;
glTranslatef( SkitSet.xLever, SkitSet.yLever, 0 );
glRotatef( -angle, 0.0, 0.0, 1.0 );                                         % Unlike x and y, z-axis goes into the screen. 
                                                                            % So rotating an object around it keeps the object 
                                                                                % on the plane of the screen
%change cone color in blind trials   
if hideBall
    coneColor=SkitSet.coneColor(SkitSet.blindTrialConeColor,:);
else
    coneColor=SkitSet.coneColor(mod(SkitSet.blindTrialConeColor,size(SkitSet.coneColor,1))+1,:);
end

glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.0 0.0 1.0 1 ] );
glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, [ 0.0 0.0 1.0 1 ] );
glRectf( -SkitSet.LeverLength, -SkitSet.LeverWidth / 2, 0, SkitSet.LeverWidth / 2 );
glPopMatrix;



% Draw ball
[ x, y, status ] = ball_position( angle, t, status);
if ~((hideBall && status.ball.thrown) || (BRP.t_posthit+BRP.t0)<t) % but not if the obstacle was hit or in the blind trials
    ball_color1=[ 0.1 0.6 0.0 0 ];
    ball_color2=[ 0.0 0.7 0.0 0 ];
    % else
    %     ball_color1=[ 0.0 0.0 0.0 0 ];
    %     ball_color2=ball_color1;
    
    glPushMatrix;
    [ x, y, status ] = ball_position( angle, t, status);
    glTranslatef( x, y, 0 );
    glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, ball_color1 );
    glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, ball_color2 );
    glutSolidSphere( SkitSet.BallRadius, 100, 100 );
    glPopMatrix;
end


% % Draw target
% % if t_trials||~status.ball.thrown 
% glPushMatrix;
% [ x, y, status ] = target_position( t, status );
% glTranslatef( x, y, 0 );
% glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.6 0.1 0.0 0 ] );
% glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, [ 0.7 0.0 0.0 0 ] );
% glutSolidSphere( SkitSet.BallRadius, 100, 100 );
% glPopMatrix;
% end


% Draw target
%if t_trials||~status.ball.thrown
	
if t_trials || mode == 'p'
    %if ~status.ball.thrown
    glPushMatrix;
    [ x, y, status ] = target_position( t, status );
    glTranslatef( x, y, 0 );
    glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.6 0.1 0.0 0 ] );
    glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, [ 0.7 0.0 0.0 0 ] );
    glutSolidSphere( SkitSet.BallRadius, 100, 100 );
    glPopMatrix;
elseif ~status.ball.thrown
    glPushMatrix;
    [ x, y, status ] = target_position( t, status );
    glTranslatef( x, y, 0 );
    glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.6 0.1 0.0 0 ] );
    glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, [ 0.7 0.0 0.0 0 ] );
    glutSolidSphere( SkitSet.BallRadius, 100, 100 );
    glPopMatrix;	
end

% Draw center post
glPushMatrix;
glTranslatef( SkitSet.xCenter, SkitSet.yCenter, 0 );
glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, coneColor );
glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, coneColor );

glutSolidCone( SkitSet.CenterRadius, 0.5, 100, 100 );
glPopMatrix;



%Play sound
if useSound
    if status.ball.hitTarget == 1 && status.soundPlayed.targetCollision == 0
        PsychPortAudio( 'Start', pahandle_target, 1, 0, 1 );
        status.soundPlayed.targetCollision = 1;
    end
    if status.ball.hitPost == 1 && status.soundPlayed.centerBallCollision== 0
        PsychPortAudio( 'Start', pahandle_center, 1, 0, 1 );
        status.soundPlayed.centerBallCollision = 1;
    end
    if status.ball.posthit_target == 1 && status.soundPlayed.centerTargetCollision == 0
        PsychPortAudio( 'Start', pahandle_center, 1, 0, 1 );
        status.soundPlayed.centerTargetCollision  = 1;
    end
    if status.ball.posthit_after_collision == 1 && status.soundPlayed.centerBallCollision_2 == 0
        PsychPortAudio( 'Start', pahandle_center, 1, 0, 1 );
        status.soundPlayed.centerBallCollision_2 = 1;
    end
end
end