% Script to show the target hit results

function [hits_all] = show_target_hit_result(block, n_trials)

global resultDataTraj
global resultDataAngle
global win;
global SkitSet;
global debug;

hits_all = [];

for b = 1:block
    
    BRP_Traj = resultDataTraj.block(b).BRP;
    BRP_Angle = resultDataAngle.block(b).BRP;
    
    for i=1:length(BRP_Traj)
        targetHit_Traj(i)= ~isnan(BRP_Traj(i).x_collision) && ( ~BRP_Traj(i).posthit || (BRP_Traj(i).t_posthit>BRP_Traj(i).t_collision)) && (BRP_Traj(i).t_collision < SkitSet.max_fligth_time);
    end
    
    for i=1:length(BRP_Angle)
        targetHit_Angle(i)= ~isnan(BRP_Angle(i).x_collision) && ( ~BRP_Angle(i).posthit || (BRP_Angle(i).t_posthit>BRP_Angle(i).t_collision)) && (BRP_Angle(i).t_collision < SkitSet.max_fligth_time);
    end
    
    hitssum_Traj(b) = sum(targetHit_Traj);
    hitssum_Angle(b) = sum(targetHit_Angle);
    hits_Traj_Angle(b) = hitssum_Traj(b) + hitssum_Angle(b);
end
hits_all = sum(hits_Traj_Angle);

targetHitMessage = ['Target hits: ' num2str(hits_all) ' times out of ' num2str(n_trials*block*2) '. Please press space bar to continue. \n '...
    '\n Ziel erreicht: ' num2str(hits_all) ' Mal aus ' num2str(n_trials*block*2) '. Bitte druecken Sie die Leertaste  um zu fortfahren.'];


if debug;wrapat= 60; vSpace=1;else;wrapat= 110;vSpace=2;end

DrawFormattedText(win, targetHitMessage,'center','center',SkitSet.textColor, wrapat,0,0,vSpace);

Screen( 'Flip', win, [], [], 0 );
KbWait;
WaitSecs(.7)

end