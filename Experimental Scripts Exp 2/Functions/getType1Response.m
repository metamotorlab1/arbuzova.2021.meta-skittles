function [status] = getType1Response(angle, t, status, BRP, BRP_alt, trial, feedback, c_trials, dev, streamTime)

global SkitSetTraj
global GL
global win winCenter
global resultDataTraj
global visualBlock
global block
global leverData leverDataIndex
global SkitSet
global useDaq

t2wait = 10; % time to wait before automatic proceeding to type2 task

white_col = [1 1 1];

% Color of the trajectories used to be different for easier response with
% the lever. With the button presses responses, this is unneccessary,
% therefore, the color is now white for both trajectories. 
color(1,:) = white_col; 
color(2,:) = white_col; 
color_traj = white_col;

text_x_offset = 50;
text_y_offset = 125; % adjust the position of the response letter keys to the position of levers

Screen( 'BeginOpenGL', win );
glClear;

% Draw lever
glPushMatrix;
glTranslatef( SkitSetTraj.xLever, SkitSetTraj.yLever, 0 );
glRotatef( -angle, 0.0, 0.0, 1.0 );
glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.6 0.1 0.0 0 ] );
glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, [ 0.6 0.1 0.0 0 ] );
glRectf( -SkitSetTraj.LeverLength, -SkitSetTraj.LeverWidth / 2, 0, SkitSetTraj.LeverWidth / 2 );
glPopMatrix;

% % (We don't draw the target when we show the two alternatives)

% Draw center post
% Draw center post and adapt its color to the blind trial color
if resultDataTraj.block(block).blindTrials(trial)&&~visualBlock
    coneColor=SkitSetTraj.coneColor(SkitSetTraj.blindTrialConeColor,:);
else
    coneColor=SkitSetTraj.coneColor(mod(SkitSetTraj.blindTrialConeColor,size(SkitSetTraj.coneColor,1))+1,:);
end
glPushMatrix;
glTranslatef( SkitSetTraj.xCenter, SkitSetTraj.yCenter, 0 );
glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, coneColor );
glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, coneColor );
glutSolidCone( SkitSetTraj.CenterRadius, 0.5, 100, 100 );
glPopMatrix;

% Get and draw actual ball trajectory vector
crossBehindPost = Inf;      %initialize these high: we'll search for the minimum
crossBeforePost.index_x = Inf;

if isnan(BRP.posthit) || BRP.posthit == 0||BRP.posthitAfterCross %BRP.posthit set to 0 in calculate_release_params
    crossBehindPost = BRP.crossBehindPost;
else
    [~, crossBeforePost.index_x] = min(abs(BRP.x_posthit-BRP.trajectory.x));
    [~, crossBeforePost.index_y] = min(abs(BRP.y_posthit-BRP.trajectory.y));
    
end
firstVerticalCrossing = min(crossBehindPost, crossBeforePost.index_x);

% same procedure for alternative trajectory
crossBehindPost = Inf;      %initialize these high: we'll search for the minimum
crossBeforePost.index_x = Inf;

if isnan(BRP_alt.posthit) || BRP_alt.posthit == 0||BRP_alt.posthitAfterCross %BRP.posthit set to 0 in calculate_release_params
    crossBehindPost = BRP_alt.crossBehindPost;
else
    [~, crossBeforePost.index_x] = min(abs(BRP_alt.x_posthit-BRP_alt.trajectory.x));
    [~, crossBeforePost.index_y] = min(abs(BRP_alt.y_posthit-BRP_alt.trajectory.y));
    
end
firstVerticalCrossing_alt = min(crossBehindPost, crossBeforePost.index_x);

% Real trajectory
for trajectoryPoint = 1 : 10 : firstVerticalCrossing  %length(BRP.trajectory.time) %plot trajectory every 10 points       
    glPushMatrix;
    glTranslatef( BRP.trajectory.x(trajectoryPoint), BRP.trajectory.y(trajectoryPoint), 0 );
    glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.0 0.0 0.0 0 ] ); % used to be [ 0.1 0.6 0.0 0 ], but it gives a greeninsh hue
    %     glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, [ color(colorTraces.actualTrace(trial),:) 0 ] );
    glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, color_traj);
    glutSolidSphere( SkitSetTraj.BallRadius/5, 100, 100 );
    glPopMatrix;
end

% Get and draw alternative ball trajectory vector
for trajectoryPoint = 1 : 10 : firstVerticalCrossing_alt  %plot trajectory every 10 points
    glPushMatrix;
    glTranslatef( BRP_alt.trajectory.x(trajectoryPoint), BRP_alt.trajectory.y(trajectoryPoint), 0 );
    glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.0 0.0 0.0 0 ] ); % used to be [ 0.1 0.6 0.0 0 ], but it gives a greeninsh hue
    %     glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, [ color(colorTraces.alternativeTrace(trial),:) 0 ] );
    glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, color_traj );
    glutSolidSphere( SkitSetTraj.BallRadius/5, 100, 100 );
    glPopMatrix;
end
last_angle = -angle; % Save the last angle before the type1 stuff appears to show exactly the same angle in feedback
Screen( 'EndOpenGL', win );

%fixed mapping between letter and colour. We could add stickers to keyboard
Screen('DrawText', win, SkitSetTraj.responseKeyNames(1), winCenter.x-text_x_offset, winCenter.y-text_y_offset, 255*color(1,:));
Screen('DrawText', win, SkitSetTraj.responseKeyNames(2), winCenter.x+text_x_offset, winCenter.y-text_y_offset, 255*color(2,:));


displayTime_2ACF = Screen( 'Flip', win, [], [], 0 );

keyIsDown = 0;
timedout = false;

keyCode=1;
while ~timedout
    % For recording the lever data before type1 response
    %read from the stream
    if useDaq
        if ~visualBlock
            [leverRead,backlog]=dev.streamRead;
            dataAv=true;
            while dataAv
                event.Angle=leverRead(2,1);
                event.Grip=leverRead(1,1);
                streamTime=streamTime+1/dev.StreamRate;
                event.TimeStamps=streamTime;
                get_leverData_1ms_LJ_Traj(event);
                
                if backlog<=dev.scansPerRead*2
                    dataAv=false;
                else
                    [leverRead,backlog]=dev.streamRead;
                end
            end
        end
        
        [keyIsDown, RTsecs, keyCode] = KbCheck;
        if find(keyCode)==SkitSetTraj.responseKeys(1)
            break;
        end
        if find(keyCode)==SkitSetTraj.responseKeys(2)
            break;
        end
        
        if((GetSecs - displayTime_2ACF) > t2wait)
            timedout = true;
            keyIsDown = true;
            RTsecs = GetSecs; % not a real RT, but need in for further lever data extraction
            keyCode = 10; % arbitrary number
        end
    else
        [RTsecs, keyCode] = KbPressWait;
        if find(keyCode)==SkitSetTraj.responseKeys(1)
            break;
        end
        if find(keyCode)==SkitSetTraj.responseKeys(2)
            break;
        end
    end
end

if visualBlock
    resultDataTraj.visual.type1(block).response{trial}    = KbName(keyCode);
    resultDataTraj.visual.type1(block).RT(trial)           = RTsecs - displayTime_2ACF;
    if ~timedout
        
        if ((resultDataTraj.visual.type1(block).directionAlternative(trial) == -1 && find(keyCode==1) == SkitSetTraj.responseKeys(2)) || ...
                (resultDataTraj.visual.type1(block).directionAlternative(trial) == 1 && find(keyCode==1) == SkitSetTraj.responseKeys(1)))
            resultDataTraj.visual.type1(block).correct(trial)  = 1;
        elseif ((resultDataTraj.visual.type1(block).directionAlternative(trial) == -1 && find(keyCode==1) == SkitSetTraj.responseKeys(1)) || ...
                (resultDataTraj.visual.type1(block).directionAlternative(trial) == 1 && find(keyCode==1) == SkitSetTraj.responseKeys(2)))
            resultDataTraj.visual.type1(block).correct(trial)  = 0;
        else
            resultDataTraj.visual.type1(block).correct(trial)  = NaN;
        end
    else
        resultDataTraj.visual.type1(block).correct(trial)  = NaN; 
        resultDataTraj.visual.type1(block).errorTrials(trial) = 1;
    end
    
else   
    
    resultDataTraj.type1(block).response{trial}    = KbName(keyCode);

    resultDataTraj.type1(block).RT(trial)           = RTsecs - displayTime_2ACF;
    
    if ~timedout

        if ((resultDataTraj.type1(block).directionAlternative(trial) == -1 && find(keyCode==1) == SkitSetTraj.responseKeys(2)) || ...
                (resultDataTraj.type1(block).directionAlternative(trial) == 1 && find(keyCode==1) == SkitSetTraj.responseKeys(1)))
            resultDataTraj.type1(block).correct(trial)  = 1;
        elseif ((resultDataTraj.type1(block).directionAlternative(trial) == -1 && find(keyCode==1) == SkitSetTraj.responseKeys(1)) || ...
                (resultDataTraj.type1(block).directionAlternative(trial) == 1 && find(keyCode==1) == SkitSetTraj.responseKeys(2)))
            resultDataTraj.type1(block).correct(trial)  = 0;
        else
            resultDataTraj.type1(block).correct(trial)  = NaN;
        end
    else
        resultDataTraj.type1(block).correct(trial)  = NaN; 
        resultDataTraj.type1(block).errorTrials(trial) = 1;
    end
end

% Give feedback if needed

if feedback && ~c_trials
    feedbackGreen = [0 1 0];  %green
    feedbackRed = [1 0 0];  %red
    
    % Show Skittles Scene again
    Screen('BeginOpenGL', win );
    glClear;
    
    % Get and draw the actual ball trajectory vector
    for trajectoryPoint = 1 : 10 : firstVerticalCrossing  %length(BRP.trajectory.time) %plot trajectory every 10 points
%         % For color: if directionAlternative is -1, then show the REAL trajectory in
%         % yellow, otherwise - in cyan
%         if resultDataTraj.type1(block).directionAlternative(trial) == -1
%             color_traj = color(2,:);
%         else
%             color_traj = color(1,:);
%         end
        glPushMatrix;
        glTranslatef( BRP.trajectory.x(trajectoryPoint), BRP.trajectory.y(trajectoryPoint), 0 );
        glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.0 0.0 0.0 0 ] );
        glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, color_traj);
        %         glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, [ color(colorTraces.actualTrace(trial),:) 0 ] );
        glutSolidSphere( SkitSetTraj.BallRadius/5, 100, 100 );
        glPopMatrix;
    end
    
    % Get and draw alternative ball trajectory vector
    for trajectoryPoint = 1 : 10 : firstVerticalCrossing_alt  %plot trajectory every 10 points
%         % For color: if directionAlternative is -1, then show the ALT trajectory in
%         % cyan, otherwise - in yellow
%         if resultDataTraj.type1(block).directionAlternative(trial) == -1
%             color_traj = color(1,:);
%         else
%             color_traj = color(2,:);
%         end
        glPushMatrix;
        glTranslatef( BRP_alt.trajectory.x(trajectoryPoint), BRP_alt.trajectory.y(trajectoryPoint), 0 );
        glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.0 0.0 0.0 0 ] );
        glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, color_traj);
        %         glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, [ color(colorTraces.alternativeTrace(trial),:) 0 ] );
        glutSolidSphere( SkitSetTraj.BallRadius/5, 100, 100 );
        glPopMatrix;
    end
    % Central post
    glPushMatrix;
    glTranslatef( SkitSetTraj.xCenter, SkitSetTraj.yCenter, 0 );
    glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, coneColor );
    glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, coneColor );
    glutSolidCone( SkitSetTraj.CenterRadius, 0.5, 100, 100 );
    glPopMatrix;
    
    % Draw lever
    glPushMatrix;
    glTranslatef( SkitSetTraj.xLever, SkitSetTraj.yLever, 0 );
    glRotatef( last_angle, 0.0, 0.0, 1.0 );
    glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.6 0.1 0.0 0 ] );
    glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, [ 0.6 0.1 0.0 0 ] );
    glRectf( -SkitSetTraj.LeverLength, -SkitSetTraj.LeverWidth / 2, 0, SkitSetTraj.LeverWidth / 2 );
    glPopMatrix;
    
    
    Screen( 'EndOpenGL', win );
    
    % Just with one letter displayed
    if (find(keyCode) == SkitSetTraj.responseKeys(1))
        if resultDataTraj.type1(block).correct(trial) == 1
            Screen('DrawText', win, SkitSetTraj.responseKeyNames(1), winCenter.x-text_x_offset, winCenter.y-text_y_offset, 255*feedbackGreen);
            Screen('DrawText', win, SkitSetTraj.responseKeyNames(2), winCenter.x+text_x_offset, winCenter.y-text_y_offset, 255*white_col);
        else
            Screen('DrawText', win, SkitSetTraj.responseKeyNames(1), winCenter.x-text_x_offset, winCenter.y-text_y_offset, 255*feedbackRed);
            Screen('DrawText', win, SkitSetTraj.responseKeyNames(2), winCenter.x+text_x_offset, winCenter.y-text_y_offset, 255*white_col);
        end
    elseif (find(keyCode) == SkitSetTraj.responseKeys(2))
        if resultDataTraj.type1(block).correct(trial) == 1
            Screen('DrawText', win, SkitSetTraj.responseKeyNames(1), winCenter.x-text_x_offset, winCenter.y-text_y_offset, 255*white_col);
            Screen('DrawText', win, SkitSetTraj.responseKeyNames(2), winCenter.x+text_x_offset, winCenter.y-text_y_offset, 255*feedbackGreen);
        else
            Screen('DrawText', win, SkitSetTraj.responseKeyNames(1), winCenter.x-text_x_offset, winCenter.y-text_y_offset, 255*white_col);
            Screen('DrawText', win, SkitSetTraj.responseKeyNames(2), winCenter.x+text_x_offset, winCenter.y-text_y_offset, 255*feedbackRed);
        end
    else % print both white if too slow
        Screen('DrawText', win, SkitSetTraj.responseKeyNames(1), winCenter.x-text_x_offset, winCenter.y-text_y_offset, 255*white_col);
        Screen('DrawText', win, SkitSetTraj.responseKeyNames(2), winCenter.x+text_x_offset, winCenter.y-text_y_offset, 255*white_col);
    end
    
    %draw the text again
    Screen('Flip',win);
    
        WaitSecs(.5);
    
end

end