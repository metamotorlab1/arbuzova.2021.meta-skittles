% Script to show the target hit results

function [hits_all] = show_target_hit_result_separate(resultData, block, n_trials)

global win;
global SkitSet;
global debug;

hits_sum = [];

text_size = 32;

%for b = 1:block
    
    BRP = resultData.block(block).BRP;
    
    for i=1:length(BRP)
        targetHit(i)= ~isnan(BRP(i).x_collision) && ( ~BRP(i).posthit || (BRP(i).t_posthit>BRP(i).t_collision)) && (BRP(i).t_collision < SkitSet.max_fligth_time);
    end
    

    hits_sum = sum(targetHit);

%end
hits_all = sum(hits_sum);

targetHitMessage = ['Target hits in block ' num2str(block) ': ' num2str(hits_all) ' times out of ' num2str(n_trials) '. Block ' num2str(block) ' successfully completed. Please press space bar to continue. \n '...
    '\n Ziel erreicht in Block ' num2str(block) ': ' num2str(hits_all) ' Mal aus ' num2str(n_trials) '. Aufgabenblock ' num2str(block) ' erfolgreich abgeschlossen. Bitte druecken Sie die Leertaste  um zu fortfahren.'];


if debug;wrapat= 60; vSpace=1;else;wrapat= 110;vSpace=2;end
Screen('TextSize', win, text_size); 
DrawFormattedText(win, targetHitMessage,'center','center',SkitSet.textColor, wrapat,0,0,vSpace);

Screen( 'Flip', win, [], [], 0 );
KbWait;
WaitSecs(.7);

end