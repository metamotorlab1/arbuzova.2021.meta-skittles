function inter_condition_screen(experiment)
%% function that draws the welcome screen

global win winCenter;
global SkitSet;
global debug;

if experiment == 'tr'
    next_task_en = 'Angles.';
    next_task_de = 'Winkel.';
    
elseif experiment == 'a'
    next_task_en = 'Trajectories.';
    next_task_de = 'Flugbahnen.';
end



interMessage_1_en = ['Next task: ']; 
interMessage_2_en = ['\n \n' next_task_en];  
interMessage_3_en = ['\n \n \n \n \n \n Please press space bar to continue. '];

interMessage_1_de = ['Naechste Aufgabe: ']; 
interMessage_2_de = ['\n '  next_task_de];  
interMessage_3_de = ['\n \n \n  Bitte druecken Sie die Leertaste  um zu fortfahren.'];


if debug;wrapat= 60; vSpace=1;else;wrapat= 110;vSpace=2;end

Screen('TextSize',win, 20);  
DrawFormattedText(win, interMessage_1_en,'center','center',SkitSet.textColor, wrapat,0,0,vSpace);
Screen('TextSize',win, 36);
DrawFormattedText(win, interMessage_2_en,'center','center',SkitSet.textColor, wrapat,0,0,vSpace);
Screen('TextSize',win, 20); 
DrawFormattedText(win, interMessage_3_en,'center','center',SkitSet.textColor, wrapat,0,0,vSpace);

Screen('TextSize',win, 20);  
DrawFormattedText(win, interMessage_1_de,'center', winCenter.y+200,SkitSet.textColor, wrapat,0,0,vSpace);
Screen('TextSize',win, 36);
DrawFormattedText(win, interMessage_2_de,'center',winCenter.y+200,SkitSet.textColor, wrapat,0,0,vSpace);
Screen('TextSize',win, 20); 
DrawFormattedText(win, interMessage_3_de,'center',winCenter.y+200,SkitSet.textColor, wrapat,0,0,vSpace);

Screen( 'Flip', win, [], [], 0 );
KbWait;
WaitSecs(.7);
end