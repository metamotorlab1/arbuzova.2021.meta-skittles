function megablock_screen(megablock, megablocks)
%% function that draws the welcome screen

global win;
global SkitSet;
global debug;
global resultDataTraj
global resultDataAngle

% Calculate the number of target hits across VM and M conditions




startMessage=['Block ' num2str(megablock) ' out of ' num2str(megablocks) '. \n Please press space bar to continue. \n '...
    '\n Block ' num2str(megablock) ' von ' num2str(megablocks) '. \n Bitte druecken Sie die Leertaste  um zu fortfahren.'];
     

if debug;wrapat= 60; vSpace=1;else;wrapat= 110;vSpace=2;end

DrawFormattedText(win, startMessage,'center','center',SkitSet.textColor, wrapat,0,0,vSpace);

Screen( 'Flip', win, [], [], 0 );
KbWait;
WaitSecs(2);
end