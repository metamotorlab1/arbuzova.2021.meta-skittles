
function [data] = collectVASjudge(feedback, correct)

% 'Plugin' function to display and record responses to a visual analog
% scale. 
% 
% You'll have to set some configuration parameters in the function calling
% this one. They are:
% 
% Get mouseId
% dev.mouseId = GetMouseIndices;
% dev.mouseId = dev.mouseId(1);
% 
% [dev.wnd, dev.screenRect] = Screen('OpenWindow', screen, ... your OpenScreen
% command
%
% cfg.txtCol  = 255 * [1 1 1];
% cfg.cursorCol = 255 * [0 0 0];
% dev.xCenter = (dev.screenRect(3) - dev.screenRect(1))/2 + dev.screenRect(1);
% dev.yCenter = (dev.screenRect(4) - dev.screenRect(2))/2 + dev.screenRect(2);
% 
% 
% cfg.maxConfRT           = Inf;            %secs
% cfg.VAS.bandHeight      = 20;
% cfg.VAS.stringLeft      = 'Sehr unsicher';
% cfg.VAS.stringRight     = 'Sehr sicher';
% cfg.VAS.questionString  = 'Wie sicher?';
% cfg.VAS.nBands          = 10;
% cfg.VAS.bandWidth       = round(dev.xCenter/7);     %width of each colour band in the confidence scale
% cfg.VAS.colorLight      = round(1/2 * 255 * [1 1 1]);
% cfg.VAS.colorDark       = 255 * [1 1 1];
%
% Data is returned in the cell data with the following fields:
% data.VAS.firstRT
% data.VAS.confidence
% data.VAS.conf_absolute
% data.VAS.movRT
% data.VAS.mousetrail_x
% data.VAS.moveTime
% data.VAS.mousePosOrig
% data.VAS.onset
%
% The parameter feedback should be True if feedback should be given and
% False otherwise. The parameter correct is only needed when feedback is
% True, and says if the given answer was correct.

if ~exist('feedback','var')
  feedback=0;
end

if ~exist('correct','var')
  correct=0;
end

global cfg;
global dev;

oldTextSize  = Screen('TextSize', dev.wnd);
oldTextColor = Screen('TextColor', dev.wnd);


%draw display intially to get VASrange
VASrange = drawVAS;

%VASrange  = [dev.xCenter-100, dev.xCenter+100];
initialx  = VASrange(1) + (VASrange(2) - VASrange(1)) * rand;
initialx  = round(initialx);
constanty = 0; %We hide the actual cursor. Only the '|' sign on the scale is shown

SetMouse(initialx, 0, dev.wnd, dev.mouseId);
data.VAS.mousePosOrig = (initialx - VASrange(1))/range(VASrange);
data.VAS.onset = GetSecs;


%% Initialize

counter       = 1;
mousetrail_x  = [];
movementBegun = 0;
mouseButtons  = 0;
FlushEvents;

startVAS = GetSecs;

while (GetSecs - startVAS) < cfg.maxConfRT && ~mouseButtons(1)
    
    [mouseX, ~, mouseButtons] = GetMouse;
    if any(mouseButtons(2:length(mouseButtons)))                           % This allows the subject to right-click to skip the trial
        break
    end
    
    %restrict the bounds
    if mouseX > VASrange(2)
        mouseX = VASrange(2);
    elseif mouseX < VASrange(1)
        mouseX = VASrange(1);
    end
    
    %update display
    VASrange = drawVAS;
    
    Screen('TextSize', dev.wnd, 64);                                    %draw the cursor
    TextWidth = Screen('TextBounds', dev.wnd, '|');                     %centered on the mouse x position
    correctionPixelsX = TextWidth(3)/2; %TextWidth(3) + 3;
    DrawFormattedText(dev.wnd, '|', mouseX - correctionPixelsX, dev.yCenter+20, cfg.cursorCol);
    Screen('Flip',dev.wnd);
    
    if mouseX ~= initialx && ~movementBegun
        data.VAS.firstRT = GetSecs - startVAS;
        movementBegun = 1;
    end
    
    mousetrail_x(counter)   = mouseX;
    moveTime(counter)       = GetSecs - startVAS;
    counter                 = counter + 1;
end


if mouseButtons(1)
    %internal to external extremes
    data.VAS.confidence      = (mouseX - VASrange(1))/range(VASrange);
    data.VAS.conf_absolute   = mouseX;
    data.VAS.movRT           = GetSecs - startVAS;
    
    %---------------
    % Give feedback:
    %---------------
        
    if feedback
        if correct
            feedbackColor = 255 * [0 1 0];  %green
        else
            feedbackColor = 255 * [1 0 0];  %red
        end
            
        drawVAS;
        
        Screen('TextSize', dev.wnd, 64);                                    %draw the cursor
        TextWidth = Screen('TextBounds', dev.wnd, '|');                     %centered on the mouse x position
        correctionPixelsX = TextWidth(3)/2;
        DrawFormattedText(dev.wnd, '|', mouseX - correctionPixelsX, dev.yCenter+20, feedbackColor);
        
        Screen('Flip',dev.wnd);
        WaitSecs(.5);
    end
    
    
else
    data.VAS.confidence    = NaN;
    data.VAS.conf_absolute = NaN;
    data.VAS.movRT         = NaN;
end

if ~movementBegun
    data.VAS.firstRT      = NaN;
end

data.VAS.mousetrail_x = mousetrail_x;
data.VAS.moveTime     = moveTime;
Screen('Flip', dev.wnd);
WaitSecs(.3); %this prevents double clicks from messing it all up

Screen('TextColor',dev.wnd, oldTextColor); %set back to old color and size
Screen('TextSize', dev.wnd, oldTextSize);

end


function VASrange = drawVAS

global dev;
global cfg;


%Bands should be 4 x n matrices with the rows corresponding to: left
%edge, top edge, right edge, bottom edge.
leftEdge  = dev.xCenter + ( (0:(cfg.VAS.nBands -1)) - (cfg.VAS.nBands/2) ) * (cfg.VAS.bandWidth);
rightEdge = dev.xCenter + ( (1:(cfg.VAS.nBands   )) - (cfg.VAS.nBands/2) ) * (cfg.VAS.bandWidth);
topEdge   = repmat(dev.yCenter - cfg.VAS.bandHeight/2, 1, cfg.VAS.nBands);
lowEdge   = repmat(dev.yCenter + cfg.VAS.bandHeight/2, 1, cfg.VAS.nBands);

bands  = [leftEdge; topEdge; rightEdge; lowEdge];
colors = repmat([cfg.VAS.colorDark' cfg.VAS.colorLight'], 1, cfg.VAS.nBands/2);

VASrange = [min(leftEdge) max(rightEdge)];

Screen('TextSize', dev.wnd, 24);
Screen('FillRect', dev.wnd, colors, bands);
DrawFormattedText(dev.wnd, cfg.VAS.questionString, 'center', dev.yCenter - 60, cfg.txtCol);
TextWidth = Screen('TextBounds', dev.wnd, cfg.VAS.stringRight);
DrawFormattedText(dev.wnd, cfg.VAS.stringRight, VASrange(2) - (TextWidth(3)/2), dev.yCenter + 40, cfg.txtCol);
TextWidth = Screen('TextBounds', dev.wnd, cfg.VAS.stringLeft);
DrawFormattedText(dev.wnd, cfg.VAS.stringLeft,  VASrange(1) - (TextWidth(3)/2), dev.yCenter + 40, cfg.txtCol);

end