%% SkittlesMatlab
% designed for LabJack devices (LJ)
% streaming the input (S)

clc
%% set globals
global useSound useDaq;
global SkitSet;
global status;
global BRP BRP_alt;                 % ball release parameters (actual and alternative)
global BCP;                         % ball collision parameters
global TRP;                         % target release parameters (?)
global VASsettings;
global leverData leverDataIndex;    %LeverDataIndex says where in the leverData array the datapoint is to be stored
global GL;                          %includes the openGL drawing ..stuff for PTB
global win winCenter;               %window handle for PTB (necessary for e.g. showMovingBall)
global session;                     %daq session
global trial;
global resultData;
global t_trials;
global debug;
global frameCount;
global visualBlock;
global block;
%global zeroSpeed

% TODO unequal posthit mouse

%% user
user = 'office';
%user = 'elisa';
%user='lukasHome';
if strcmp(user, 'elisa')
    homePath = '~/git/SkittlesMatlabLJ/';
    resultPath = '~/git/SkittlesMatlabLJ/SkittlesMatlab/Results';
elseif strcmp(user,'lukasHome')
    homePath='C:/Users/lukas/Documents/Uni/Arbeit/GitLab';
    resultPath = 'C:/Users/lukas/Documents/Uni/Arbeit/GitLab/SkittlesMatlab/Results';
else
    homePath = '~/Dropbox/HiWis/Lukas';
    resultPath = '~/Dropbox/Data/Skittles_Visual';
end
cd([homePath '/SkittlesMatlab'])
addpath(genpath([homePath '/SkittlesMatlab']))

%% flags
debug               = 0;
useDaq              = 1;                               %use lever through data acquisition toolbox (1) or keyboard and mouse (0)
useSound            = 0;

breaksBetweenBlocks = 1;                                % 1 yes 0 no


trials_training          = 50;  % even numbers (1 block)
trials_feedback          = 8;  % devided by 4 (1 block)
trials_per_block         = 72;   % will be devided by 4 (+50% visual trials)
blocks                   = 3;

%% Subject settings
mode  = input('Training (t), Feedback (f), normal (n) : ','s');%Test trials with no saved data
while ~(strcmp(mode,'f')||strcmp(mode,'n')||strcmp(mode,'t'))
    mode  = input('Training (t), Feedback (f), normal (n) : ','s');%Test trials with no saved data
end
subjID = input('Participants ID: ','s');

if mode=='t'
    t_trials=1;
    while exist([resultPath filesep subjID '/SkittlesTrainingResult_' subjID,'.mat'],'file')
        subjID = input('Participants ID already taken: ','s');
    end
    n_trials = trials_training;
    blocks=1;
    feedback    = 0;
    disp([int2str(n_trials*blocks) ' TRAINING trials are beginning'])
elseif  mode=='n'
    t_trials=0;
    while exist([resultPath filesep subjID '/SkittlesResult_' subjID,'.mat'],'file')
        subjID = input('Participants ID already taken: ','s');
    end
    n_trials=trials_per_block;
    disp([int2str(n_trials*1.5*blocks) ' trials are beginning'])
    feedback    = 0;
elseif  mode=='f'
    t_trials=0;
    feedback    = 1;
    blocks=1;
    while exist([resultPath filesep '/SkittlesFeedbackResult_' subjID,'.mat'],'file')
        subjID = input('Participants ID already taken: ','s');
    end
    n_trials=trials_feedback;
    disp([int2str(n_trials*1.5*blocks) ' FEEDBACK trials are beginning'])
    
end

if isempty(subjID); subjID = 'test'; end

if ~exist([resultPath filesep subjID],'dir')
    mkdir([resultPath filesep subjID])
end


%% Skittles setting

SkitSet     = set_SkitSet;                  %Set up the whole Skittles scene
status      = set_status;                   % a primitive state machine
BRP         = set_BallReleaseParameters;    % ball release (and result) parameters
BRP_alt     = set_BallReleaseParameters;    %alternative BRP has the same structure
BCP         = set_BallCollisionParameters;
TRP         = set_TargetReleaseParameters;
resultData  = set_resultData;               %do I need this global?
resultData.SkitSet = SkitSet;

% blind trials
for b=1:blocks
    if t_trials
        resultData.block(b).blindTrials       = zeros(1,n_trials);
        %     resultData.vDiff= SkitSet.t_vdiff;
    else
        %resultData.type1.blindTrials        = [zeros(1,n_trials/2),ones(1,n_trials/2)]; %array indicates the blind trials(no ball displayed); should have the same length as n_trials (perhaps n_trials is not neccessary)
        blindTrials                             = repmat([0,1],1,n_trials/2);
        resultData.block(b).blindTrials         = blindTrials(randperm(n_trials));
        resultData.visual.type1(b).errorTrials  = zeros(1,n_trials-sum(blindTrials));
        
    end
    resultData.type1(b).errorTrials = zeros(1,n_trials);%trials that are marked during type2 task as wrong selection (space)
end

%% initialize variables/arrays
%resultData.targetPos= repmat([0 1 1 0],1,n_trials/4); % target position

resultData.useDaq = useDaq;
trial             = 1;                        %Initialize
breaks            = 0;
visualBlock       = 0;

%color of cone in blind trials randomly determined
if binornd(1,0.5)
    resultData.blindTrialColor = 1;
    SkitSet.blindTrialConeColor= 1;
else
    resultData.blindTrialColor = 2;
    SkitSet.blindTrialConeColor= 2;
    
end


%% Psychtoolbox settings

KbName('UnifyKeyNames');
whichScreen  = 0;
RestrictKeysForKbCheck(SkitSet.responseKeys);
bg           = 0.65;
grey     = round(255*[bg bg bg]);
SkitSet.bg_color=grey;

if ~useDaq
    elbowYcoordsPixels = 729; %929 if winRect 0 0 1000 1000; %529 if winRect 0 0 600 600
end
if debug
    
    Screen('Preference', 'SkipSyncTests', 1); %1 or 2->without testing
    %Screen('Preference', 'ConserveVRAM', 64); %ONLY FOR WINDOWS
    debuggingRect = [0 0 800 800];
    %Find (by hand) to the y pixel (in PTB coordinates) of the lever elbow
    %Will change if size of PTB's windowRect changes
    
else
    Screen('Preference', 'SkipSyncTests', 0);
    %  Screen('Preference', 'SkipSyncTests', 1)
    debuggingRect = [];
  %  Screen('Preference', 'SkipSyncTests', 1);   %TODO make psychtoolbox work!!!!
end


%% Initialize and pseudorandomize parameters(s) for the type1 question

                            %will have to move the lever to realistic min and max speeds for the staircase
vDiff_max = 1.96;


if ~t_trials
    if exist([resultPath filesep subjID '/SkittlesTrainingResult_' subjID,'.mat'],'file')
        resultTraining=load([resultPath filesep subjID '/SkittlesTrainingResult_' subjID,'.mat']);
        vDiff_start=abs(resultTraining.resultData.type1(end).vdiff(end));
         
    else
        warning('No training data found!! Set staircase start to default. Hit space to continue.')
        KbWait;
        vDiff_start=0.76;
         
    end
    
    %This staircases the initial ball release speed (v) parameter.
    %Set up two staircases. One will start off ascending; the other
    %descending. This is to prevent hysteresis and expectation
    vStepsize = .05;
    vDiff_min = 0.01;
    % resultData.type1.whichStair   = resultData.blindTrials+1; %use one stairCase for blind tials the other one for non-blind trials
    
    %Generate a series of choices for the staircase and randomize them so that
    %half the trials are covered by each staircase
    % whichStair  = repmat([1;length(stairs)],n_trials/2,1);
    % randomOrder = randperm(n_trials);
    % whichStair  = whichStair(randomOrder);
    stairs(2) = staircaseSetup(1, vDiff_start, [vDiff_min vDiff_max], [2 1]); %blind trials
    stairs(3) =staircaseSetup(1,vDiff_start,[vDiff_min vDiff_max], [2 1]); %visual task
else
    vStepsize = .15;
    vDiff_start=0.96;
    vDiff_min = 0.21;
    %resultData.type1.v_diff=SkitSet.t_vdiff;
end


for b=1:blocks
    resultData.type1(b).whichStair   = resultData.block(b).blindTrials+1; %use one stairCase for blind tials the other one for non-blind trials
end
stairs(1) = staircaseSetup(1, vDiff_start, [vDiff_min vDiff_max], [2 1]); %non-blind trials
resultData.stairs = stairs;


%Generate also a random series of directions. Unlike with visual stimuli
%where the two stimuli have similar statuses so to speak, the two stimuli will
%have different 'names' in that one of the two trajectories is the real one
%and the other has either higher or lower value of one of the starting
%parameters.
directionAlternative = repmat([1;-1],n_trials/2,1);
for b=1:blocks
    randomOrder = randperm(n_trials);
    directionAlternative = directionAlternative(randomOrder);
    resultData.type1(b).directionAlternative   = directionAlternative;
    
    resultData.type1(b).TrajectoriesDifferentDirections=zeros(1,n_trials);
    resultData.visual.type1(b).TrajectoriesDifferentDirections=zeros(1,n_trials/2);
    
    resultData.type1(b).unequalPosthit=zeros(1,n_trials);
    resultData.visual.type1(b).unequalPosthit=zeros(1,n_trials/2);
    
    %Randomize also the colour of the two alternative trajectories
    colorTraces.actualTrace      = repmat([1;2],n_trials/2,1);
    randomOrder = randperm(n_trials);
    colorTraces.actualTrace      = colorTraces.actualTrace(randomOrder);
    colorTraces.alternativeTrace = 3 - colorTraces.actualTrace;
    resultData.type1(b).colorTraces   = colorTraces;
end

%and the same for the visual blocks
if ~t_trials
directionAlternative = repmat([1;-1],(n_trials-sum(resultData.block(1).blindTrials))/2,1);
for b=1:blocks
    randomOrder = randperm(sum(resultData.block(b).blindTrials==0));
    directionAlternative = directionAlternative(randomOrder);
    resultData.visual.type1(b).directionAlternative   = directionAlternative;
    
    %Randomize also the colour of the two alternative trajectories
    colorTraces.actualTrace      = repmat([1;2],(n_trials-sum(resultData.block(1).blindTrials))/2,1);
    randomOrder = randperm(n_trials-sum(resultData.block(1).blindTrials));
    colorTraces.actualTrace      = colorTraces.actualTrace(randomOrder);
    colorTraces.alternativeTrace = 3 - colorTraces.actualTrace;
    resultData.visual.type1(b).colorTraces   = colorTraces;
end
end

%% Init sound
if useSound
    InitializePsychSound;
    
    [y, freq]       = psychwavread( ['.' filesep 'SoundFiles' filesep 'ball_collision.wav'] );
    wavedata        = y';
    wavedata        = [ wavedata; wavedata ];
    nrchannels      = 2;
    pahandle_target = PsychPortAudio( 'Open', [], [], 0, freq, nrchannels );
    PsychPortAudio( 'FillBuffer', pahandle_target, wavedata );
    
    [y, freq]       = psychwavread([ '.' filesep 'SoundFiles' filesep 'center_collision.wav' ]);
    wavedata        = y';
    wavedata        = [ wavedata; wavedata ];
    nrchannels      = 2;
    pahandle_center = PsychPortAudio( 'Open', [], [], 0, freq, nrchannels );
    PsychPortAudio( 'FillBuffer', pahandle_center, wavedata );
end


%% Open window and specify some OpenGL settings
try 
%second argument (debuglevel) set to 0 to increase performance. Does not check after every GL call
InitializeMatlabOpenGL( [], [], [], 0 );
[win , winRect] = PsychImaging( 'OpenWindow', whichScreen, grey, debuggingRect, [], [], 0, 0 );

%Screen( 'BlendFunction', win, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' );  % we can remove this I think
Screen( 'BeginOpenGL', win );
screenAspectRatio = winRect( 4 ) / winRect( 3 );
winCenter.x = winRect(3)/2;
winCenter.y = winRect(4)/2;
glEnable( GL.LIGHTING );
glEnable( GL.LIGHT0 );
glEnable( GL.DEPTH_TEST );
glMatrixMode( GL.PROJECTION );
glLoadIdentity;
gluPerspective( 25, 1 / screenAspectRatio, 0.1, 100 );
glMatrixMode( GL.MODELVIEW );
glLoadIdentity;
%Set light source position. The original, cool-looking scene was with [ 1 2 5 0 ].
% We changed this because the hue of the lever depended on the angle: not
% good! [0 0 1 0] is boring but correct. [1 2 15 0] is a compromise that sort of works
glLightfv( GL.LIGHT0,GL.POSITION, [ 0 0 1 0 ] );
%Use gluLookAt to position the camera. Thre 9 arguments to the function
%set:
%-view point (0,0,8):     camera at the center of the scene, elevated 8 units on z
%-center (0,0,0):         a reference point towards which the camera is aimed
%-and direction (0,1,0):  indicate which direction is up.
gluLookAt( 0, 0, 8, 0, 0, 0, 0, 1, 0);
glClearColor( bg,bg,bg, 0 );
Screen( 'EndOpenGL', win );
%Screen( 'Flip', win, [], [], 0 );

%This depends on winCenter, therefore all the way down here
VASsettings = set_VASsettings;              %to draw (banded) confidence scale

if ~debug
    HideCursor();
end




%% welcome screen
welcomeScreen(); 

%% Start data acquisition

%leverDataIndex will be a counter, increased in 1 after each iteration of the get_leverData_1ms.m
%It indicates the position of the datapoint in the leverData array
leverDataIndex = 0;

session = [];  %TODO: Do we need this?

if useDaq
    dev=LJ_Handle;
    dev.scansPerRead=1;
    hz=dev.StreamRate;
    
    if dev.streamStarted %just to be sure that no stream is running
        dev.streamStop;
    end
    pause( 0.5 )
else
    event.Angle = 0; %#ok<*UNRCH>
    hz=Screen('NominalFrameRate',win);
    
    if strcmp(user, 'elisa')
        hz = 60;
    end
    
end

% Preallocate leverData. size = timepoints x 3. Columns correspond to:
%(1) time stamp;
%(2) released ball (figer lifted from sensor or mouse pressed);
%(3) lever angle
%(4) lever data used for screen (Bool)
leverData = zeros(SkitSet.leverDataPreallocateLength,4);
if not(hz==SkitSet.FrameRate)
    SkitSet.FrameRate=hz;
    disp('Attention Measuring-Rate is at:')
    disp(SkitSet.FrameRate)
end
%% PTB main loop
pause( .1 )

frameCount   = 1;
%src          = [];                                              %correct for both daq and no-daq options?
totalTime=GetSecs;


for block=1:blocks
    
    for trial = 1 : n_trials
        %     if (resultData.targetPos(trial))
        %         SkitSet.xTarget=SkitSet.xTarget_alt1;
        %     else
        %         SkitSet.xTarget=SkitSet.xTarget_alt2;
        %     end
        %startTime = GetSecs;
        %zeroSpeed=0; % this flag indicates if the release speed was 0 (if the two trajectories point in different directions)
       
        if useDaq
            dev.streamStart;
            %streamTime=GetSecs-startTime;
            streamTime=0;
            % tDiff=0;
        else
            startTime=GetSecs;
        end
        while status.task.motor
            
            Screen( 'BeginOpenGL', win );
            glClear;
            
            if ~useDaq
                [mouse.x, mouse.y, buttons] = GetMouse(win);
                
                dx = (winRect(3)/2 - mouse.x);
                dy = (elbowYcoordsPixels - mouse.y);
                %We call these event.etc to emulate the data acquisition output
                event.Angle      = atan2d(dy,dx);
                event.Buttons    = any(buttons);
                
                streamTime =GetSecs-startTime;
                event.TimeStamps = streamTime;
                get_leverData_1ms_mouse(event);
                
                
            else
                [leverRead,backlog]=dev.streamRead;
                dataAv=true;
                while dataAv
                    event.Angle=leverRead(2,1);
                    event.Grip=leverRead(1,1);
                    streamTime=streamTime+1/dev.StreamRate;
                    event.TimeStamps=streamTime;
                    % event.TimeStamps=leverRead(3,1)-tDiff;
                    
                    get_leverData_1ms_LJ(event);
                    
                    if backlog<=dev.scansPerRead*2
                        dataAv=false;
                    else
                        [leverRead,backlog]=dev.streamRead;
                    end
                end
            end
            
            %% display the skittles scene
            
            angle = leverData(leverDataIndex, 3 );
            t     = leverData( leverDataIndex, 1 );
            leverData(leverDataIndex,4)=1;
            
            showSkittlesScene(angle, t,resultData.block(block).blindTrials(trial));
            
            
            Screen( 'EndOpenGL', win );
            
            % Show rendered image at next vertical retrace
            Screen( 'Flip', win, [], [], 0 );
            frameCount=frameCount+1;
            
            %% Define states
            if t > BRP.t0 + SkitSet.max_fligth_time && status.ball.thrown
                %if leverData( leverDataIndex, 1 ) > BRP.t0 + SkitSet.max_fligth_time && status.ball.thrown
                status.task.motor = 0;
                status.task.type1 = 1;
                resultData.block(block).ballCollision(trial)=status.ball.hitTarget;
            end
            
        end
        
        pause( 0.0005 )
        %% do type 1 task
        %
        if useDaq
            status = getType1Response_1ms_LJ(status, BRP, BRP_alt, resultData.type1(block).colorTraces,dev,backlog);
        else
            status = getType1Response(angle, t, status, BRP, BRP_alt, trial, resultData.type1(block).colorTraces);
        end
        
        %TODO error trials remove!
        if ~resultData.type1(block).errorTrials(trial) &&  (~resultData.type1(block).TrajectoriesDifferentDirections(trial)||t_trials) && (~resultData.type1(block).unequalPosthit(trial) ||resultData.block(block).blindTrials(trial)|| t_trials)%~zeroSpeed &&
            %Determine which staircase you're gonna use
            resultData.stairs(resultData.type1(block).whichStair(trial)) = staircaseTrial(1, resultData.stairs(resultData.type1(block).whichStair(trial)), resultData.type1(block).correct(trial));
            resultData.stairs(resultData.type1(block).whichStair(trial)) = staircaseUpdate(1, resultData.stairs(resultData.type1(block).whichStair(trial)), -vStepsize);
        end
        status.task.type1 = 0;
        
        
        
        %% do type 2 task
        %
        if ~t_trials %|| n_trials-trial<10
            status.task.type2 = 1;
                resultData = getType2Response(resultData, trial,feedback);
            status.task.type2 = 0;
        end
        
        %  if t > BRP.t0 + SkitSet.max_fligth_time && status.ball_released  %TODO: is this if condition neccessary?!
        resultData = close_trial(trial, resultData);                        %reset all statuses
        %  end
        
    end
    
    %% Visual condition
    % the ball and lever data is shown in an video
    
    if ~t_trials
        status.task.motor=0;
        status.task.visual=1;
        
        movementData=resultData.block(block).movementData(resultData.block(block).blindTrials(:)==0); %use only movement Data from non-blind trials
        v_trials=length(movementData); %number of trials is the same as the number of non-blind trials in the motor task
        
        randOrder=randperm(v_trials);
        movementData=movementData(randOrder);
       % resultData.visual.movementData(block)=movementData;
        
       resultData.visual.movementData(block).order =randOrder;
        % show instructions for visual task
        intraBlockBreak=visualInstruction(win);
        resultData.intraBlockBreak(block)=intraBlockBreak;
        
        for trial=1:v_trials
            visualBlock=1; 
            %zeroSpeed=0;
            if useDaq;movementIndex =1; else; movementIndex=600-36; end % set start moint for display
           
            % if the trial was to short start at a point when the trial
            % started
            while movementData(v_trials).BRP.t0 + movementIndex *1/SkitSet.FrameRate <= 0.601; movementIndex=movementIndex+1; end;
            
            lastFlip=0;
            
            while status.task.visual
                Screen( 'BeginOpenGL', win );
                glClear;
                
                if ~useDaq
                    if movementIndex<=length(movementData(trial).angles)
                        event.Angle      = movementData(trial).angles(movementIndex);
                        event.Buttons    = movementData(trial).contact(movementIndex);
                        event.TimeStamps = movementData(trial).time(movementIndex);
                    else
                        event.TimeStamps = event.TimeStamps+1/SkitSet.FrameRate;
                    end
                    get_leverData_1ms_mouse(event);
                    movementIndex=movementIndex+1;
                    
                else
                    if movementIndex<=length(movementData(trial).angles)
                        dataAvailable=1;
                    else
                        dataAvailable=0;
                        event.TimeStamps = event.TimeStamps+timeSinceFlip;
                        get_leverData_1ms_LJ(event);
                    end
                    while dataAvailable
                        
                        event.Angle      = movementData(trial).angles(movementIndex);
                        event.Grip       = movementData(trial).contact(movementIndex);
                        event.TimeStamps = movementData(trial).time(movementIndex);
                        get_leverData_1ms_LJ(event);
                        if movementIndex==length(movementData(trial).angles)|| movementData(trial).displayed(movementIndex)==1
                            dataAvailable=0;
                        end
                        movementIndex=movementIndex+1;
                    end
                    
                end
                
                %% display the skittles scene
                
                angle = leverData(leverDataIndex, 3 );
                t     = leverData( leverDataIndex, 1 );
                
                showSkittlesScene(angle, t,0);
                Screen( 'EndOpenGL', win );
                
                % Show rendered image at next vertical retrace
                
                penultimateFlip=lastFlip;
                lastFlip=Screen( 'Flip', win, [], [], 0 );
                timeSinceFlip=lastFlip-penultimateFlip;
                frameCount=frameCount+1;
                
                %% Define states
                if t > BRP.t0 + SkitSet.max_fligth_time && status.ball.thrown
                    %if leverData( leverDataIndex, 1 ) > BRP.t0 + SkitSet.max_fligth_time && status.ball.thrown
                    status.task.visual = 0;
                    status.task.type1 = 1;
                    resultData.block(block).ballCollision(trial)=status.ball.hitTarget;
                end
            end
            pause( 0.0005 )
            %% do type 1 task
            %
            if useDaq
                dev.streamStart;
                status = getType1Response_1ms_LJ(status, BRP, BRP_alt, resultData.visual.type1(block).colorTraces,dev,backlog);
            else
                status = getType1Response(angle, t, status, BRP, BRP_alt, trial, resultData.visual.type1(block).colorTraces);
            end
            
            if ~resultData.visual.type1(block).errorTrials(trial) &&  (~(resultData.visual.type1(block).TrajectoriesDifferentDirections(trial)|| resultData.visual.type1(block).unequalPosthit(trial))||t_trials)%~zeroSpeed  &&
                resultData.stairs(3) = staircaseTrial(1, resultData.stairs(3), resultData.visual.type1(block).correct(trial));
                resultData.stairs(3) = staircaseUpdate(1, resultData.stairs(3), -vStepsize);
            end
            
            status.task.type1 = 0;
            status.task.type2 = 1;
            
            
            %% do type 2 task
            %
           resultData = getType2Response(resultData, trial,feedback);
    
            status.task.type2 = 0;
            
            % if t > BRP.t0 + SkitSet.max_fligth_time && status.ball_released  %TODO: is this if condition neccessary?!
            resultData = close_trial_visual(trial, resultData);                        %reset all statuses
            %  end
            
        end
        if ~(block==blocks)
            visualBlock=0;
            
            if breaksBetweenBlocks
                status.task.break=1;
                [breaks, pauseTime]= breakScreen( win,breaks,subjID,resultPath,feedback);
                status.task.break=0;
                resultData.breakTime(block)=pauseTime; % a break if the flag is set to 1 and not the last block
            end
            status.task.motor=1;
        end
    end
end

t_total = GetSecs-totalTime;
resultData.totalTime=t_total;
%% Save


if ~t_trials
    if feedback
%         if ~exist('Results/Feedback','dir')
%             mkdir('Results/Feedback')
%         end
        save([resultPath filesep subjID '/SkittlesFeedbackResult_' subjID, '.mat'], 'resultData')
    else
    if ~exist([resultPath filesep subjID '/EverythingSkittles'],'dir')
        mkdir([resultPath filesep subjID '/EverythingSkittles'])
    end
    save([resultPath filesep subjID '/SkittlesResult_' subjID, '.mat'], 'resultData')
    save([resultPath filesep subjID '/EverythingSkittles/SkittlesEverything_' subjID, '.mat'])
    end
else
%     if ~exist('Results/Training','dir')
%         mkdir('Results/Training')
%     end
    save([resultPath filesep subjID '/SkittlesTrainingResult_' subjID,'.mat'], 'resultData')
    
    % show the results of the training
    figure('Name','Training Results')
%subplot(subplotSet.rows,subplotSet.cols,subplotSet.position); hold on
%title('staircase')
    plot(abs(resultData.type1.vdiff), ['r' '-o'])

xlabel('trial')
ylabel('v0 difference')
end
catch e
    Screen( 'CloseAll' );
    ShowCursor();
    save([resultPath filesep subjID '/SkittlesERRORTRIALS_' subjID,'.mat']);
    rethrow(e);
end
%% Clean up
Screen( 'CloseAll' );
ShowCursor();

if useDaq; dev.close; end

clearvars -global leverData

if useSound
    PsychPortAudio( 'Stop',  pahandle_target );
    PsychPortAudio( 'Close', pahandle_target );
    PsychPortAudio( 'Stop',  pahandle_center );
    PsychPortAudio( 'Close', pahandle_center );
end
disp( ['mean frames per second = ', num2str( frameCount / t_total, '%3.2f' )] )



