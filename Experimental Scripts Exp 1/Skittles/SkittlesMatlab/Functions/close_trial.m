function [resultData] = close_trial(trial, resultData)

global leverData;
global BRP BRP_alt;
global status;
global BCP;
global TRP;
global block;

%Save data
%file_name = [subject_folder, '\Sub', num2str( sub, '%04d' ), 'Trial', num2str( current_trial, '%04d' ) ];

if BRP.idx0 < 599
    tmp = leverData( [ mod( BRP.idx0 - 599 , 5000 ) : 5000, 1 : BRP.idx0 + 400 ], : );
elseif BRP.idx0 > 4600
    tmp = leverData( [ BRP.idx0 - 599 : 5000, 1 : mod( BRP.idx0 + 400, 5000 ) ], : );
elseif BRP.idx0==599
    tmp=leverData([5000,1:999],:);
else
    tmp = leverData( BRP.idx0 - 599 : BRP.idx0 + 400, : );
end

%if resultData.blindTrials(trial)
    resultData.block(block).movementData(trial).time       = tmp(:,1);            %TODO Not saving these - chack what heiko was using them for!
    resultData.block(block).movementData(trial).contact    = tmp(:,2);          % can be used for visual-only condition
    resultData.block(block).movementData(trial).angles     = tmp(:,3);
    resultData.block(block).movementData(trial).displayed  = tmp(:,4);
    resultData.block(block).movementData(trial).BRP        = BRP;
    
    %save( file_name, 'BRP', 'time', 'contact', 'angles' )
    %Store other stuff
    resultData.block(block).BRP(trial)     = BRP;
    resultData.block(block).BRP_alt(trial) = BRP_alt;
    resultData.block(block).BCP(trial)     = BCP;
    resultData.block(block).TRP(trial)     = TRP;
    
% else
%     resultData.visuoMotor.block(block).movementData(trial).time      = tmp(:,1);            %TODO Not saving these - chack what heiko was using them for!
%     resultData.visuoMotor.block(block).movementData(trial).contact   = tmp(:,2);          % can be used for visual-only condition
%     resultData.visuoMotor.block(block).movementData(trial).angles    = tmp(:,3);
%     resultData.visuoMotor.block(block).movementData(trial).displayed = tmp(:,4);
%     
%     %save( file_name, 'BRP', 'time', 'contact', 'angles' )
%     %Store other stuff
%     resultData.visuoMotor.block(block).BRP(trial)     = BRP;
%     resultData.visuoMotor.block(block).BRP_alt(trial) = BRP_alt;
%     resultData.visuoMotor.block(block).BCP(trial)     = BCP;
%     resultData.visuoMotor.block(block).TRP(trial)     = TRP;
% end

%Reset variables
status  = set_status;
BRP     = set_BallReleaseParameters;
BRP_alt = set_BallReleaseParameters;
BCP     = set_BallCollisionParameters;
TRP     = set_TargetReleaseParameters;

