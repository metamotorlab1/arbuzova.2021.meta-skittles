function pause_time = visualInstruction(win)
%Visual task instructions display 
global SkitSet;
global debug
global frameCount

frameCount=frameCount+1;

message=['Im naechsten Teil der Aufgabe sehen Sie einen kurzen Videoausschnitt, bei dem der Ball um das Hindernis geworfen wird. Teil zwei und drei der Aufgabe bleiben gleich. Erst muss eine Flugbahn ausgewaehlt werden und dann '...
    'angegeben werden wie sicher Sie sich bei der Auswahl waren. Bitte die Leertaste druecken zum Fortfahren...'];
pauseTic=tic;

%Screen('DrawText',win,message,winCenter.x,winCenter.y);
if debug;wrapat= 60;else;wrapat= 110;end
DrawFormattedText(win, message,'center','center',SkitSet.textColor,wrapat,0,0,2);
Screen( 'Flip', win, [], [], 0 );
KbWait;
pause_time=toc(pauseTic);

end

