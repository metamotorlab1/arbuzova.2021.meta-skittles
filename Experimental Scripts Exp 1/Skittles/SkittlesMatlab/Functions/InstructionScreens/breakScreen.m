function [ breaks,pause_time ] = breakScreen( win,breaks,subjID,resultPath,f_trials)
%BREAKSCREEN display 
global SkitSet;
global t_trials
global resultData; %#ok<NUSED>
global frameCount;
global debug
breaks=breaks+1;
frameCount=frameCount+1;
if ~exist([resultPath filesep subjID '/Savepoints'],'dir')
    mkdir([resultPath filesep subjID '/Savepoints'])
end

if ~t_trials
    if f_trials
        save([resultPath filesep subjID '/Savepoints/SkittlesResultFeedback_' subjID, '_break_' int2str(breaks) '.mat'], 'resultData')
    else
    save([resultPath filesep subjID '/Savepoints/SkittlesResult_' subjID, '_break_' int2str(breaks) '.mat'], 'resultData')
    end
   % save(['./Results/SkittlesEverything_' subjName, '_' subjNum '.mat'])
else
    save([resultPath filesep subjID '/Savepoints/SkittlesTrainingResult_' subjID, '_break_' int2str(breaks) '.mat'], 'resultData')
end
message='Pause. Aufgabenblock erfolgreich abgeschlossen. Es folgt die Aufgabe bei der der Ball geworfen werden muss. Bitte die Leertaste druecken zum Fortfahren...';
pauseTic=tic;
if debug;wrapat= 60;else;wrapat= 110;end
%Screen('DrawText',win,message,winCenter.x,winCenter.y);
 DrawFormattedText(win, message,'center','center',SkitSet.textColor,wrapat);
Screen( 'Flip', win, [], [], 0 );
KbWait;
pause_time=toc(pauseTic);

end

