function time= welcomeScreen()
%% function that draws the welcome screen

global win;
global t_trials;
global SkitSet;
global debug;

%Screen('Preference','TextEncodingLocale','UTF-8');
%Screen('Preference','TextEncodingLocale', 'de_DE.UTF-8')

welcomeMessage=['Herzlich willkommen! \n'... 
    'Jeder Durchlauf des Experiments besteht aus drei Teilaufgaben. Zuerst nutzen Sie bitte den Hebel der vor Ihnen auf dem Tisch befestigt ist, um den gruenen Ball aufzunehmen. '...
    'Hierfuer legen Sie bitte den Arm in die Halterung auf der Oberseite. Sobald sie mit ihrer Hand das vordere Ende des Hebels umfassen beginnt die Aufgabe und der gruene Ball erscheint am Ende des Hebels auf dem Bildschirm. '...
    'Nun soll der gruene Ball durch eine Armbewegung so rechts um das Hindernis in der Mitte des Bildschirms geworfen werden, dass er den roten Ball auf dem Bildschirm trifft. Bewegen Sie den Arm und oeffnen Sie Ihre Hand im richtigen Moment, '...
 'um den Ball los zu lassen.\n Daraufhin waehlen Sie bitte die Flugbahn aus den beiden angezeigten Alternativen aus, bei der Sie glauben, dass es die Flugbahn war, die der gruene Ball genommen hat. Wenn der Hebel in eine Richtung '...
 'bewegt wird, springt die Auswahl zwischen den beiden Alternativen hin und her. Um sich fuer eine Flugbahn zu entscheiden, muss das Ende des Hebels erneut umfasst und losgelassen werden (falls Sie aus Versehren die falsche '...
 'Flugbahn ausgewaehlt haben, druecken Sie bitte beim naechsten Schritt die Leertaste).\n '];

if ~t_trials
    switch SkitSet.blindTrialConeColor
        case 1
            hidden = 'ROTES';
            displayed = 'BLAUES';
        case 2
           hidden = 'BLAUES';
           displayed = 'ROTES';
        otherwise
    end
    welcomeMessage=[welcomeMessage 'Zum Schluss geben Sie bitte mit Hilfe der Maus an, wie sicher Sie sich bei dieser Wahl waren. Wie sicher Sie bei ihrer Auswahl waren, kann beliebig angegeben werden (sehr sicher, sehr unsicher, aber auch alles dazwischen). \n'...
        'Ein ' displayed ' Hindernis in der Mitte zeigt an, dass die Flugbahn des gruenen Balls normal dargestellt wird. '...
        'Ein ' hidden ' Hindernis zeigt Durchlaeufe an, bei denen nach dem Ballwurf der gruene Ball ausgeblendet wird (blinde Durchlaeufe). '];
     
end
welcomeMessage=[welcomeMessage '\nBitte die Leertaste druecken um zu starten.'];
if debug;wrapat= 60; vSpace=1;else;wrapat= 110;vSpace=2;end

DrawFormattedText(win, welcomeMessage,'center','center',SkitSet.textColor, wrapat,0,0,vSpace);
%Screen('DrawText',win,welcomeMessage);
startTic=tic;
Screen( 'Flip', win, [], [], 0 );
KbWait;
time=toc(startTic);

end