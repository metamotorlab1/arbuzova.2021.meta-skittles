function [ x, y, status ] = target_position( t, status )

global TRP
global SkitSet

if status.ball.hitTarget == 0
    x = SkitSet.xTarget;
    y = SkitSet.yTarget;
else
    t = t - TRP.t0;
    Damping = exp( -t * SkitSet.Damping / ( 2 * SkitSet.Mass ) );
    x = TRP.Ax * Damping * cos( SkitSet.xOmega * t + TRP.phix );
    y = TRP.Ay * Damping * cos( SkitSet.yOmega * t + TRP.phiy );
    
    if ( sqrt( ( x - SkitSet.xCenter )^2 + ( y - SkitSet.yCenter )^2 ) < ( SkitSet.CenterRadius + SkitSet.BallRadius ) ) || status.ball.posthit_target == 1 %Check for post hit
        x = TRP.x_posthit;
        y = TRP.y_posthit;
        status.ball.posthit_target = 1;
    end 
end
