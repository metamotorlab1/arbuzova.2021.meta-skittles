function  status = getType1Response_1ms_LJ(status, BRP, BRP_alt, colorTraces,dev,backlog)
global GL
global win
global SkitSet
global trial
global resultData
global frameCount;
global visualBlock;
global block;

% perhaps I can set status to global.... makes the code easier to read

color(1,:) = [0.9 0.2 0.9];
color(2,:) = [0.1 0.8 0.1];


%use predetermined colors for the rest of trials
actualTraceColor=colorTraces.actualTrace(trial);
alternativeTraceColor=colorTraces.alternativeTrace(trial);

%initialize some variables
touch=0;
firstEntry=1;
NrOfStreamReads=1;


%start stream and read first time for initial angle
%dev.streamStart;
while backlog>dev.scansPerRead
    [data,backlog]=dev.streamRead;
end
startAngle=data(2,1);

%adjust start angle (randomly), so that the selection is not jumpy in the
%beginning
if binornd(1,0.5)
    startAngleAdjusted=startAngle-0.5 * SkitSet.response1Selection;
else
    startAngleAdjusted=startAngle+0.5 * SkitSet.response1Selection;
end;


while ~touch
    while backlog>=dev.scansPerRead || firstEntry
        firstEntry=0;
        [data,backlog]=dev.streamRead;
        NrOfStreamReads=NrOfStreamReads+1;
        if sum(data(1,:)>dev.gripThreshold)>=1
            touch=1;
        end
    end
    [status,correct,~]=drawTrajectories(startAngleAdjusted, status, BRP, BRP_alt, [actualTraceColor alternativeTraceColor],color,data);
    firstEntry=1;
end

while touch
    while backlog>=dev.scansPerRead || firstEntry
        firstEntry=0;
        [data,backlog]=dev.streamRead;
        NrOfStreamReads=NrOfStreamReads+1;
        
        part=sum(data(1,:)>dev.gripThreshold);
        if part<dev.scansPerRead
            touch=0;
            RT=((NrOfStreamReads*dev.scansPerRead)+part(end))/dev.StreamRate;
        end
    end
    [status,correct,leftTrajectorySelected]=drawTrajectories(startAngleAdjusted, status, BRP, BRP_alt, [actualTraceColor alternativeTraceColor],color,data);
    firstEntry=1;
end
dev.streamStop;

%store answer
if visualBlock
    resultData.visual.type1(block).RT(trial)           = RT; %TODO: ms precission?
    resultData.visual.type1(block).correct(trial)      = correct;
    resultData.visual.type1(block).response(trial)     =leftTrajectorySelected;
    resultData.visual.type1(block).initialLeverAngle(trial)   =startAngle;
    resultData.visual.type1(block).leverAngleAdjusted(trial)  =startAngleAdjusted;
    if correct
        resultData.visual.type1(block).chosenColor(trial)=actualTraceColor;
    else
        resultData.visual.type1(block).chosenColor(trial)=alternativeTraceColor;
    end
    
% elseif resultData.blindTrials(trial)
%     resultData.motor.type1.block(block).RT(trial)           = RT; %TODO: ms precission?
%     resultData.motor.type1.block(block).correct(trial)      = correct;
%     resultData.motor.type1.block(block).response(trial)     =leftTrajectorySelected;
%     resultData.motor.type1.block(block).initialLeverAngle(trial)   =startAngle;
%     resultData.motor.type1.block(block).leverAngleAdjusted(trial)  =startAngleAdjusted;
%     if correct
%         resultData.type1.block(block).chosenColor(trial)=actualTraceColor;
%     else
%         resultData.type1.block(block).chosenColor(trial)=alternativeTraceColor;
%     end
    
else
    resultData.type1(block).RT(trial)           = RT; %TODO: ms precission?
    resultData.type1(block).correct(trial)      = correct;
    resultData.type1(block).response(trial)     =leftTrajectorySelected;
    resultData.type1(block).initialLeverAngle(trial)   =startAngle;
    resultData.type1(block).leverAngleAdjusted(trial)  =startAngleAdjusted;
    if correct
        resultData.type1(block).chosenColor(trial)=actualTraceColor;
    else
        resultData.type1(block).chosenColor(trial)=alternativeTraceColor;
    end
end




    function [status,correct,leftTrajectorySelected]=drawTrajectories(startAngle, status, BRP, BRP_alt, colorTraces,color,dataTotal)
        
        angleNow=dataTotal(2,end);
        
        Screen( 'BeginOpenGL', win );
        glClear;
        
        % Draw lever
        glPushMatrix;
        glTranslatef( SkitSet.xLever, SkitSet.yLever, 0 );
        glRotatef( -angleNow, 0.0, 0.0, 1.0 );
        glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.0 0.0 1.0 1 ] );
        glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, [ 0.0 0.0 1.0 1 ] );
        glRectf( -SkitSet.LeverLength, -SkitSet.LeverWidth / 2, 0, SkitSet.LeverWidth / 2 );
        glPopMatrix;
        
        % Draw target
        %         glPushMatrix;
        %         [ x, y, status ] = target_position( t, status );
        %         glTranslatef( x, y, 0 );
        %         glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.6 0.1 0.0 0 ] );
        %         glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, [ 0.7 0.0 0.0 0 ] );
        %         glutSolidSphere( SkitSet.BallRadius, 100, 100 );
        %         glPopMatrix;
        
        % Draw center post and adapt its color to the blind trial color
        if resultData.block(block).blindTrials(trial)&&~visualBlock
            coneColor=SkitSet.coneColor(SkitSet.blindTrialConeColor,:);
        else
            coneColor=SkitSet.coneColor(mod(SkitSet.blindTrialConeColor,size(SkitSet.coneColor,1))+1,:);
        end
        glPushMatrix;
        glTranslatef( SkitSet.xCenter, SkitSet.yCenter, 0 );
        glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, coneColor );
        glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, coneColor );
        glutSolidCone( SkitSet.CenterRadius, 0.5, 100, 100 );
        glPopMatrix;
        
        % Get and draw actual ball trajectory vector
        crossBehindPost = Inf;      %initialize these high: we'll search for the minimum
        crossBeforePost.index_x = Inf;   
        
        if isnan(BRP.posthit) || BRP.posthit == 0||BRP.posthitAfterCross %BRP.posthit set to 0 in calculate_release_params
            crossBehindPost = BRP.crossBehindPost;
        else
            [~, crossBeforePost.index_x] = min(abs(BRP.x_posthit-BRP.trajectory.x));
            [~, crossBeforePost.index_y] = min(abs(BRP.y_posthit-BRP.trajectory.y));
            
        end
        firstVerticalCrossing = min(crossBehindPost, crossBeforePost.index_x);
        
        % same procedure for alternative trajectory
        crossBehindPost = Inf;      %initialize these high: we'll search for the minimum
        crossBeforePost.index_x = Inf;
       
        if isnan(BRP_alt.posthit) || BRP_alt.posthit == 0||BRP_alt.posthitAfterCross %BRP.posthit set to 0 in calculate_release_params
            crossBehindPost = BRP_alt.crossBehindPost;
        else
            [~, crossBeforePost.index_x] = min(abs(BRP_alt.x_posthit-BRP_alt.trajectory.x));
            [~, crossBeforePost.index_y] = min(abs(BRP_alt.y_posthit-BRP_alt.trajectory.y));
            
        end
        firstVerticalCrossing_alt = min(crossBehindPost, crossBeforePost.index_x);
        
        BRPleft = BRP.trajectory.x(5)-BRP_alt.trajectory.x(5)<0; %is the original trajectory on the left side at random point 5?
        % this is to determine witch trajectory is right and witch left.
        % Therefore the x-axis location of a random point is chosen and
        % compared if it is left of the alternativa trajectory
        
        %this is a new idea: the selection of the trajectories starts at
        %the first lever position. Then the selection depends on how far
        %the lever is moved left or right and changes every e.g. 10� (specified in SkitSet.response1Selevtion)
        
        leftTrajectorySelected = sum(angleNow>startAngle+SkitSet.response1Selection*[-7:2:7] & angleNow<startAngle+SkitSet.response1Selection*[-6:2:8])>=1; %selection of the left trajectory
        
        %show the selected trajectory in bold
        if (leftTrajectorySelected && BRPleft) || (not(leftTrajectorySelected) && not(BRPleft))
            BRPBallSize=  SkitSet.BallRadius/3;
            BRP_alt_BallSize=  SkitSet.BallRadius/5;
            correct= true;
            
        else
            BRPBallSize=  SkitSet.BallRadius/5;
            BRP_alt_BallSize=  SkitSet.BallRadius/3;
            correct= false;
        end
        
        
        
        for trajectoryPoint = 1 : 10 : firstVerticalCrossing  %length(BRP.trajectory.time) %plot trajectory every 10 points
            glPushMatrix;
            glTranslatef( BRP.trajectory.x(trajectoryPoint), BRP.trajectory.y(trajectoryPoint), 0 );
            glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.1 0.6 0.0 0 ] );
            glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, [ color(colorTraces(1),:) 0 ] );
            glutSolidSphere( BRPBallSize, 100, 100 );
            glPopMatrix;
        end
        
        % Get and draw alternative ball trajectory vector
        %         if BRP_alt.posthit
        %             posthit_alt=round(BRP_alt.t_posthit*SkitSet.FrameRate);
        %         end
        for trajectoryPoint = 1 : 10 : firstVerticalCrossing_alt % min([posthit_alt,firstVerticalCrossing])  %plot trajectory every 10 points
            glPushMatrix;
            glTranslatef( BRP_alt.trajectory.x(trajectoryPoint), BRP_alt.trajectory.y(trajectoryPoint), 0 );
            glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.1 0.6 0.0 0 ] );
            glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, [ color(colorTraces(2),:) 0 ] );
            glutSolidSphere( BRP_alt_BallSize, 100, 100 );
            glPopMatrix;
        end
        
        Screen( 'EndOpenGL', win );
        %fixed mapping between letter and colour. We could add stickers to keyboard
        %Screen('DrawText', win, SkitSet.responseKeyNames(1), winCenter.x, winCenter.y-300, 255*color(1,:));
        % Screen('DrawText', win, SkitSet.responseKeyNames(2), winCenter.x, winCenter.y-250, 255*color(2,:));
        
        Screen( 'Flip', win, [], [], 0 );
        frameCount=frameCount+1;
    end
end