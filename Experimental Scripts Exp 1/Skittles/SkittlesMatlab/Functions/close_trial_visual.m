function [resultData] = close_trial_visual(trial, resultData)


global BRP BRP_alt;
global status;
global BCP;
global TRP;
global block;


%Save data
%file_name = [subject_folder, '\Sub', num2str( sub, '%04d' ), 'Trial', num2str( current_trial, '%04d' ) ];



%save( file_name, 'BRP', 'time', 'contact', 'angles' )
%Store other stuff
resultData.visual.movementData(block).BRP(trial)     = BRP;
resultData.visual.movementData(block).BRP_alt(trial) = BRP_alt;
resultData.visual.movementData(block).BCP(trial)     = BCP;
resultData.visual.movementData(block).TRP(trial)     = TRP;
%Reset variables
status  = set_status;
status.task.motor=0;
status.task.visual=1;

BRP     = set_BallReleaseParameters;
BRP_alt = set_BallReleaseParameters;
BCP     = set_BallCollisionParameters;
TRP     = set_TargetReleaseParameters;