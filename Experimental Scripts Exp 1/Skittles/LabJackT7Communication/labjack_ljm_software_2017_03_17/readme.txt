Requirements
------------
1. Mac OS X 10.6 Leopard or greater. (LJM may work on 10.5.)

Additionally, to create programs with LJM, the Mac OS X Developer tools may be required.


Installation
------------

This installer will install the following by default:

/Applications/Kipling.app

/usr/local/lib:
    libusb-1.0.0.dylib
    libusb-1.0.dylib -> /usr/local/lib/libusb-1.0.0.dylib
    liblabjackusb-2.6.0.dylib
    liblabjackusb-2.dylib -> /usr/local/lib/liblabjackusb-2.6.0.dylib
    liblabjackusb.dylib -> /usr/local/lib/liblabjackusb-2.dylib
    libLabJackM-1.14.6.dylib
    libLabJackM-1.dylib -> /usr/local/lib/libLabJackM-1.14.6.dylib
    libLabJackM.dylib -> /usr/local/lib/libLabJackM-1.dylib

/usr/local/include:
    labjackusb.h
    libusb-1.0/libusb.h
    LabJackM.h

/usr/local/share/LabJack/LJM:
    ljm.log
    ljm_constants.json
    ljm_specific_ips.config
    ljm_startup_configs.json
    readme.md
