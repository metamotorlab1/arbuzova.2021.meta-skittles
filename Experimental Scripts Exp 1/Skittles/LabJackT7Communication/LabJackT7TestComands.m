%% first load the LJMlibrary (warnings suppressed)
lib = '/usr/local/lib/libLabJackM';
header = '/usr/local/include/LabJackM.h';

warning('off','all');
loadlibrary(lib, header);
warning('on','all');

%% This function is very helpful to list all functions in the library
%libfunctions('libLabJackM') 
%libfunctions('libLabJackM','-full');

% Every function we want to use from the LJMlibrary has to be called with the calllib funtion
% The signature is calllib('labJackM','name_of_function',function_parameterA,function_parameterB,....)
% To see the complete function signature (output values and input % parameters) use libfunctions('libLabJackM','-full')
%% open handle to read/write to the LabJack box

[deviceType, connectionType, identifier, handle,number,SerialNumber,IPAddress,Port,MaxBytesPerMB] =deal(0);
%[error, handle] = calllib('libLabJackM', 'LJM_OpenS', 'ANY', 'ANY','ANY',handle)

[err, deviceType, connectionType, identifier, handle] =  calllib('libLabJackM', 'LJM_OpenS', 'ANY', 'ANY','ANY',handle);
%errCheck(err)

%[error, handle] = calllib('libLabJackM', 'LJM_Open', 'LJM_dtANY', 'LJM_ctANY', 'LJM_idANY', handle)

%% read serial number and handle info
% probably we don't need this
%[err, identifier2, serial_number]=calllib('libLabJackM','LJM_eReadName',handle,'SERIAL_NUMBER',number);
%errCheck(err);
%calllib('libLabJackM','LJM_GetHandleInfo',DeviceType,ConnectionType,SerialNumber,IPAddress,Port,MaxBytesPerMB)

%% make a nice adress list (map from chanel names(e.g. AIN1) to Adress numbers. 
% Output 'aTypes' containes a list of chanel types. For specification see p.9 in the library documentation  

num_adresses=2;
scan_list=zeros(1,num_adresses);

aTypes=zeros(1,num_adresses);
chanNames={'AIN1','AIN0'};

[err, chanNames, scan_list, aTypes]= calllib('libLabJackM','LJM_NamesToAddresses',num_adresses, chanNames, scan_list, aTypes);
%errCheck(err)
%% Read single signal by name in a loop from AIN1 and AIN0


Value,Value0=deal(0);
 for i=1:1:4000
     [err,~,Value]=calllib('libLabJackM','LJM_eReadName',handle,'AIN1',Value);
      errCheck(err)
     [err,~,Value0]=calllib('libLabJackM','LJM_eReadName',handle,'AIN0',Value0);
     errCheck(err)
    [Value Value0]
 end


%% read array from adress

dataPoints=10;
newValues=zeros(1,dataPoints);

errorAddress=0;

[err,newValues,errorAddress]=calllib('libLabJackM','LJM_eReadAddressArray',handle, 0, 3, dataPoints, newValues,errorAddress);
%errCheck(err)


%% write to String to address
[err,a]=calllib('libLabJackM','LJM_eWriteAddressString',handle,60500, 'LabJackT7MetaCognitionLab');
%errCheck(err)


%% read bytes from adress
% this does not work

% numBytes = 10;
% aBytes='          ';
% %[err,bla,numBytes] = calllib('libLabJackM','LJM_eReadName',handle, 'LUA_DEBUG_NUM_BYTES', numBytes)
% %errCheck(err)
% 
% [err,aBytes,errorAddress]=calllib('libLabJackM','LJM_eReadAddressByteArray',handle,5050,numBytes,aBytes,errorAddress)
% errCheck(err)

%% Streams Open/Close and hopefully read
% opening + closing works, but not reading from stream


% scan_rate=10;
% [aData, DeviceScanBacklog , LJMScanBacklog]=deal(0);
 
%scan_list=[0,2];
%[err, int32Ptr, doublePtr]= calllib('libLabJackM','LJM_eStreamStart',handle,scan_rate,num_adresses,scan_list,scan_rate);
%errCheck(err);
%
% %for i=1:1:10
% %[err, aData, DeviceScanBacklog , LJMScanBacklog]= calllib('libLabJackM','LJM_eStreamRead',handle,aData,DeviceScanBacklog,LJMScanBacklog);    
% %errCheck(err);
% 
 %err=calllib('libLabJackM','LJM_eStreamStop',handle);
 %errCheck(err);
% 
%% stream burst
% open,read and close a stream simultaneously

% NumScans=0;
% num_adresses=1;
% scan_list=4800;
% [err, scan_rate, NumScans, aData]=calllib('libLabJackM','LJM_StreamBurst',handle,num_adresses,scan_list,scan_rate,NumScans,aData);
% errCheck(err)

%% List All Devices found 
%[NumFound,aDeviceTypes,aConnectionTypes,aSerialNumbers,aIPAddresses]=deal(0);
%[err, NumFound, aDeviceTypes, aConnectionTypes, aSerialNumbers, aIPAddresses]=calllib('libLabJackM','LJM_ListAll',0,0,NumFound,aDeviceTypes,aConnectionTypes,aSerialNumbers,aIPAddresses)
%errCheck(err);

calllib('libLabJackM','LJM_CloseAll');
