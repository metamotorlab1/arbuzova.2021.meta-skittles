% (c) Polina Arbuzova, December 2019
% This script calculates how stable was the staircasing during the task,
% based on SD of the difficulty

% Go to the folder
results_folder = '/Users/polinaarbuzova/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/Analysis';
cd(results_folder);

%% Experiment 1
% Load the file
exp1 = load('staircasesExp1.mat');
% Get all the SDs
exp1_VM = cellfun(@std,exp1.vdiff_vm);
exp1_M = cellfun(@std,exp1.vdiff_m);
exp1_V = cellfun(@std,exp1.vdiff_v);

% Get their means
exp1_mean_VM = mean(exp1_VM)
exp1_mean_M = mean(exp1_M)
exp1_mean_V = mean(exp1_V)

%% Experiment 2
% Load the file
exp2= load('staircasesExp2.mat');
% Get all the SDs
exp2_traj_VM = cellfun(@std,exp2.traj_vdiff_vm);
exp2_traj_M = cellfun(@std,exp2.traj_vdiff_m);
exp2_traj_V = cellfun(@std,exp2.traj_vdiff_v);

% Get their means
exp2_mean_traj_VM = mean(exp2_traj_VM)
exp2_mean_traj_M = mean(exp2_traj_M)
exp2_mean_traj_V = mean(exp2_traj_V)

% Get all the SDs
exp2_ang_VM = cellfun(@std,exp2.ang_adiff_vm);
exp2_ang_M = cellfun(@std,exp2.ang_adiff_m);
exp2_ang_V = cellfun(@std,exp2.ang_adiff_v);

% Get their means
exp2_mean_ang_VM = mean(exp2_ang_VM);
exp2_mean_ang_M = mean(exp2_ang_M);
% exp2_mean_ang_V = mean(exp2_ang_V);

%% Compare the means in Traj task
[h, p, CI] = ttest2(exp1_VM, exp2_traj_VM)
[h, p, CI] = ttest2(exp1_M, exp2_traj_M)
[h, p, CI, stats] = ttest2(exp1_VM, exp1_M)
% ttest2(exp1_V, exp2_traj_V)
[h, p, CI, stats] = ttest2(exp1_V, exp1_M)
[h, p, CI, stats] = ttest2(exp2_traj_V, exp2_traj_M)

[h, p, CI, stats] = ttest2(exp2_traj_M, exp2_ang_M)
[h, p, CI, stats] = ttest2(exp2_traj_V, exp2_ang_V)
