% (c) Polina Arbuzova, Novemeber 2019.
% This script is to plot staircases in Traj and Angles task

% 
clear all;
%% User

user = 'polina';

switch user
    case {'polina'}
        addpath(genpath('/Users/polinaarbuzova/Dropbox/PhDs/Polina/MatLab Toolboxes/Corr_toolbox_v2'));
        addpath(genpath('/Users/polinaarbuzova/Dropbox/PhDs/Polina/MatLab Toolboxes/jags-3.4.0'));
        saveDir = '/Users/polinaarbuzova/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/Analysis';
        userpath=['~/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/'];
        resultDir=['~/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/DataTest'];
        rDir = '/Users/polinaarbuzova/Dropbox/PhDs/Polina/R Analysis';
end
addpath(strcat(userpath, 'Analysis/fx'));
addpath(strcat(userpath, 'BasicVariables'));

cd(saveDir);
load('staircasesExp1Training.mat');
load('staircasesExp2Training.mat');

% TRAJECTORIES
figure
hold on
for s=1:length(traj_vdiff_vm)
    plot(traj_vdiff_vm{s})
end

figure
hold on
for s=1:length(traj_vdiff_m)
    plot(traj_vdiff_m{s})
end


% ANGLES
figure
hold on
for s=1:length(ang_adiff_vm)
    plot(ang_adiff_vm{s})
end

figure
hold on
for s=1:length(ang_adiff_m)
    plot(ang_adiff_m{s})
end

% Exp 1

figure
hold on
for s=1:length(vdiff_vm)
    plot(vdiff_vm{s})
end

%% Convert data into R-readable format

subjExp1 = {'020SM','030GY', '030VS', '050FJ', '050IN', '060UA', '070EE', ...
    '080MH', '090MK', '130MK', '140HN', '150RB', ...
    '170SA', '180CH', '181GJ', '180IG', '210KC', '211PM', ...
    '240AM', '240ME',  '260AK', '260EA', '270FS', '290SD',...
    '290US', '300BA', '220PH', '190SZ', '071SM', '130BH', '300MR', '221II', ...
    '060HD', '310AU', '050GN', '221NV', '280EB', '081MC', '130BY'}; 

subjExp2 = {'020EN', '021MT', '050MJ', '050MM',  '060NS', '070HN', '080AK', '080MS', ...
    '090MM', '090RH', '091LV', '100AB', '100NZ', '110JA', '120BT', '120RW', '120SY', '120VZ', '130ME', ...
    '131KK', '140BH', '150BR', '151TV', '160AG', '170EP', '170SP', '171HP', '180IV', '181BF', ...
    '210JJ', '211DB', '240KS', '240SI', '240SS', '240TN', '260MR', '271IM', '280SS', '290SR', '291GZ', '300IJ'};

% Four columns: subj task condition stairs

% 4 - staircase
% Exp1
exp1_stairs_vm = [];


exp1_cond_vm = [];

exp1_stairs = [];
exp1_cond = [];
exp1_subj = [];
exp1_time = [];

for s = 1:length(subjExp1)
    
    if strcmp(subjExp1(s),'020SM')
        exp1_stairs = NaN(length(exp1_stairs_vm),1);
        
    else
        
        exp1_stairs_vm = [(vdiff_vm{s})'];
    end
    exp1_stairs = [exp1_stairs; exp1_stairs_vm];
    
    exp1_time_vm = 1:1:length(exp1_stairs_vm);
    exp1_time_vm = flip(exp1_time_vm)*-1;
    
    exp1_time = [exp1_time; exp1_time_vm'];
    
    exp1_cond_vm    = cell(1, length(exp1_stairs_vm));
    exp1_cond_vm(:) = {'visuomotor'};
    
    
    exp1_cond = [exp1_cond; exp1_cond_vm'];
    
    A    = cell(1, (length(exp1_cond_vm)));
    A(:) = {subjExp1{s}};
    exp1_subj = [exp1_subj; A'];
end


exp1_task    = cell(length(exp1_cond), 1);
exp1_task(:) = {'traj1'};

% Exp2
% TRAJECTORIES
exp2_tr_stairs_vm = [];
exp2_tr_stairs_m = [];

exp2_tr_cond_vm = [];
exp2_tr_cond_m = [];

exp2_tr_stairs = [];
exp2_tr_cond = [];
exp2_tr_subj = [];
exp2_tr_time = [];


for s = 1:length(subjExp2)
    exp2_tr_stairs_vm = [(traj_vdiff_vm{s})'];
    exp2_tr_stairs_m = [(traj_vdiff_m{s})'];
    
    exp2_tr_stairs = [exp2_tr_stairs; exp2_tr_stairs_vm; exp2_tr_stairs_m];
    
    exp2_tr_time_vm = 1:1:length(exp2_tr_stairs_vm);
    exp2_tr_time_m = 1:1:length(exp2_tr_stairs_m);
    exp2_tr_time_vm = flip(exp2_tr_time_vm)*-1;
    exp2_tr_time_m = flip(exp2_tr_time_m)*-1;
    
    exp2_tr_time = [exp2_tr_time; exp2_tr_time_vm'; exp2_tr_time_m'];
    
    exp2_tr_cond_vm    = cell(1, length(exp2_tr_stairs_vm));
    exp2_tr_cond_vm(:) = {'visuomotor'};

    exp2_tr_cond_m    = cell(1, length(exp2_tr_stairs_m));
    exp2_tr_cond_m(:) = {'motor'};
    
    exp2_tr_cond = [exp2_tr_cond; exp2_tr_cond_vm'; exp2_tr_cond_m'];
    
    A    = cell(1, (length(exp2_tr_cond_vm)+length(exp2_tr_cond_m)));
    A(:) = {subjExp2{s}};
    exp2_tr_subj = [exp2_tr_subj; A'];
end

exp2_tr_task    = cell(length(exp2_tr_cond), 1);
exp2_tr_task(:) = {'traj2'};

% ANGLES
exp2_a_stairs_vm = [];
exp2_a_stairs_m = [];

exp2_a_cond_vm = [];
exp2_a_cond_m = [];

exp2_a_stairs = [];
exp2_a_cond = [];
exp2_a_subj = [];
exp2_a_time = [];


for s = 1:length(subjExp2)
    exp2_a_stairs_vm = [(ang_adiff_vm{s})'];
    exp2_a_stairs_m = [(ang_adiff_m{s})'];
    
    exp2_a_stairs = [exp2_a_stairs; exp2_a_stairs_vm; exp2_a_stairs_m];
    
    exp2_a_time_vm = 1:1:length(exp2_a_stairs_vm);
    exp2_a_time_m = 1:1:length(exp2_a_stairs_m);
    exp2_a_time_vm = flip(exp2_a_time_vm)*-1;
    exp2_a_time_m = flip(exp2_a_time_m)*-1;
    
    exp2_a_time = [exp2_a_time; exp2_a_time_vm'; exp2_a_time_m'];
    
    exp2_a_cond_vm    = cell(1, length(exp2_a_stairs_vm));
    exp2_a_cond_vm(:) = {'visuomotor'};

    exp2_a_cond_m    = cell(1, length(exp2_a_stairs_m));
    exp2_a_cond_m(:) = {'motor'};
    
    
    exp2_a_cond = [exp2_a_cond; exp2_a_cond_vm'; exp2_a_cond_m'];
    
    A    = cell(1, (length(exp2_a_cond_vm)+length(exp2_a_cond_m)));
    A(:) = {subjExp2{s}};
    exp2_a_subj = [exp2_a_subj; A'];
end

exp2_a_task    = cell(length(exp2_a_cond), 1);
exp2_a_task(:) = {'angles'};


%%%% Put everything together
exp1_all = [exp1_subj exp1_task exp1_cond num2cell(exp1_time) num2cell(exp1_stairs)];
exp2_all = [exp2_tr_subj exp2_tr_task exp2_tr_cond num2cell(exp2_tr_time) num2cell(exp2_tr_stairs); exp2_a_subj exp2_a_task exp2_a_cond num2cell(exp2_a_time) num2cell(exp2_a_stairs);];

skittles_all = [exp1_all; exp2_all];
skittles = table;
skittles.subj = skittles_all(:,1);
skittles.task = skittles_all(:,2);
skittles.condition = skittles_all(:,3);
skittles.time = skittles_all(:,4);
skittles.difficulty = skittles_all(:,5);

cd(rDir);
writetable(skittles,'skittlesAll_forStaircasesTraining.csv')