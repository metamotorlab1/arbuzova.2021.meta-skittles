library(psych)
# Jennrich test tests if two correlation matrices are different. 
mratio_traj = matrix(nrow = 3, ncol = 3)
mratio_angles = matrix(nrow = 3, ncol = 3)

# Input for ANGLES (by hand because there's no robust correlation toolbox in R)
#     VM    M      V       #     VM    M      V       
# VM   1   0.58   0.51          1,1  1,2   1,3   
# M   0.58  1     0.55          2,1  2,2   2,3
# V   0.51  0.55    1           3,1  3,2   3,3

mratio_angles[1,1] = mratio_angles[2,2] = mratio_angles[3,3] = 1
mratio_angles[2,1] = mratio_angles[1,2] = 0.58
mratio_angles[3,1] = mratio_angles[1,3] = 0.51
mratio_angles[3,2] = mratio_angles[2,3] = 0.55

# Sample size for Angles
n1 = c(39, 39, 39)

# Input for TRAJ (by hand because there's no robust correlation toolbox in R)
#     VM    M      V       #     VM    M      V       
# VM   1   0.50   0.61          1,1  1,2   1,3   
# M   0.50  1     0.43          2,1  2,2   2,3
# V   0.61  0.43    1           3,1  3,2   3,3    

mratio_traj[1,1] = mratio_traj[2,2] = mratio_traj[3,3] = 1
mratio_traj[2,1] = mratio_traj[1,2] = 0.50
mratio_traj[3,1] = mratio_traj[1,3] = 0.61
mratio_traj[3,2] = mratio_traj[2,3] = 0.43

n2 = c(31, 31, 29)

jennrich_test = cortest.jennrich(mratio_angles,mratio_traj,n1, n2)  #the Jennrich test