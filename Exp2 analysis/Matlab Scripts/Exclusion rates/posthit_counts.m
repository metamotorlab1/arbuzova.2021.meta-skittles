% (c) Polina Arbuzova, December 2019.
% This script checks the proportion of trials with post hits
% (Skittles Parameters)

% 
clear all;
%% User

user = 'polina';

switch user
    case {'polina'}
        addpath(genpath('/Users/polinaarbuzova/Dropbox/PhDs/Polina/MatLab Toolboxes/Corr_toolbox_v2'));
        addpath(genpath('/Users/polinaarbuzova/Dropbox/PhDs/Polina/MatLab Toolboxes/jags-3.4.0'));
        saveDir = '/Users/polinaarbuzova/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/Analysis';
        userpath=['~/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/'];
        resultDir=['~/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/DataTest'];
end
addpath(strcat(userpath, 'Analysis/fx'));
addpath(strcat(userpath, 'BasicVariables'));

cd(saveDir);

subjectNames = {'020EN', '021MT', '050MJ', '050MM',  '060NS', '070HN', '080AK', '080MS', ...
'090MM', '090RH', '091LV', '100AB', '100NZ', '110JA', '120BT', '120RW', '120SY', '120VZ', '130ME', ...
'131KK', '140BH', '150BR', '151TV', '160AG', '170EP', '170SP', '171HP', '180IV', '181BF', ...
'210JJ', '211DB', '240KS', '240SI', '240SS', '240TN', '260MR', '271IM', '280SS', '290SR', '291GZ', '300IJ'};

% subjectNames = {'150BR', '151TV'};

% TRAJ

for subj = 1:length(subjectNames)
    
    allBlocks = load([resultDir filesep subjectNames{subj} filesep 'Trajectories' filesep 'SkittlesResult_' subjectNames{subj} '_Traj_block_5.mat']);
    
    subjectNames{subj} % this is just printing the current subjectName to check how far we are in the analyses
    
    % fill the data into resultData
    resultDataTraj =   get_Data_Traj(allBlocks);
    % For mean difficulty 
    traj_posthit_vm(subj) = sum(resultDataTraj.posthit(resultDataTraj.type1.blindTrials==0))/length(resultDataTraj.posthit(resultDataTraj.type1.blindTrials==0));
    traj_posthit_v(subj) = sum(resultDataTraj.visual.posthit)/length(resultDataTraj.visual.posthit);
end

% Get descriptives


% 
% % TRAJECTORIES
posthit.traj.vm.null = sum(traj_posthit_vm == 0);
posthit.traj.vm.min = min(traj_posthit_vm(traj_posthit_vm~=0))
posthit.traj.vm.max = max(traj_posthit_vm(traj_posthit_vm~=0))
posthit.traj.vm.median_nonzero = median(traj_posthit_vm(traj_posthit_vm~=0))

posthit.traj.v.null = sum(traj_posthit_v == 0);
posthit.traj.v.min = min(traj_posthit_v(traj_posthit_v~=0))
posthit.traj.v.max = max(traj_posthit_v(traj_posthit_v~=0))
posthit.traj.v.median_nonzero = median(traj_posthit_v(traj_posthit_v~=0))

save('posthitErrorTrialsExp2.mat', '-v7.3')
