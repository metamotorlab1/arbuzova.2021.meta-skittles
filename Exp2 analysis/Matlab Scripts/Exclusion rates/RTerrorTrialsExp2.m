% (c) Polina Arbuzova, December 2019.
% This script checks the proportion of trials below 300ms and over 8s in Experiment 2
% (Skittles Parameters)

% 
clear all;
%% User

user = 'polina';

switch user
    case {'polina'}
        addpath(genpath('/Users/polinaarbuzova/Dropbox/PhDs/Polina/MatLab Toolboxes/Corr_toolbox_v2'));
        addpath(genpath('/Users/polinaarbuzova/Dropbox/PhDs/Polina/MatLab Toolboxes/jags-3.4.0'));
        saveDir = '/Users/polinaarbuzova/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/Analysis';
        userpath=['~/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/'];
        resultDir=['~/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/DataTest'];
end
addpath(strcat(userpath, 'Analysis/fx'));
addpath(strcat(userpath, 'BasicVariables'));

cd(saveDir);

subjectNames = {'020EN', '021MT', '050MJ', '050MM',  '060NS', '070HN', '080AK', '080MS', ...
'090MM', '090RH', '091LV', '100AB', '100NZ', '110JA', '120BT', '120RW', '120SY', '120VZ', '130ME', ...
'131KK', '140BH', '150BR', '151TV', '160AG', '170EP', '170SP', '171HP', '180IV', '181BF', ...
'210JJ', '211DB', '240KS', '240SI', '240SS', '240TN', '260MR', '271IM', '280SS', '290SR', '291GZ', '300IJ'};

% subjectNames = {'150BR', '151TV'};

lower_threshold = 0.3;
higher_threshold = 8;

% ANGLES

for subj = 1:length(subjectNames)
    
    if exist([resultDir filesep subjectNames{subj} filesep 'Angles' filesep 'SkittlesResult_' subjectNames{subj} '_Angle_block_5.mat'], 'file') == 2
        allBlocks = load([resultDir filesep subjectNames{subj} filesep 'Angles' filesep 'SkittlesResult_' subjectNames{subj} '_Angle_block_5.mat']);
    else
        allBlocks = load([resultDir filesep subjectNames{subj} filesep 'Angles' filesep 'SkittlesResult_' subjectNames{subj} '_Angle_block_4.mat']);
    end
    subjectNames{subj} % this is just printing the current subjectName to check how far we are in the analyses
    
    % fill the data into resultData
    resultDataAngles =   get_Data_Angles(allBlocks);
    
    ang_RTerror_vm(subj) = sum((resultDataAngles.type1.RT(resultDataAngles.type1.blindTrials==1)<lower_threshold)|(resultDataAngles.type1.RT(resultDataAngles.type1.blindTrials==1)>higher_threshold));
    ang_RTerror_m(subj) = sum((resultDataAngles.type1.RT(resultDataAngles.type1.blindTrials==0)<lower_threshold)|(resultDataAngles.type1.RT(resultDataAngles.type1.blindTrials==0)>higher_threshold));
    ang_RTerror_v(subj) = sum((resultDataAngles.visual.type1.RT<lower_threshold)|(resultDataAngles.visual.type1.RT>higher_threshold));
    ang_RTerror_all(subj) = ang_RTerror_vm(subj) + ang_RTerror_m(subj) + ang_RTerror_v(subj);
    
    ang_perc_RTerror_all(subj) = ang_RTerror_all(subj)/(length(resultDataAngles.type1.RT)+length(resultDataAngles.visual.type1.RT));
end

% TRAJ

for subj = 1:length(subjectNames)
    
    allBlocks = load([resultDir filesep subjectNames{subj} filesep 'Trajectories' filesep 'SkittlesResult_' subjectNames{subj} '_Traj_block_5.mat']);
    
    subjectNames{subj} % this is just printing the current subjectName to check how far we are in the analyses
    
    % fill the data into resultData
    resultDataTraj =   get_Data_Traj(allBlocks);
    % For mean difficulty 
    traj_RTerror_vm(subj) = sum((resultDataTraj.type1.RT(resultDataTraj.type1.blindTrials==1)<lower_threshold)|(resultDataTraj.type1.RT(resultDataTraj.type1.blindTrials==1)>higher_threshold));
    traj_RTerror_m(subj) = sum((resultDataTraj.type1.RT(resultDataTraj.type1.blindTrials==0)<lower_threshold)|(resultDataTraj.type1.RT(resultDataTraj.type1.blindTrials==0)>higher_threshold));
    traj_RTerror_v(subj) = sum((resultDataTraj.visual.type1.RT<lower_threshold)|(resultDataTraj.visual.type1.RT>higher_threshold));
    traj_RTerror_all(subj) = traj_RTerror_vm(subj) + traj_RTerror_m(subj) + traj_RTerror_v(subj);
    
    traj_perc_RTerror_all(subj) = traj_RTerror_all(subj)/(length(resultDataTraj.type1.RT)+length(resultDataTraj.visual.type1.RT));
end

% Get descriptives
% ANGLES

% % number of people with zero error trials
RTerror.angle.null = sum(ang_RTerror_all == 0);
RTerror.angle.min = min(ang_RTerror_all(ang_RTerror_all~=0))
RTerror.angle.max = max(ang_RTerror_all(ang_RTerror_all~=0))
RTerror.angle.median_nonzero = median(ang_RTerror_all(ang_RTerror_all~=0))

% % percentage of such trials
RTerror.angle.perc_min = min(ang_perc_RTerror_all(ang_perc_RTerror_all~=0))
RTerror.angle.perc_max = max(ang_perc_RTerror_all(ang_perc_RTerror_all~=0))
RTerror.angle.perc_median_nonzero = median(ang_perc_RTerror_all(ang_RTerror_all~=0))

% 
% % TRAJECTORIES
RTerror.traj.null = sum(traj_RTerror_all == 0);
RTerror.traj.min = min(traj_RTerror_all(traj_RTerror_all~=0))
RTerror.traj.max = max(traj_RTerror_all(traj_RTerror_all~=0))
RTerror.traj.median_nonzero = median(traj_RTerror_all(traj_RTerror_all~=0))

% % percentage of such trials
RTerror.traj.perc_min = min(traj_perc_RTerror_all(traj_perc_RTerror_all~=0))
RTerror.traj.perc_max = max(traj_perc_RTerror_all(traj_perc_RTerror_all~=0))
RTerror.traj.perc_median_nonzero = median(traj_perc_RTerror_all(traj_RTerror_all~=0))

save('RTerrorTrialsExp2.mat', 'RTerror', '-v7.3')
