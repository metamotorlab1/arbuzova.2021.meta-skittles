% (c) Polina Arbuzova, December 2019.
% This script checks the proportion of error trials in Experiment 2
% (Skittles Parameters)

% 
clear all;
%% User

user = 'polina';

switch user
    case {'polina'}
        addpath(genpath('/Users/polinaarbuzova/Dropbox/PhDs/Polina/MatLab Toolboxes/Corr_toolbox_v2'));
        addpath(genpath('/Users/polinaarbuzova/Dropbox/PhDs/Polina/MatLab Toolboxes/jags-3.4.0'));
        saveDir = '/Users/polinaarbuzova/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/Analysis';
        userpath=['~/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/'];
        resultDir=['~/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/DataTest'];
end
addpath(strcat(userpath, 'Analysis/fx'));
addpath(strcat(userpath, 'BasicVariables'));

cd(saveDir);

subjectNames = {'020EN', '021MT', '050MJ', '050MM',  '060NS', '070HN', '080AK', '080MS', ...
'090MM', '090RH', '091LV', '100AB', '100NZ', '110JA', '120BT', '120RW', '120SY', '120VZ', '130ME', ...
'131KK', '140BH', '150BR', '151TV', '160AG', '170EP', '170SP', '171HP', '180IV', '181BF', ...
'210JJ', '211DB', '240KS', '240SI', '240SS', '240TN', '260MR', '271IM', '280SS', '290SR', '291GZ', '300IJ'};

% subjectNames = {'150BR', '151TV'};

% ANGLES

for subj = 1:length(subjectNames)
    
    if exist([resultDir filesep subjectNames{subj} filesep 'Angles' filesep 'SkittlesResult_' subjectNames{subj} '_Angle_block_5.mat'], 'file') == 2
        allBlocks = load([resultDir filesep subjectNames{subj} filesep 'Angles' filesep 'SkittlesResult_' subjectNames{subj} '_Angle_block_5.mat']);
    else
        allBlocks = load([resultDir filesep subjectNames{subj} filesep 'Angles' filesep 'SkittlesResult_' subjectNames{subj} '_Angle_block_4.mat']);
    end
    subjectNames{subj} % this is just printing the current subjectName to check how far we are in the analyses
    
    % fill the data into resultData
    resultDataAngles =   get_Data_Angles(allBlocks);
    
    ang_error_vm(subj) = sum(resultDataAngles.type1.errorTrials(resultDataAngles.type1.blindTrials==1))/length(resultDataAngles.type1.errorTrials(resultDataAngles.type1.blindTrials==1))
    ang_error_m(subj) = sum(resultDataAngles.type1.errorTrials(resultDataAngles.type1.blindTrials==0))/length(resultDataAngles.type1.errorTrials(resultDataAngles.type1.blindTrials==0))
    ang_error_v(subj) = sum(resultDataAngles.visual.type1.errorTrials)/length(resultDataAngles.visual.type1.errorTrials)
    ang_error_all(subj) = (sum(resultDataAngles.type1.errorTrials)+sum(resultDataAngles.visual.type1.errorTrials))/(length(resultDataAngles.type1.errorTrials+length(resultDataAngles.visual.type1.errorTrials)))
end

% TRAJ

for subj = 1:length(subjectNames)
    
    allBlocks = load([resultDir filesep subjectNames{subj} filesep 'Trajectories' filesep 'SkittlesResult_' subjectNames{subj} '_Traj_block_5.mat']);
    
    subjectNames{subj} % this is just printing the current subjectName to check how far we are in the analyses
    
    % fill the data into resultData
    resultDataTraj =   get_Data_Traj(allBlocks);
    % For mean difficulty 
    traj_error_vm(subj) = sum(resultDataTraj.type1.errorTrials(resultDataTraj.type1.blindTrials==1))/length(resultDataTraj.type1.errorTrials(resultDataTraj.type1.blindTrials==1))
    traj_error_m(subj) = sum(resultDataTraj.type1.errorTrials(resultDataTraj.type1.blindTrials==0))/length(resultDataTraj.type1.errorTrials(resultDataTraj.type1.blindTrials==0))
    traj_error_v(subj) = sum(resultDataTraj.visual.type1.errorTrials)/length(resultDataTraj.visual.type1.errorTrials) 
    traj_error_all(subj) = (sum(resultDataTraj.type1.errorTrials)+sum(resultDataTraj.visual.type1.errorTrials))/(length(resultDataTraj.type1.errorTrials+length(resultDataTraj.visual.type1.errorTrials)))
end

% Get descriptives
% ANGLES
angles_all_errors = ang_error_all
error.angle.all = angles_all_errors
% number of people with zero error trials
error.angle.null = sum(angles_all_errors == 0)
error.angle.min_nonzero = min(angles_all_errors(angles_all_errors~=0))
error.angle.max = max(angles_all_errors)
error.angle.mean_nonzero = mean(angles_all_errors(angles_all_errors~=0))
error.angle.mean_wzero = mean(angles_all_errors)
error.angle.meadian_nonzero = median(angles_all_errors(angles_all_errors~=0))
error.angle.median_wzero = median(angles_all_errors)

% TRAJECTORIES
traj_all_errors = traj_error_all
error.traj.all = traj_all_errors
error.traj.min = min(traj_all_errors(traj_all_errors~=0))
error.traj.max = max(traj_all_errors)
error.traj.mean_nonzero = mean(traj_all_errors(traj_all_errors~=0))
error.traj.mean_wzero = mean(traj_all_errors)
error.traj.median_nonzero = median(traj_all_errors(traj_all_errors~=0))
error.traj.median_wzero = median(traj_all_errors)

save('errorTrialsExp2.mat', 'error', '-v7.3')
