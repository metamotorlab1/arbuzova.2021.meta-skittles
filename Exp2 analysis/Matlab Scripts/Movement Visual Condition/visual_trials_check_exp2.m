% (c) Polina Arbuzova, December 2019.
% This script checks the proportion of visual trials with movement in Experiment 2
% (Skittles Parameters)
% 
clear all;
%% User

user = 'polina';

switch user
    case {'polina'}
        addpath(genpath('/Users/polinaarbuzova/Dropbox/PhDs/Polina/MatLab Toolboxes/Corr_toolbox_v2'));
        addpath(genpath('/Users/polinaarbuzova/Dropbox/PhDs/Polina/MatLab Toolboxes/jags-3.4.0'));
        saveDir = '/Users/polinaarbuzova/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/Analysis';
        userpath=['~/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/'];
        resultDir=['~/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/DataTest'];
end
addpath(strcat(userpath, 'Analysis/fx'));
addpath(strcat(userpath, 'BasicVariables'));

cd(saveDir);
% subjectNames = {'020EN', '021MT'};

subjectNames = {'020EN', '021MT', '050MJ', '050MM',  '060NS', '070HN', '080AK', '080MS', ...
'090MM', '090RH', '091LV', '100AB', '100NZ', '110JA', '120BT', '120RW', '120SY', '120VZ', '130ME', ...
'131KK', '140BH', '150BR', '151TV', '160AG', '170EP', '170SP', '171HP', '180IV', '181BF', ...
'210JJ', '211DB', '240KS', '240SI', '240SS', '240TN', '260MR', '271IM', '280SS', '290SR', '291GZ', '300IJ'};


% TRAJ

for subj = 1:length(subjectNames)
    
    allBlocks = load([resultDir filesep subjectNames{subj} filesep 'Trajectories' filesep 'SkittlesResult_' subjectNames{subj} '_Traj_block_5.mat']);
    
    subjectNames{subj} % this is just printing the current subjectName to check how far we are in the analyses
    
    numBlocks = length(allBlocks.resultDataTraj.visual.block);
    
    for b=1:numBlocks
        movementDataExtra{b} = allBlocks.resultDataTraj.visual.block(b).movementDataExtra;
        for t=1:40
            vis_range(b,t) = range(movementDataExtra{b}(t).angles)         
        end
    end
    prop_vis_mov_traj(subj) = length(find(vis_range>3))/numel(vis_range)
    
end
average_movement_vis_trials_traj = mean(prop_vis_mov_traj);

% ANGLES

for subj = 1:length(subjectNames)
    
    if exist([resultDir filesep subjectNames{subj} filesep 'Angles' filesep 'SkittlesResult_' subjectNames{subj} '_Angle_block_5.mat'], 'file') == 2
        allBlocks = load([resultDir filesep subjectNames{subj} filesep 'Angles' filesep 'SkittlesResult_' subjectNames{subj} '_Angle_block_5.mat']);
    else
        allBlocks = load([resultDir filesep subjectNames{subj} filesep 'Angles' filesep 'SkittlesResult_' subjectNames{subj} '_Angle_block_4.mat']);
    end
    subjectNames{subj} % this is just printing the current subjectName to check how far we are in the analyses
    
    numBlocks = length(allBlocks.resultDataAngle.visual.block);
    
    for b=1:numBlocks
        movementDataExtra{b} = allBlocks.resultDataAngle.visual.block(b).movementDataExtra;
        for t=1:40
            vis_range(b,t) = range(movementDataExtra{b}(t).angles)
        end
    end
    prop_vis_mov_angle(subj) = length(find(vis_range>3))/numel(vis_range)
end

average_movement_vis_trials_angle = mean(prop_vis_mov_angle);
save('visual_mov_Exp2.mat', '-v7.3')
