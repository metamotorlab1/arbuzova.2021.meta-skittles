close all
clear all

% Look at the correlation between the same conditions of two different
% tasks

%% User

user = 'polina';

switch user
    case {'polina'}
        addpath(genpath('/Users/polinaarbuzova/Dropbox/PhDs/Polina/MatLab Toolboxes/Corr_toolbox_v2'));
        addpath(genpath('/Users/polinaarbuzova/Dropbox/PhDs/Polina/MatLab Toolboxes/jags-3.4.0'));
        saveDir = '/Users/polinaarbuzova/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/Analysis';
        jaspDir = '/Users/polinaarbuzova/Dropbox/PhDs/Polina/JASP Analysis/SkittlesParameters/For Bayesian analysis';
end
%%

cd(saveDir);

results_Angle = load('metaRatios_Angles.mat')
results_Traj = load('metaRatios_Traj.mat')

% Get the data
Angles_motor = results_Angle.Skittles_motor;
Angles_visual = results_Angle.Skittles_visual;
Angles_visuomotor = results_Angle.Skittles_visuomotor;

Traj_motor = results_Traj.Skittles_motor;
Traj_visual = results_Traj.Skittles_visual;
Traj_visuomotor = results_Traj.Skittles_visuomotor;


%% Angles Motor vs Traj Motor
indeces_NaN_motor = find((~isnan(Angles_motor)) & (~isnan(Traj_motor)));
Angles_motor_clean = Angles_motor(indeces_NaN_motor)
Traj_motor_clean = Traj_motor(indeces_NaN_motor)

% normal correlation 
[r, p] = corr(Angles_motor_clean, Traj_motor_clean)

% robust correlation (toolbox) 
%correlation_results_motor = robust_correlation(Angles_motor_clean, Traj_motor_clean);
[r,t,h,outid,hboot,CI] =  skipped_correlation_changedPlots(Angles_motor_clean, Traj_motor_clean);
close all;
%% Find the bivariate outliers (in skipped correlations, boxplots rule is
% used for that)

% outliers_idx = find(correlation_results_motor.outliers.bivariate.boxplot==1);
% Angles_motor_clean(outliers_idx)=[];
% Traj_motor_clean(outliers_idx)=[];
% 
% Bayes_Inter_Motor = table;
% Bayes_Inter_Motor.Ang_Motor = Angles_motor_clean;
% Bayes_Inter_Motor.Traj_Motor = Traj_motor_clean;
% 
% % Save it as a CSV
% cd(jaspDir);
% writetable(Bayes_Inter_Motor,'Bayes_Inter_Motor.csv')    
%%

%ttest
[h, p, ci, stats] = ttest(Angles_motor_clean, Traj_motor_clean)

%% Angles Visuomotor vs Traj Visuomotor
indeces_NaN_visuomotor = find((~isnan(Angles_visuomotor)) & (~isnan(Traj_visuomotor)));
Angles_visuomotor_clean = Angles_visuomotor(indeces_NaN_visuomotor)
Traj_visuomotor_clean = Traj_visuomotor(indeces_NaN_visuomotor)

% normal correlation 
[r, p] = corr(Angles_visuomotor_clean, Traj_visuomotor_clean)

% robust correlation (toolbox) 
%correlation_results_visuomotor = robust_correlation(Angles_visuomotor_clean, Traj_visuomotor_clean);
[r,t,h,outid,hboot,CI] = skipped_correlation_changedPlots(Angles_visuomotor_clean, Traj_visuomotor_clean);
close all;

% %% Find the bivariate outliers (in skipped correlations, boxplots rule is
% % used for that)
% 
% outliers_idx = find(correlation_results_visuomotor.outliers.bivariate.boxplot==1);
% Angles_visuomotor_clean(outliers_idx)=[];
% Traj_visuomotor_clean(outliers_idx)=[];
% 
% Bayes_Inter_Visuomotor = table;
% Bayes_Inter_Visuomotor.Ang_Visuomotor = Angles_visuomotor_clean;
% Bayes_Inter_Visuomotor.Traj_Visuomotor = Traj_visuomotor_clean;
% 
% % Save it as a CSV
% cd(jaspDir);
% writetable(Bayes_Inter_Visuomotor,'Bayes_Inter_Visuomotor.csv')    
%%

%ttest
[h, p, ci, stats] = ttest(Angles_visuomotor_clean, Traj_visuomotor_clean)

%% Angles Visual vs Traj Visual
indeces_NaN_visual = find((~isnan(Angles_visual)) & (~isnan(Traj_visual)));
Angles_visual_clean = Angles_visual(indeces_NaN_visual)
Traj_visual_clean = Traj_visual(indeces_NaN_visual)

% normal correlation 
[r, p] = corr(Angles_visual_clean, Traj_visual_clean)

% robust correlation (toolbox) 
%correlation_results_visual = robust_correlation(Angles_visual_clean, Traj_visual_clean);

[r,t,h,outid,hboot,CI] = skipped_correlation_changedPlots(Angles_visual_clean, Traj_visual_clean);
%close all;
% %% Find the bivariate outliers (in skipped correlations, boxplots rule is
% % used for that)
% 
% outliers_idx = find(correlation_results_visual.outliers.bivariate.boxplot==1);
% Angles_visual_clean(outliers_idx)=[];
% Traj_visual_clean(outliers_idx)=[];
% 
% Bayes_Inter_Visual = table;
% Bayes_Inter_Visual.Ang_Visual = Angles_visual_clean;
% Bayes_Inter_Visual.Traj_Visual = Traj_visual_clean;
% 
% % Save it as a CSV
% cd(jaspDir);
% writetable(Bayes_Inter_Visual,'Bayes_Inter_Visual.csv')    
%%

%ttest
[h, p, ci, stats] = ttest(Angles_visual_clean, Traj_visual_clean)

