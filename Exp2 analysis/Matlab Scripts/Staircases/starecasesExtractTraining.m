% (c) Polina Arbuzova, Novemeber 2019.
% This script is to plot staircases from the TRAINING block in Traj and Angles task

% 
clear all;
%% User

user = 'polina';

switch user
    case {'polina'}
        addpath(genpath('/Users/polinaarbuzova/Dropbox/PhDs/Polina/MatLab Toolboxes/Corr_toolbox_v2'));
        addpath(genpath('/Users/polinaarbuzova/Dropbox/PhDs/Polina/MatLab Toolboxes/jags-3.4.0'));
        saveDir = '/Users/polinaarbuzova/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/Analysis';
        userpath=['~/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/'];
        resultDir=['~/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/DataTest'];
end
addpath(strcat(userpath, 'Analysis/fx'));
addpath(strcat(userpath, 'BasicVariables'));

cd(saveDir);

subjectNames = {'020EN', '021MT', '050MJ', '050MM',  '060NS', '070HN', '080AK', '080MS', ...
'090MM', '090RH', '091LV', '100AB', '100NZ', '110JA', '120BT', '120RW', '120SY', '120VZ', '130ME', ...
'131KK', '140BH', '150BR', '151TV', '160AG', '170EP', '170SP', '171HP', '180IV', '181BF', ...
'210JJ', '211DB', '240KS', '240SI', '240SS', '240TN', '260MR', '271IM', '280SS', '290SR', '291GZ', '300IJ'};

% subjectNames = {'170SP'};

% ANGLES

for subj = 1:length(subjectNames)
    
    if exist([resultDir filesep subjectNames{subj} filesep 'Angles' filesep 'SkittlesResult_' subjectNames{subj} '_Angle.mat'], 'file') == 2
        allBlocks = load([resultDir filesep subjectNames{subj} filesep 'Angles' filesep 'SkittlesTrainingResult_' subjectNames{subj} '.mat']);
    else
        allBlocks = load([resultDir filesep subjectNames{subj} filesep 'Angles' filesep 'SkittlesTrainingResult_' subjectNames{subj} '.mat']);
    end
    subjectNames{subj} % this is just printing the current subjectName to check how far we are in the analyses
    
    % fill the data into resultData
    resultDataAngles =   get_Data_Angles_Training(allBlocks);
    % For mean vdiff / adiff ('difficulty')
    resultDataAngles.type1.whichStair = resultDataAngles.type1.whichStair(1:length(resultDataAngles.type1.RT)); % for cases when not all 5 blocks are finished
    
    ang_adiff_vm{subj} = abs(resultDataAngles.type1.adiff(resultDataAngles.type1.whichStair==1));
    ang_adiff_m{subj} = abs(resultDataAngles.type1.adiff(resultDataAngles.type1.whichStair==2));
    
end

% TRAJ

for subj = 1:length(subjectNames)
    
    allBlocks = load([resultDir filesep subjectNames{subj} filesep 'Trajectories' filesep 'SkittlesTrainingResult_' subjectNames{subj} '.mat']);
    
    subjectNames{subj} % this is just printing the current subjectName to check how far we are in the analyses
    
    % fill the data into resultData
    resultDataTraj =   get_Data_Traj_Training(allBlocks);
    % For mean difficulty 
    traj_vdiff_vm{subj} = abs(resultDataTraj.type1.vdiff(resultDataTraj.type1.whichStair==1));
    traj_vdiff_m{subj} = abs(resultDataTraj.type1.vdiff(resultDataTraj.type1.whichStair==2));
     
end
save('staircasesExp2Training.mat','ang_adiff_vm','ang_adiff_m', 'traj_vdiff_vm', 'traj_vdiff_m', '-v7.3')

