clear all
close all

%% Before doing this analysis, execute run_correlateTasks.
% This will save a .mat file that will be loaded here for analyses (so you only need to run it ones and you can do the analyses on the .mat file).
% saveDir must be the same path you used to save the .mat file in
% run_correlateTasks

% If you just want to get skipped correlation plots, use the short version
% below

%% User

user = 'polina';

switch user
    case {'polina'}
        addpath(genpath('/Users/polinaarbuzova/Dropbox/PhDs/Polina/MatLab Toolboxes/Corr_toolbox_v2'));
        addpath(genpath('/Users/polinaarbuzova/Dropbox/PhDs/Polina/MatLab Toolboxes/jags-3.4.0'));
        saveDir = '/Users/polinaarbuzova/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/Analysis';
end

%% Trajectories or Angles?
% Comment in the task

% task = '';
task = '_Angles';
% task = '_Traj';
%% Long or short?
% Long version plots all the graphs provided by the robust correlations
% toolbox, does regular correlation tests and also does t-tests between
% conditions.
% Short version just plots skipped correlations (including bootstrapping
% distributions for Pearson's and Spearman's correlations)

details = 1; % 1 for long and 0 for short;

%% compute correlations and t-tests
cd(saveDir);

load(['metaRatios' task '.mat'])

if details == 1
    % Long version with all the possible plots from robust correlation
    % toolbox and extra tests
    
%     % plot correlation matrix
%     figure;
%     pairs(metaRatios, {'Skittles/NonBlind' 'Skittles/Blind' 'Skittles/Visual' 'Dots/Visual'})
    
    'visuomotor / visual'
    
    indeces_NaN_visuomotor_Skittles_visual = find((~isnan(Skittles_visuomotor)) & (~isnan(Skittles_visual)));
    
    Skittles_visuomotor_clean = Skittles_visuomotor(indeces_NaN_visuomotor_Skittles_visual);
    Skittles_visual_clean = Skittles_visual(indeces_NaN_visuomotor_Skittles_visual);
    
    % normal correlation
    [r, p] = corr(Skittles_visuomotor_clean, Skittles_visual_clean);
    
    % robust correlation (toolbox) visuomotor Skittles/ visual Skittles
    correlation_results_visuomotor_visual = robust_correlation(Skittles_visuomotor_clean, Skittles_visual_clean);
    
    % t-test
    % first test if normally distributed
    h_visuomotor_normal = lillietest(Skittles_visuomotor_clean);
    h_visual_skittles_normal = lillietest(Skittles_visual_clean);
    
    [h, p, ci, stats] = ttest(Skittles_visuomotor_clean, Skittles_visual_clean)
    
    
    'visuomotor / motor'
    
    indeces_NaN_visuomotor_motor = find((~isnan(Skittles_visuomotor)) & (~isnan(Skittles_motor)));
    
    Skittles_visuomotor_clean = Skittles_visuomotor(indeces_NaN_visuomotor_motor);
    Skittles_motor_clean = Skittles_motor(indeces_NaN_visuomotor_motor);
    
    % normal correlation
    [r, p] = corr(Skittles_visuomotor_clean, Skittles_motor_clean);
    
    % robust correlation (toolbox) visuomotor Skittles/ motor Skittles
    correlation_results_visuomotor_motor = robust_correlation(Skittles_visuomotor_clean, Skittles_motor_clean);
    
    % t-test
    % first test if normally distributed
    h_visuomotor_normal = lillietest(Skittles_visuomotor_clean);
    h_motor_normal = lillietest(Skittles_motor_clean);
    
    [h, p, ci, stats] = ttest(Skittles_visuomotor_clean, Skittles_motor_clean)
    
    
    'visual / motor'
    
    indeces_NaN_visual_Skittles_motor = find((~isnan(Skittles_visual)) & (~isnan(Skittles_motor)));
    
    Skittles_visual_clean = Skittles_visual(indeces_NaN_visual_Skittles_motor);
    Skittles_motor_clean = Skittles_motor(indeces_NaN_visual_Skittles_motor);
    
    % normal correlation
    [r, p] = corr(Skittles_visual_clean, Skittles_motor_clean);
    
    % robust correlation (toolbox) visual Skittles/ motor Skittles
    correlation_results_visual_Skittles_motor = robust_correlation(Skittles_visual_clean, Skittles_motor_clean);
    
    % t-test
    % first test if normally distributed
    h_visual_normal = lillietest(Skittles_visual_clean);
    h_motor_normal = lillietest(Skittles_motor_clean);
    
    [h, p, ci, stats] = ttest(Skittles_visual_clean, Skittles_motor_clean)
else
    
    % Short version: just skipped correlations plots
    
    'visuomotor / visual'
    
    indeces_NaN_visuomotor_Skittles_visual = find((~isnan(Skittles_visuomotor)) & (~isnan(Skittles_visual)));
    
%     Skittles_visuomotor_clean = Skittles_visuomotor(indeces_NaN_visuomotor_Skittles_visual);
%     Skittles_visual_clean = Skittles_visual(indeces_NaN_visuomotor_Skittles_visual);
    
%     Sk_VM_Dprime = [skittlesResults.SSEfit_nonBlind.da]' 
%     Sk_M_Dprime = [skittlesResults.SSEfit_blind.da]'
%     Sk_V_Dprime = [skittlesResults.SSEfit_visual.da]'
    
    Skittles_visuomotor_clean = Skittles_visuomotor(indeces_NaN_visuomotor_Skittles_visual);
    Skittles_visual_clean = Skittles_visual(indeces_NaN_visuomotor_Skittles_visual);    
    
    [r,t,h,outid,hboot,CI] = skipped_correlation_changedPlots(Skittles_visuomotor_clean, Skittles_visual_clean); % skipped correlation
    
    clear r t h outid hboot CI
    
    'visuomotor / motor'
    
    indeces_NaN_visuomotor_motor = find((~isnan(Skittles_visuomotor)) & (~isnan(Skittles_motor)));
    
    Skittles_visuomotor_clean = Skittles_visuomotor(indeces_NaN_visuomotor_motor);
    Skittles_motor_clean = Skittles_motor(indeces_NaN_visuomotor_motor);
    
    [r,t,h,outid,hboot,CI] = skipped_correlation_changedPlots(Skittles_visuomotor_clean, Skittles_motor_clean); % skipped correlation
    
    clear r t h outid hboot CI
    
    'visual  / motor'
    
    indeces_NaN_visual_Skittles_motor = find((~isnan(Skittles_visual)) & (~isnan(Skittles_motor)));
    
    Skittles_visual_clean = Skittles_visual(indeces_NaN_visual_Skittles_motor);
    Skittles_motor_clean = Skittles_motor(indeces_NaN_visual_Skittles_motor);    
    
    [r,t,h,outid,hboot,CI] = skipped_correlation_changedPlots(Skittles_visual_clean, Skittles_motor_clean); % skipped correlation
    clear r t h outid hboot CI
    
end

