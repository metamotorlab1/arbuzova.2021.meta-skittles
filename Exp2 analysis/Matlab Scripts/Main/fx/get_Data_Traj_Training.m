function resultData=get_Data_Traj_Training(allBlocks)

resultData.type1.errorTrials  = [allBlocks.resultDataTraj.type1.errorTrials];
resultData.type1.RT           = [allBlocks.resultDataTraj.type1.RT];

resultData.type1.blindTrials  = [allBlocks.resultDataTraj.block.blindTrials];
resultData.type1.blindTrials  = resultData.type1.blindTrials( : )';

resultData.type1.whichStair   = [allBlocks.resultDataTraj.type1.whichStair];
resultData.type1.vdiff        = [allBlocks.resultDataTraj.type1.vdiff];

% set (only) here the condition for clean trials
resultData.cleanIndexes           = find(resultData.type1.RT <= 8 & resultData.type1.RT >= 0.3 & ~resultData.type1.errorTrials(1:length(resultData.type1.RT)));
resultData.type1.blindTrials      = resultData.type1.blindTrials(1:length(resultData.type1.RT)); % just for the cases when the legth of the 
resultData.type1.blindTrialsClean = resultData.type1.blindTrials(resultData.cleanIndexes); 

end