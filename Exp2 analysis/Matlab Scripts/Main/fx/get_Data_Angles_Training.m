function resultData=get_Data_Angles_Training(allBlocks)

resultData.type1.errorTrials  = [allBlocks.resultDataAngle.type1.errorTrials];
resultData.type1.RT           = [allBlocks.resultDataAngle.type1.RT];

resultData.type1.blindTrials  = [allBlocks.resultDataAngle.block.blindTrials];
resultData.type1.blindTrials  = resultData.type1.blindTrials( : )';

resultData.type1.whichStair   = [allBlocks.resultDataAngle.type1.whichStair];
resultData.type1.adiff        = [allBlocks.resultDataAngle.type1.adiff];

% set (only) here the condition for clean trials
resultData.cleanIndexes           = find(resultData.type1.RT <= 8 & resultData.type1.RT >= 0.3 & ~resultData.type1.errorTrials(1:length(resultData.type1.RT)));
resultData.type1.blindTrials      = resultData.type1.blindTrials(1:length(resultData.type1.RT)); % just for the cases when the legth of the 
resultData.type1.blindTrialsClean = resultData.type1.blindTrials(resultData.cleanIndexes); 


end