function [crossing, diff] = filter_crossing(angleRange, speedRange, targetHit, distanceToTarget, angle, velActual, velAlt)

angle_index = discretize(angle, angleRange);
velActual_index = discretize(velActual, speedRange);
velAlt_index = discretize(velAlt, speedRange);

crossing = zeros(size(angle));
diff = zeros(size(angle));
for t = 1:length(angle)
    if ~isnan(angle_index(t)) && ~isnan(velActual_index(t)) && ~isnan(velAlt_index(t))
        if targetHit(velActual_index(t), angle_index(t))~=targetHit(velAlt_index(t), angle_index(t))
            crossing(t) = 1;
            diff(t) = distanceToTarget(velActual_index(t), angle_index(t))-distanceToTarget(velAlt_index(t), angle_index(t));
        end
    end
end

end