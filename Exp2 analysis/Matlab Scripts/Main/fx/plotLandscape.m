function plotLandscape(SkitSet, angleRange, speedRange, distanceToTarget, targetHit)

imagesc(angleRange, speedRange, 100*distanceToTarget);
ax = gca;
ax.YDir = 'normal';
xlabel('release Angle [deg]')
ylabel('speed [m/sec]')
c = colorbar;
ylabel(c, 'min distance to target [cm]')
title(['xTarget = ' num2str(SkitSet.xTarget *100) ' cm, yTarget = ' num2str(SkitSet.yTarget *100) ])
colormap(flipud(gray))
%overlay a white line if the 
hold on
hitHandle = imagesc(angleRange, speedRange, targetHit);
alpha = targetHit;
set(hitHandle,'AlphaData', alpha);

end
