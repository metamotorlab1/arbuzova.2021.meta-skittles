function resultData=get_Data_Angles(allBlocks)


resultData.type1.errorTrials  = [allBlocks.resultDataAngle.type1.errorTrials];
resultData.type1.response     = [allBlocks.resultDataAngle.type1.response];
resultData.type1.chosenLever  = [allBlocks.resultDataAngle.type1.chosenLeverPosition];
resultData.type1.RT           = [allBlocks.resultDataAngle.type1.RT];
resultData.type1.correct      = [allBlocks.resultDataAngle.type1.correct];
resultData.type1.blindTrials  = [allBlocks.resultDataAngle.block.blindTrials];
resultData.type1.blindTrials  = resultData.type1.blindTrials( : )';

resultData.type2.conf         = [allBlocks.resultDataAngle.type2.conf];
resultData.type1.whichStair   = [allBlocks.resultDataAngle.type1.whichStair];
resultData.type1.adiff        = [allBlocks.resultDataAngle.type1.adiff];
BRPs                          = [allBlocks.resultDataAngle.block.BRP];
BRPs_alt                      = [allBlocks.resultDataAngle.block.BRP_alt];

% Loop for leverPosition (didn't find a better way for this structure)
resultData.type1.leverPosition = [];
for l=1:length(allBlocks.resultDataAngle.type1)
    resultData.type1.leverPosition = [resultData.type1.leverPosition allBlocks.resultDataAngle.type1(l).leverPosition.actualAngle(1:length(allBlocks.resultDataAngle.type1(1).response))'];
end

resultData.posthit            = [BRPs.posthit];
resultData.posthitAfterCross  = [BRPs.posthitAfterCross];
resultData.velRelease  = [BRPs.v];


resultData.angleRelease       = [BRPs.angle];
resultData.angleRelease_alt   = [BRPs_alt.angle];

% set (only) here the condition for clean trials
resultData.cleanIndexes           = find(resultData.type1.RT <= 8 & resultData.type1.RT >= 0.3 & ~resultData.type1.errorTrials(1:length(resultData.type1.RT)));
resultData.type1.responseClean    = resultData.type1.response(resultData.cleanIndexes);
resultData.type1.chosenLeverClean      = resultData.type1.chosenLever(resultData.cleanIndexes);
resultData.type1.RTclean          = resultData.type1.RT(resultData.cleanIndexes);
resultData.type1.correctClean     = resultData.type1.correct(resultData.cleanIndexes);
resultData.type1.leverPositionClean     = resultData.type1.leverPosition(resultData.cleanIndexes);
resultData.type2.confClean        = resultData.type2.conf(resultData.cleanIndexes);
resultData.type1.blindTrials      = resultData.type1.blindTrials(1:length(resultData.type1.RT)); % just for the cases when the legth of the 
resultData.type1.blindTrialsClean = resultData.type1.blindTrials(resultData.cleanIndexes); 
resultData.posthitClean           = resultData.posthit(resultData.cleanIndexes);
resultData.posthitAfterCrossClean = resultData.posthitAfterCross(resultData.cleanIndexes);
resultData.adiffClean             = resultData.type1.adiff(resultData.cleanIndexes);

%% visual condition

resultData.visual.type1.errorTrials     = [allBlocks.resultDataAngle.visual.type1.errorTrials];
resultData.visual.type1.response        = [allBlocks.resultDataAngle.visual.type1.response];
resultData.visual.type1.chosenLever     = [allBlocks.resultDataAngle.visual.type1.chosenLeverPosition];
resultData.visual.type1.correct         = [allBlocks.resultDataAngle.visual.type1.correct];
resultData.visual.type2.conf            = [allBlocks.resultDataAngle.visual.type2.conf];
resultData.visual.type1.RT              = [allBlocks.resultDataAngle.visual.type1.RT];
resultData.visual.type1.adiff           = [allBlocks.resultDataAngle.visual.type1.adiff];
BRPs                                    = [allBlocks.resultDataAngle.visual.movementData.BRP];
resultData.visual.posthit               = [BRPs.posthit];
resultData.visual.posthitAfterCross     = [BRPs.posthitAfterCross];
resultData.visual.velRelease            = [BRPs.v];
BRPs_alt                                = [allBlocks.resultDataAngle.visual.movementData.BRP_alt];

resultData.visual.angleRelease          = [BRPs.angle];
resultData.visual.angleRelease_alt      = [BRPs_alt.angle];

% Loop for leverPosition (didn't find a better way for this structure)
resultData.visual.type1.leverPosition = [];
for l=1:length(allBlocks.resultDataAngle.visual.type1)
    resultData.visual.type1.leverPosition = [resultData.visual.type1.leverPosition allBlocks.resultDataAngle.visual.type1(l).leverPosition.actualAngle(1:length(allBlocks.resultDataAngle.visual.type1(1).response))'];
end


% % work around for old data in else blocks
% if isfield(allBlocks.resultData.visual.type1,'unequalPosthit')
%     resultData.visual.type1.unequalPosthit = [allBlocks.resultData.visual.type1.unequalPosthit];
% else
%     resultData.visual.type1.unequalPosthit = ([BRPs.posthit]&~[BRPs_alt.posthit])|(~[BRPs.posthit]&[BRPs_alt.posthit]);
%     warning('no unequal posthits in visual condition')
% end
% 
% if isfield(allBlocks.resultData.visual.type1,'TrajectoriesDifferentDirections')
%     resultData.visual.type1.differentDirections   = [allBlocks.resultData.visual.type1.TrajectoriesDifferentDirections];
% else
%     warning('no trajectories different directions in visual condition')
%     resultData.visual.type1.differentDirections   =([BRPs.v]>0 & [BRPs_alt.v]<0) | ([BRPs.v]<0 & [BRPs_alt.v]>0);
% end

%TODO: clean indexes
% set (only) here the condition for clean trials
resultData.visual.cleanIndexes           = find(resultData.visual.type1.RT <= 8 & resultData.visual.type1.RT >= 0.3 & ~resultData.visual.type1.errorTrials(1:length(resultData.visual.type1.RT)));
resultData.visual.type1.responseClean    = resultData.visual.type1.response(resultData.visual.cleanIndexes);
resultData.visual.type1.chosenLeverClean = resultData.visual.type1.chosenLever(resultData.visual.cleanIndexes);
resultData.visual.type1.RTclean          = resultData.visual.type1.RT(resultData.visual.cleanIndexes);
resultData.visual.type1.correctClean     = resultData.visual.type1.correct(resultData.visual.cleanIndexes);
resultData.visual.type1.leverPositionClean    = resultData.visual.type1.leverPosition(resultData.visual.cleanIndexes);
resultData.visual.type2.confClean        = resultData.visual.type2.conf(resultData.visual.cleanIndexes);
resultData.visual.posthitClean           = resultData.visual.posthit(resultData.visual.cleanIndexes);
resultData.visual.posthitAfterCrossClean = resultData.visual.posthitAfterCross(resultData.visual.cleanIndexes);
resultData.visual.adiffClean             = resultData.visual.type1.adiff(resultData.visual.cleanIndexes);

end