function [allSubjs] = step1_describeSkittles_Traj(subjectNames, user, make_plots, exclusion, adjustment)


global resultData
clear resultData

switch user
    case {'office'}
        resultDir=['~/Dropbox/Data/Skittles_Visual/'];
        userpath=['~/Dropbox/HiWis/Lukas/'];
    case {'elisa'}
        userpath='~/git/SkittlesMatlabLJ/';
        resultDir='~/Dropbox/2_juniorGroup_BCCN/_metamotorlab/Data/Skittles_Visual/';
    case {'polina'}
        userpath=['~/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/'];
        resultDir=['~/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/DataTest'];
    case {'lab'}
        userpath=['~/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/'];
        resultDir=['~/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/DataTest'];
end 
addpath(strcat(userpath, 'Analysis/fx'));
addpath(strcat(userpath, 'BasicVariables'));
addpath(strcat(userpath, 'getCollisionLandscape/functions/'));


cd(resultDir)

% subjectsnames has to be a cell like this: 
% subjectNames = {'030VS','060RK','130MK','150RB'};

% if left empty a list of all folders with 5 letters length is created 
if isempty(subjectNames)
 subjectNames= dir;
 subjectNames={subjectNames.name};
 for i=1:length(subjectNames)
     %delete folders that have not 5 letter names
     if length(subjectNames{length(subjectNames)-i+1})~=5; subjectNames{length(subjectNames)-i+1}=[]; end;
 end
 subjectNames=subjectNames(~cellfun('isempty',subjectNames));
end

subplotSet.rows = 4;
subplotSet.cols = 4;

for subj = 1:length(subjectNames)
    
    allBlocks = load([resultDir filesep subjectNames{subj} filesep 'Trajectories' filesep 'SkittlesResult_' subjectNames{subj} '_Traj_block_5.mat']);
    
     if make_plots == 1 || make_plots == 2
        figHandle = figure('Name',subjectNames{subj},'NumberTitle','off'); hold on
    else
        figHandle = figure('visible','off');
    end
    subjectNames{subj} % this is just printing the current subjectName to check how far we are in the analyses
    
     %Get the landscape description once (it takes time and it will be the same
     %for all subjects)
     if subj == 1
         SkitSet = allBlocks.resultDataTraj.SkitSet;
         [distanceToTarget, angleRange, speedRange, targetHit] = getLandscape(SkitSet);
     end
    
    % fill the data into resultData
    resultData =   get_Data_Traj(allBlocks);
    allSubjs.nSlowTrials(subj)        = length(resultData.type1.RT) - length(resultData.cleanIndexes);
    allSubjs.nSlowTrialsVisual(subj)  = length(resultData.visual.type1.RT) - length(resultData.visual.cleanIndexes);
    

    %% calculate and plot stuff
    
    %Overlay the velocity pairs for each trial on the landscape
    
    visibleTrials           = ~resultData.type1.blindTrials;
    blindTrials             = logical(resultData.type1.blindTrials);
    angle_visibleTrials     = resultData.angleRelease(visibleTrials);
    angle_blindTrials       = resultData.angleRelease(blindTrials);
    angle_visualTrials      = resultData.visual.angleRelease;
    velActual_VisibleTrials = resultData.velRelease_actual(visibleTrials);    
    velAlt_VisibleTrials    = resultData.velRelease_alt(visibleTrials);
    velActual_blindTrials   = resultData.velRelease_actual(blindTrials);
    velAlt_blindTrials      = resultData.velRelease_alt(blindTrials);
    velActual_visualTrials  = resultData.visual.velRelease_actual;
    velAlt_visualTrials     = resultData.visual.velRelease_alt;
    
    % For the unequal target hit analysis
    angle_visuomotor_motorTrials        = resultData.angleRelease(visibleTrials | blindTrials);
    velActual_visuomotor_motorTrials    = resultData.velRelease_actual(visibleTrials | blindTrials);
    velAlt_visuomotor_motorTrials       = resultData.velRelease_alt(visibleTrials | blindTrials);
        
    if make_plots == 2 % for landscape plots
        figure('units','normalized','outerposition',[0 0 1 .5], 'Name',subjectNames{subj}); hold on
        subplot(1,3,1);
        plotLandscape(SkitSet, angleRange, speedRange, distanceToTarget, targetHit);
    end
    
     
    [crossing_visual, ~] = filter_crossing(angleRange, speedRange, targetHit, distanceToTarget, angle_visualTrials, velActual_visualTrials, velAlt_visualTrials);
    allSubjs.crossPercent_visual(subj) = sum(crossing_visual)/length(crossing_visual)*100;
    
    if make_plots == 2
        for t = 1:length(vel_visibleTrials)
            if ~crossing_visual(t)
                plot([vel_visualTrials(t) vel_visualTrials(t)], [angleActual_visualTrials(t) angleAlt_visualTrials(t)], 'g-');
            else
                plot([vel_visualTrials(t) vel_visualTrials(t)], [angleActual_visualTrials(t) angleAlt_visualTrials(t)], 'r-');
            end
        end
        title('visual')
    end
    
    try
        if make_plots == 1 || make_plots == 2
            subplotSet.position = [1 5];
            getStaircase(figHandle, subplotSet,resultData)
        end
        
        
            subplotSet.position = [2 6];
            allSubjs.percentCorrect(subj,:) = getPercentCorrect(figHandle, subplotSet,resultData, make_plots);
            
        if make_plots == 1 || make_plots == 2
            subplotSet.position = [9 11];
            getReactionTimes(figHandle, subplotSet,resultData)
            
            subplotSet.position = [13 15];
            getConfidenceDistributions(figHandle, subplotSet,resultData)
            
            subplotSet.position = [3 7];
            confVsRT(figHandle, subplotSet,resultData)
            
            subplotSet.position = [12 16];
            
        end
    
        
               
    [allSubjs.SSEfit_nonBlind(subj), allSubjs.SSEfit_blind(subj), allSubjs.SSEfit_visual(subj), allSubjs.SSEfit_nonBlind_2bins(subj), allSubjs.SSEfit_blind_2bins(subj), allSubjs.SSEfit_visual_2bins(subj), resultData, allSubjs.nR_S1_nonBlind{subj}, allSubjs.nR_S2_nonBlind{subj}, allSubjs.nR_S1_blind{subj}, allSubjs.nR_S2_blind{subj}, allSubjs.nR_S1_visual{subj}, allSubjs.nR_S2_visual{subj}] = metadPrime(figHandle, subplotSet,resultData, crossing_visual, make_plots, exclusion, adjustment);
     
    subplotSet.position = [4 8];
    
    [allSubjs.AUROC2_nonBlind(subj), allSubjs.AUROC2_blind(subj), allSubjs.AUROC2_visual(subj),resultData] = Aroc(figHandle, subplotSet,resultData, make_plots);
    
    catch e
        e.message
        % warning(['error message in ' subjectNames(subj) ': ' e.message])
    end
    
    %%
    % For mean confidence
    allSubjs.conf_vm{subj} = resultData.type2.conf(visibleTrials);
    allSubjs.conf_m{subj} = resultData.type2.conf(blindTrials);
    allSubjs.conf_v{subj} = resultData.visual.type2.conf;
    
    allSubjs.mean_conf_vm(subj) = nanmean(allSubjs.conf_vm{subj});
    allSubjs.mean_conf_m(subj) = nanmean(allSubjs.conf_m{subj});
    allSubjs.mean_conf_v(subj) = nanmean(allSubjs.conf_v{subj});
    
    % ... and for the median
    allSubjs.median_conf_vm(subj) = nanmedian(allSubjs.conf_vm{subj});
    allSubjs.median_conf_m(subj) = nanmedian(allSubjs.conf_m{subj});
    allSubjs.median_conf_v(subj) = nanmedian(allSubjs.conf_v{subj});
    

    
end
    % For HMeta-d' estimation
    % % Transpose contents of cells
    allSubjs.nR_S1_nonBlind = cellfun(@transpose, allSubjs.nR_S1_nonBlind,'un',0);
    allSubjs.nR_S2_nonBlind = cellfun(@transpose, allSubjs.nR_S2_nonBlind,'un',0);
    allSubjs.nR_S1_blind = cellfun(@transpose, allSubjs.nR_S1_blind,'un',0);
    allSubjs.nR_S2_blind = cellfun(@transpose, allSubjs.nR_S2_blind,'un',0);
    allSubjs.nR_S1_visual = cellfun(@transpose, allSubjs.nR_S1_visual,'un',0);
    allSubjs.nR_S2_visual = cellfun(@transpose, allSubjs.nR_S2_visual,'un',0);
end



%% functions
function getStaircase(figHandle, subplotSet,resultData)

staircaseColours = 'rbg';

figure(figHandle)

subplot(subplotSet.rows,subplotSet.cols,subplotSet.position); hold on
title('staircase')

for s = 1:max(resultData.type1.whichStair)
    plot(abs( resultData.type1.vdiff(resultData.type1.whichStair==s)), [staircaseColours(s) '-o'])
end
plot(abs(resultData.visual.type1.vdiff), [staircaseColours(3) '-o'])

xlabel('trial')
ylabel('v0 difference')

end


%%
function percentCorrect = getPercentCorrect(figHandle, subplotSet, resultData, make_plots)

if make_plots == 1 || make_plots == 2
    figure(figHandle)
    subplot(subplotSet.rows,subplotSet.cols,subplotSet.position); hold on
    title('type1 task')
    b = bar([sum(~resultData.type1.blindTrialsClean & resultData.type1.correctClean) /sum(~resultData.type1.blindTrialsClean), ...
        sum(resultData.type1.blindTrialsClean  & resultData.type1.correctClean) /sum(resultData.type1.blindTrialsClean),...
        sum(resultData.visual.type1.correctClean)/length(resultData.visual.type1.correctClean);...
        sum(~resultData.type1.blindTrialsClean & ~resultData.type1.correctClean)/sum(~resultData.type1.blindTrialsClean),...
        sum(resultData.type1.blindTrialsClean  & ~resultData.type1.correctClean)/sum(resultData.type1.blindTrialsClean),...
        sum(resultData.visual.type1.correctClean==0)/length(resultData.visual.type1.correctClean)]);
    b(1).FaceColor = 'r';
    b(2).FaceColor = 'b';
    b(3).FaceColor = 'g';
    set(gca, 'XTickLabel', {'correct', 'incorrect'}, 'XTick', 1:2)
    legend('visuomotor trials', 'motor trials','visual trials')
    ylabel('% trials')
else
    figHandle = figure('visible','off');
end

percentCorrect = [sum(~resultData.type1.blindTrialsClean & resultData.type1.correctClean)/...
    sum(~resultData.type1.blindTrialsClean)*100, ...
    sum(resultData.type1.blindTrialsClean & resultData.type1.correctClean)/...
    sum(resultData.type1.blindTrialsClean)*100, ...
    sum(resultData.visual.type1.correctClean)/length(resultData.visual.type1.correctClean)*100];
end


%%
function getReactionTimes(figHandle, subplotSet,resultData)

figure(figHandle)

subplot(subplotSet.rows,subplotSet.cols,subplotSet.position(1)); hold on
title('Non-blind trials')
nbins = 10;
histogram(resultData.type1.RTclean(resultData.type1.correctClean>0 & ~resultData.type1.blindTrialsClean), min(resultData.type1.RTclean(~resultData.type1.blindTrialsClean)):range(resultData.type1.RTclean(~resultData.type1.blindTrialsClean))/nbins:max(resultData.type1.RTclean(~resultData.type1.blindTrialsClean)), 'FaceColor', [0 .5 0]);
histogram(resultData.type1.RTclean(resultData.type1.correctClean<1 & ~resultData.type1.blindTrialsClean), min(resultData.type1.RTclean(~resultData.type1.blindTrialsClean)):range(resultData.type1.RTclean(~resultData.type1.blindTrialsClean))/nbins:max(resultData.type1.RTclean(~resultData.type1.blindTrialsClean)), 'FaceColor', [.5 0 0]);
legend('correct', 'incorrect')
xlabel('type1 RT (sec)')
ylabel('count')

subplot(subplotSet.rows,subplotSet.cols,subplotSet.position(1)+1); hold on
title('Blind trials')
nbins = 10;
histogram(resultData.type1.RTclean(resultData.type1.correctClean>0 & resultData.type1.blindTrialsClean), min(resultData.type1.RTclean(logical(resultData.type1.blindTrialsClean))):range(resultData.type1.RTclean(logical(resultData.type1.blindTrialsClean)))/nbins:max(resultData.type1.RTclean(logical(resultData.type1.blindTrialsClean))), 'FaceColor', [0 .5 0]);
histogram(resultData.type1.RTclean(resultData.type1.correctClean<1 & resultData.type1.blindTrialsClean), min(resultData.type1.RTclean(logical(resultData.type1.blindTrialsClean))):range(resultData.type1.RTclean(logical(resultData.type1.blindTrialsClean)))/nbins:max(resultData.type1.RTclean(logical(resultData.type1.blindTrialsClean))), 'FaceColor', [.5 0 0]);
xlabel('type1 RT (sec)')

subplot(subplotSet.rows,subplotSet.cols,subplotSet.position(2)); hold on
title('Visual trials')
nbins = 10;
histogram(resultData.visual.type1.RTclean(resultData.visual.type1.correctClean>0), min(resultData.visual.type1.RTclean):range(resultData.visual.type1.RTclean)/nbins:max(resultData.visual.type1.RTclean), 'FaceColor', [0 .5 0]);
histogram(resultData.visual.type1.RTclean(resultData.visual.type1.correctClean<1), min(resultData.visual.type1.RTclean):range(resultData.visual.type1.RTclean)/nbins:max(resultData.visual.type1.RTclean), 'FaceColor', [.5 0 0]);
xlabel('type1 RT (sec)')

end

%%
function getConfidenceDistributions(figHandle, subplotSet,resultData)

figure(figHandle)

subplot(subplotSet.rows,subplotSet.cols,subplotSet.position(1)); hold on
title('Visuomotor trials')
histogram(resultData.type2.confClean(resultData.type1.correctClean>0 & ~resultData.type1.blindTrialsClean), 1:1:6, 'FaceColor', [0 .5 0]);
histogram(resultData.type2.confClean(resultData.type1.correctClean<1 & ~resultData.type1.blindTrialsClean), 1:1:6, 'FaceColor', [.5 0 0]);
legend('correct', 'incorrect')
xlabel('confidence')
ylabel('count')

subplot(subplotSet.rows,subplotSet.cols,subplotSet.position(1)+1); hold on
title('Motor trials')
histogram(resultData.type2.confClean(resultData.type1.correctClean>0 & resultData.type1.blindTrialsClean), 1:1:6, 'FaceColor', [0 .5 0]);
histogram(resultData.type2.confClean(resultData.type1.correctClean<1 & resultData.type1.blindTrialsClean), 1:1:6, 'FaceColor', [.5 0 0]); 
xlabel('confidence')

subplot(subplotSet.rows,subplotSet.cols,subplotSet.position(2)); hold on
title('Visual trials')
histogram(resultData.visual.type2.confClean(resultData.visual.type1.correctClean>0), 1:1:6, 'FaceColor', [0 .5 0]);
histogram(resultData.visual.type2.confClean(resultData.visual.type1.correctClean<1), 1:1:6, 'FaceColor', [.5 0 0]);
xlabel('confidence')

end

%%
function confVsRT(figHandle, subplotSet,resultData)

figure(figHandle)

subplot(subplotSet.rows,subplotSet.cols,subplotSet.position); hold on
title('Confidence vs type1RT')
scatter(resultData.type1.RTclean(~resultData.type1.blindTrialsClean), ...
    resultData.type2.confClean(~resultData.type1.blindTrialsClean), 'r')
lsline
scatter(resultData.type1.RTclean(logical(resultData.type1.blindTrialsClean)), ...
    resultData.type2.confClean(logical(resultData.type1.blindTrialsClean)), 'b')
lsline
scatter(resultData.visual.type1.RTclean, ...
    resultData.visual.type2.confClean, 'g')
lsline
xlabel('RT (secs)')
ylabel('confidence')


end

%%
function [SSEfit_nonBlind, SSEfit_blind, SSEfit_visual, SSEfit_nonBlind_2bins, SSEfit_blind_2bins, SSEfit_visual_2bins, resultData, nR_S1_nonBlind, nR_S2_nonBlind, nR_S1_blind, nR_S2_blind, nR_S1_visual, nR_S2_visual] = metadPrime(figHandle, subplotSet,resultData, crossing_visual, make_plots, exclusion, adjustment)


if make_plots == 1 || make_plots == 2
    figure(figHandle)
else
    figHandle = figure('visible','off');
end

Nratings = 6; % first run type2_SDT_SSE with 2 bins to find people with 0 or 1 in FAR2/HR2

% Binning for NONBLIND (aka VISUOMOTOR)
confvector_nonblind = resultData.type2.confClean(~resultData.type1.blindTrialsClean);

% Binning for BLIND (aka MOTOR)
confvector_blind = resultData.type2.confClean(logical(resultData.type1.blindTrialsClean));

% Binning for VISUAL:
confvector_visual = resultData.visual.type2.confClean;

% Get the clean VDIFF 
% for nonblind and blind (aka visomotor and motor)
resultData.type1.vdiffClean = resultData.type1.vdiff(resultData.cleanIndexes); 
% for visual
resultData.visual.type1.vdiffClean = resultData.visual.type1.vdiff(resultData.visual.cleanIndexes);

%% 
% I take arbitrarily resultData.type1.vdiff<0 to be S1 and >0 to be
% S2, think if we want to define this differently- TODO: Maybe, given
% the restriction to not have the two trajectories as one hitting the
% post and the other one not at all


% now the data is divided in the groups posthit and no-posthit(or posthit after cross)

nR_S1_nonBlind = zeros(2*Nratings,1);
nR_S2_nonBlind = zeros(2*Nratings,1);
nR_S1_blind    = zeros(2*Nratings,1);
nR_S2_blind    = zeros(2*Nratings,1);
nR_S1_visual   = zeros(2*Nratings,1);
nR_S2_visual   = zeros(2*Nratings,1);

response_vm = [];
stimID_vm = [];
response_m = [];
stimID_m = [];
response_v = [];
stimID_v = [];

response_button_VM = resultData.type1.responseClean(find(~resultData.type1.blindTrialsClean));
response_button_M = resultData.type1.responseClean(find(resultData.type1.blindTrialsClean));
vdiff_displayed_VM = resultData.vdiffClean(find(~resultData.type1.blindTrialsClean));
vdiff_displayed_M = resultData.vdiffClean(find(resultData.type1.blindTrialsClean));

% VISUOMOTOR
for m = 1:length(response_button_VM)
    if response_button_VM{m} == 'x'
        response_vm(m) = 0;
    else
        response_vm(m) = 1;
    end
    if vdiff_displayed_VM(m) > 0
        stimID_vm(m) = 0;
    else 
        stimID_vm(m) = 1;
    end
end
% MOTOR
for m = 1:length(response_button_M)
    if response_button_M{m} == 'x'
        response_m(m) = 0;
    else
        response_m(m) = 1;
    end
    if vdiff_displayed_M(m) > 0
        stimID_m(m) = 0;
    else 
        stimID_m(m) = 1;
    end
end

% VISUAL
for m = 1:length(resultData.visual.type1.responseClean)
    if resultData.visual.type1.responseClean{m} == 'x'
        response_v(m) = 0;
    else
        response_v(m) = 1;
    end
    if resultData.visual.vdiffClean(m) > 0
        stimID_v(m) = 0;
    else 
        stimID_v(m) = 1;
    end
end
    [nR_S1_VM, nR_S2_VM] = trials2counts(stimID_vm, response_vm, confvector_nonblind, Nratings, 0, 0)
    [nR_S1_M, nR_S2_M] = trials2counts(stimID_m, response_m, confvector_blind, Nratings, 0, 0)
    [nR_S1_V, nR_S2_V] = trials2counts(stimID_v, response_v, confvector_visual, Nratings, 0, 0)

for i = 1:Nratings
    nR_S1_nonBlind(i)          = sum(resultData.type1.correctClean(~resultData.type1.blindTrialsClean)  & resultData.vdiffClean(~resultData.type1.blindTrialsClean)>0 & confvector_nonblind == Nratings+1-i);
    nR_S1_nonBlind(Nratings+i) = sum(~resultData.type1.correctClean(~resultData.type1.blindTrialsClean)  &  resultData.vdiffClean(~resultData.type1.blindTrialsClean)>0 & confvector_nonblind == i);
    nR_S2_nonBlind(Nratings+i) = sum(resultData.type1.correctClean(~resultData.type1.blindTrialsClean) & resultData.vdiffClean(~resultData.type1.blindTrialsClean)<=0 & confvector_nonblind == i);
    nR_S2_nonBlind(i)          = sum(~resultData.type1.correctClean(~resultData.type1.blindTrialsClean)  & resultData.vdiffClean(~resultData.type1.blindTrialsClean)<=0 & confvector_nonblind == Nratings+1-i);
    
    nR_S1_blind(i)             = sum(resultData.type1.correctClean(logical(resultData.type1.blindTrialsClean))  &  resultData.vdiffClean(logical(resultData.type1.blindTrialsClean))>0 & confvector_blind == Nratings+1-i);
    nR_S1_blind(Nratings+i)    = sum(~resultData.type1.correctClean(logical(resultData.type1.blindTrialsClean))  &  resultData.vdiffClean(logical(resultData.type1.blindTrialsClean))>0 & confvector_blind == i);
    nR_S2_blind(Nratings+i)    = sum(resultData.type1.correctClean(logical(resultData.type1.blindTrialsClean)) & resultData.vdiffClean(logical(resultData.type1.blindTrialsClean))<=0 & confvector_blind == i);
    nR_S2_blind(i)             = sum(~resultData.type1.correctClean(logical(resultData.type1.blindTrialsClean))  & resultData.vdiffClean(logical(resultData.type1.blindTrialsClean))<=0 & confvector_blind == Nratings+1-i);
    
    nR_S1_visual(i)            = sum(resultData.visual.type1.correctClean  & resultData.visual.vdiffClean>0 & confvector_visual == Nratings+1-i);
    nR_S1_visual(Nratings+i)   = sum(~resultData.visual.type1.correctClean & resultData.visual.vdiffClean>0 & confvector_visual == i);
    nR_S2_visual(Nratings+i)   = sum(resultData.visual.type1.correctClean & resultData.visual.vdiffClean<=0 & confvector_visual == i);
    nR_S2_visual(i)            = sum(~resultData.visual.type1.correctClean & resultData.visual.vdiffClean<=0 & confvector_visual == Nratings+1-i);
end


'Visuomotor'
nR_S1_VM
nR_S1_nonBlind'
nR_S2_VM
nR_S2_nonBlind'
'Motor'
nR_S1_M
nR_S1_blind'
nR_S2_M
nR_S2_blind'
'Visual'
nR_S1_V
nR_S1_visual'
nR_S2_V
nR_S2_visual'



% use the special type2_SDT_SSE_2bins because we need to figure out the
% outliers in the data --> we exclude people if they have 1 or 0 in FAR2 or
% HR2 later (if exclusion = 1)

% concatenate halves for N=2
nR_S1_nonBlind = [sum(nR_S1_nonBlind(1:3)) sum(nR_S1_nonBlind(4:6)) sum(nR_S1_nonBlind(7:9)) sum(nR_S1_nonBlind(10:12))]';
nR_S2_nonBlind = [sum(nR_S2_nonBlind(1:3)) sum(nR_S2_nonBlind(4:6)) sum(nR_S2_nonBlind(7:9)) sum(nR_S2_nonBlind(10:12))]';
nR_S1_blind = [sum(nR_S1_blind(1:3)) sum(nR_S1_blind(4:6)) sum(nR_S1_blind(7:9)) sum(nR_S1_blind(10:12))]';
nR_S2_blind = [sum(nR_S2_blind(1:3)) sum(nR_S2_blind(4:6)) sum(nR_S2_blind(7:9)) sum(nR_S2_blind(10:12))]';
nR_S1_visual = [sum(nR_S1_visual(1:3)) sum(nR_S1_visual(4:6)) sum(nR_S1_visual(7:9)) sum(nR_S1_visual(10:12))]';
nR_S2_visual = [sum(nR_S2_visual(1:3)) sum(nR_S2_visual(4:6)) sum(nR_S2_visual(7:9)) sum(nR_S2_visual(10:12))]';

SSEfit_nonBlind_2bins = type2_SDT_SSE_2bins(nR_S1_nonBlind, nR_S2_nonBlind); %requires optimization toolbox
SSEfit_blind_2bins    = type2_SDT_SSE_2bins(nR_S1_blind, nR_S2_blind);
SSEfit_visual_2bins   = type2_SDT_SSE_2bins(nR_S1_visual, nR_S2_visual);

% (re)define nRatings now to 6 bins and run everything again, overwrite
% existing nR_S1_nonBlind...

Nratings = 6; %define how many confidence levels we want


% Binning for NONBLIND (aka VISUOMOTOR)
confvector_nonblind = resultData.type2.confClean(~resultData.type1.blindTrialsClean);
% Binning for BLIND (aka MOTOR)
confvector_blind = resultData.type2.confClean(logical(resultData.type1.blindTrialsClean));
% Binning for VISUAL:
confvector_visual = resultData.visual.type2.confClean;


nR_S1_nonBlind = zeros(2*Nratings,1);
nR_S2_nonBlind = zeros(2*Nratings,1);
nR_S1_blind    = zeros(2*Nratings,1);
nR_S2_blind    = zeros(2*Nratings,1);
nR_S1_visual   = zeros(2*Nratings,1);
nR_S2_visual   = zeros(2*Nratings,1);
    
for i = 1:Nratings
    nR_S1_nonBlind(i)          = sum(resultData.type1.correctClean(~resultData.type1.blindTrialsClean)  & resultData.vdiffClean(~resultData.type1.blindTrialsClean)>0 & confvector_nonblind == Nratings+1-i);
    nR_S1_nonBlind(Nratings+i) = sum(~resultData.type1.correctClean(~resultData.type1.blindTrialsClean)  &  resultData.vdiffClean(~resultData.type1.blindTrialsClean)>0 & confvector_nonblind == i);
    nR_S2_nonBlind(Nratings+i) = sum(resultData.type1.correctClean(~resultData.type1.blindTrialsClean) & resultData.vdiffClean(~resultData.type1.blindTrialsClean)<=0 & confvector_nonblind == i);
    nR_S2_nonBlind(i)          = sum(~resultData.type1.correctClean(~resultData.type1.blindTrialsClean)  & resultData.vdiffClean(~resultData.type1.blindTrialsClean)<=0 & confvector_nonblind == Nratings+1-i);

    nR_S1_blind(i)             = sum(resultData.type1.correctClean(logical(resultData.type1.blindTrialsClean))  &  resultData.vdiffClean(logical(resultData.type1.blindTrialsClean))>0 & confvector_blind == Nratings+1-i);
    nR_S1_blind(Nratings+i)    = sum(~resultData.type1.correctClean(logical(resultData.type1.blindTrialsClean))  &  resultData.vdiffClean(logical(resultData.type1.blindTrialsClean))>0 & confvector_blind == i);
    nR_S2_blind(Nratings+i)    = sum(resultData.type1.correctClean(logical(resultData.type1.blindTrialsClean)) & resultData.vdiffClean(logical(resultData.type1.blindTrialsClean))<=0 & confvector_blind == i);
    nR_S2_blind(i)             = sum(~resultData.type1.correctClean(logical(resultData.type1.blindTrialsClean))  & resultData.vdiffClean(logical(resultData.type1.blindTrialsClean))<=0 & confvector_blind == Nratings+1-i);

    nR_S1_visual(i)            = sum(resultData.visual.type1.correctClean  & resultData.visual.vdiffClean>0 & confvector_visual == Nratings+1-i);
    nR_S1_visual(Nratings+i)   = sum(~resultData.visual.type1.correctClean & resultData.visual.vdiffClean>0 & confvector_visual == i);
    nR_S2_visual(Nratings+i)   = sum(resultData.visual.type1.correctClean & resultData.visual.vdiffClean<=0 & confvector_visual == i);
    nR_S2_visual(i)            = sum(~resultData.visual.type1.correctClean & resultData.visual.vdiffClean<=0 & confvector_visual == Nratings+1-i);
end

% use the normal type2_SDT_SSE function to calculte output. In case of
% adjustment, add the adjustment factor

if adjustment == 1
    adj_f = 1/length(nR_S1_nonBlind);
    
    nR_S1_nonBlind_adj = nR_S1_nonBlind + adj_f;
    nR_S2_nonBlind_adj = nR_S2_nonBlind + adj_f;
    
    nR_S1_blind_adj    = nR_S1_blind + adj_f;
    nR_S2_blind_adj    = nR_S2_blind + adj_f;
    
    nR_S1_visual_adj   = nR_S1_visual + adj_f;
    nR_S2_visual_adj   = nR_S2_visual + adj_f;
    
    SSEfit_nonBlind = type2_SDT_SSE(nR_S1_nonBlind_adj, nR_S2_nonBlind_adj); %requires optimization toolbox
    SSEfit_blind    = type2_SDT_SSE(nR_S1_blind_adj, nR_S2_blind_adj);
    SSEfit_visual   = type2_SDT_SSE(nR_S1_visual_adj, nR_S2_visual_adj);
else    
    SSEfit_nonBlind = type2_SDT_SSE(nR_S1_nonBlind, nR_S2_nonBlind); %requires optimization toolbox
    SSEfit_blind    = type2_SDT_SSE(nR_S1_blind, nR_S2_blind);
    SSEfit_visual   = type2_SDT_SSE(nR_S1_visual, nR_S2_visual);
end
    
% check FAR2 output of 2bins calculation and put NaN where there are 0s or
% 1s in FAR2 or HR2
% only do this in case of exclusion of weird values (exclusion = 1)

if exclusion == 1
    if SSEfit_nonBlind_2bins.FAR2_rS1 == 0 || SSEfit_nonBlind_2bins.FAR2_rS1 == 1 || SSEfit_nonBlind_2bins.FAR2_rS2 == 0 || SSEfit_nonBlind_2bins.FAR2_rS2 == 1 || SSEfit_nonBlind_2bins.HR2_rS1 == 0 || SSEfit_nonBlind_2bins.HR2_rS1 == 1 || SSEfit_nonBlind_2bins.HR2_rS2 == 0 || SSEfit_nonBlind_2bins.HR2_rS2 == 1 

        SSEfit_nonBlind.da        = NaN;
        SSEfit_nonBlind.meta_da   = NaN;
        SSEfit_nonBlind.M_ratio   = NaN;
        SSEfit_nonBlind.M_diff    = NaN;

    end

    if SSEfit_blind_2bins.FAR2_rS1 == 0 || SSEfit_blind_2bins.FAR2_rS1 == 1 || SSEfit_blind_2bins.FAR2_rS2 == 0 || SSEfit_blind_2bins.FAR2_rS2 == 1 || SSEfit_nonBlind_2bins.HR2_rS1 == 0 || SSEfit_nonBlind_2bins.HR2_rS1 == 1 || SSEfit_nonBlind_2bins.HR2_rS2 == 0 || SSEfit_nonBlind_2bins.HR2_rS2 == 1 

        SSEfit_blind.da        = NaN;
        SSEfit_blind.meta_da   = NaN;
        SSEfit_blind.M_ratio   = NaN;
        SSEfit_blind.M_diff    = NaN;

    end    

    if SSEfit_visual_2bins.FAR2_rS1 == 0 || SSEfit_visual_2bins.FAR2_rS1 == 1 || SSEfit_visual_2bins.FAR2_rS2 == 0 || SSEfit_visual_2bins.FAR2_rS2 == 1 || SSEfit_nonBlind_2bins.HR2_rS1 == 0 || SSEfit_nonBlind_2bins.HR2_rS1 == 1 || SSEfit_nonBlind_2bins.HR2_rS2 == 0 || SSEfit_nonBlind_2bins.HR2_rS2 == 1 

        SSEfit_visual.da        = NaN;
        SSEfit_visual.meta_da   = NaN;
        SSEfit_visual.M_ratio   = NaN;
        SSEfit_visual.M_diff    = NaN;
    end
end


%Plot if it worked
if make_plots == 1||make_plots == 2
    figure(figHandle)
    subplot(subplotSet.rows,subplotSet.cols,subplotSet.position); hold on
    title('Meta-d analysis')
    b = bar([SSEfit_nonBlind.meta_da, SSEfit_blind.meta_da,SSEfit_visual.meta_da;
        SSEfit_nonBlind.da,       SSEfit_blind.da,SSEfit_visual.da;
        SSEfit_nonBlind.M_ratio,  SSEfit_blind.M_ratio,SSEfit_visual.M_ratio;
        SSEfit_nonBlind.M_diff,    SSEfit_blind.M_diff,SSEfit_visual.M_diff]);
    b(1).FaceColor = 'r';
    b(2).FaceColor = 'b';
    b(3).FaceColor = 'g';
    
    set(gca, 'XTickLabel', {'Meta-d', 'd', 'Mratio', 'Mdiff'}, 'XTick', 1:4)
end



end


function [auroc2_nonBlind, auroc2_blind, auroc2_visual,resultData] = Aroc(figHandle, subplotSet,resultData, make_plots)

if make_plots == 1 || make_plots == 2
    figure(figHandle)
else
    figHandle = figure('visible','off');
end

Nratings = 6; %define how many confidence levels we want

% non-blind trials (aka VISUOMOTOR)
confvector = resultData.type2.confClean(~resultData.type1.blindTrialsClean);
resultData.type2.confClean_binned_nonblind = quantileranks(confvector, Nratings);
correct = resultData.type1.correctClean(~resultData.type1.blindTrialsClean);

conf = resultData.type2.confClean_binned_nonblind;
[auroc2_nonBlind, cum_FA2_nonBlind, cum_H2_nonBlind] = type2roc(correct, conf, Nratings);

% blind trials (aka MOTOR)
confvector = resultData.type2.confClean(logical(resultData.type1.blindTrialsClean));
resultData.type2.confClean_binned_blind = quantileranks(confvector, Nratings);
correct = resultData.type1.correctClean(logical(resultData.type1.blindTrialsClean));
conf = resultData.type2.confClean_binned_blind;
[auroc2_blind, cum_FA2_blind, cum_H2_blind] = type2roc(correct, conf, Nratings);

% visual 
confvector = resultData.visual.type2.confClean;
resultData.visual.type2.confClean_binned = quantileranks(confvector, Nratings);

correct = resultData.visual.type1.correctClean;
conf = resultData.visual.type2.confClean_binned;
[auroc2_visual, cum_FA2_visual, cum_H2_visual] = type2roc(correct, conf, Nratings);

if make_plots == 1 || make_plots == 2
    figure(figHandle)
    subplot(subplotSet.rows,subplotSet.cols,subplotSet.position); hold on
    color = 'r'; % VISUOMOTOR - RED
    plot(cum_FA2_nonBlind,cum_H2_nonBlind, color);
    plot(cum_FA2_nonBlind,cum_H2_nonBlind,[color, 'o']);
    color = 'b'; % MOTOR - BLUE
    plot(cum_FA2_blind,cum_H2_blind, color);
    plot(cum_FA2_blind,cum_H2_blind,[color, 'o']);
    color = 'g'; % VISUAL - GREEN
    plot(cum_FA2_visual,cum_H2_visual, color);
    plot(cum_FA2_visual,cum_H2_visual,[color, 'o']);
    axis square
    set(gca,'YLim',[0 1]);
    set(gca,'XLim',[0 1]);
    title ('type 2 ROC')
    xlabel('FA2'); ylabel('HITS2')
end

end
 
function [nR_S1, nR_S2] = trials2counts(stimID, response, rating, nRatings, padCells, padAmount)

% [nR_S1, nR_S2] = trials2counts(stimID, response, rating, nRatings, padCells, padAmount)
%
% Given data from an experiment where an observer discriminates between two
% stimulus alternatives on every trial and provides confidence ratings,
% converts trial by trial experimental information for N trials into response 
% counts.
%
% INPUTS
% stimID:   1xN vector. stimID(i) = 0 --> stimulus on i'th trial was S1.
%                       stimID(i) = 1 --> stimulus on i'th trial was S2.
%
% response: 1xN vector. response(i) = 0 --> response on i'th trial was "S1".
%                       response(i) = 1 --> response on i'th trial was "S2".
%
% rating:   1xN vector. rating(i) = X --> rating on i'th trial was X.
%                       X must be in the range 1 <= X <= nRatings.
%
% N.B. all trials where stimID is not 0 or 1, response is not 0 or 1, or
% rating is not in the range [1, nRatings], are omitted from the response
% count.
%
% nRatings: total # of available subjective ratings available for the
%           subject. e.g. if subject can rate confidence on a scale of 1-4,
%           then nRatings = 4
%
% optional inputs
%
% padCells: if set to 1, each response count in the output has the value of
%           padAmount added to it. Padding cells is desirable if trial counts 
%           of 0 interfere with model fitting.
%           if set to 0, trial counts are not manipulated and 0s may be
%           present in the response count output.
%           default value for padCells is 0.
%
% padAmount: the value to add to each response count if padCells is set to 1.
%            default value is 1/(2*nRatings)
%
%
% OUTPUTS
% nR_S1, nR_S2
% these are vectors containing the total number of responses in
% each response category, conditional on presentation of S1 and S2.
%
% e.g. if nR_S1 = [100 50 20 10 5 1], then when stimulus S1 was
% presented, the subject had the following response counts:
% responded S1, rating=3 : 100 times
% responded S1, rating=2 : 50 times
% responded S1, rating=1 : 20 times
% responded S2, rating=1 : 10 times
% responded S2, rating=2 : 5 times
% responded S2, rating=3 : 1 time
%
% The ordering of response / rating counts for S2 should be the same as it
% is for S1. e.g. if nR_S2 = [3 7 8 12 27 89], then when stimulus S2 was
% presented, the subject had the following response counts:
% responded S1, rating=3 : 3 times
% responded S1, rating=2 : 7 times
% responded S1, rating=1 : 8 times
% responded S2, rating=1 : 12 times
% responded S2, rating=2 : 27 times
% responded S2, rating=3 : 89 times


%% sort inputs

% check for valid inputs
if ~( length(stimID) == length(response) && length(stimID) == length(rating) )
    error('stimID, response, and rating input vectors must have the same lengths')
end

% filter bad trials
f = (stimID == 0 | stimID == 1) & (response == 0 | response == 1) & (rating >=1 & rating <= nRatings);
stimID   = stimID(f);
response = response(f);
rating   = rating(f);


% set input defaults
if ~exist('padCells','var') || isempty(padCells)
    padCells = 0;
end

if ~exist('padAmount','var') || isempty(padAmount)
    padAmount = 1 / (2*nRatings);
end


%% compute response counts

nR_S1 = [];
nR_S2 = [];

% S1 responses
for r = nRatings : -1 : 1 % r iterates over nRatings till 1, with increments of -1 (first iteration when nRatings = 6: 6, second: 5 and so on)
    nR_S1(end+1) = sum(stimID==0 & response==0 & rating==r); % first iteration: first entry of nR_S1 is HIT with highest rating (and so on)
    nR_S2(end+1) = sum(stimID==1 & response==0 & rating==r); % first iteration: first entry of nR_S2 is FA with highest rating 
end

% S2 responses
for r = 1 : nRatings
    nR_S1(end+1) = sum(stimID==0 & response==1 & rating==r); % S1 FA
    nR_S2(end+1) = sum(stimID==1 & response==1 & rating==r); % S2 HIT
end


% pad response counts to avoid zeros
if padCells
    nR_S1 = nR_S1 + padAmount;
    nR_S2 = nR_S2 + padAmount;
end
end