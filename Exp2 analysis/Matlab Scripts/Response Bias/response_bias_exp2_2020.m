%%response bias analysis, Polina Arbuzova 01/2020 (based on script from
%%03/2019)

%
clear all;
%% User

user = 'polina';

switch user
    case {'polina'}
        addpath(genpath('/Users/polinaarbuzova/Dropbox/PhDs/Polina/MatLab Toolboxes/Corr_toolbox_v2'));
        addpath(genpath('/Users/polinaarbuzova/Dropbox/PhDs/Polina/MatLab Toolboxes/jags-3.4.0'));
        saveDir = '/Users/polinaarbuzova/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/Analysis';
        userpath=['~/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/'];
        resultDir=['~/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/DataTest'];
end
addpath(strcat(userpath, 'Analysis/fx'));
addpath(strcat(userpath, 'BasicVariables'));

cd(saveDir);

subjectNames = {'020EN', '021MT', '050MJ', '050MM',  '060NS', '070HN', '080AK', '080MS', ...
    '090MM', '090RH', '091LV', '100AB', '100NZ', '110JA', '120BT', '120RW', '120SY', '120VZ', '130ME', ...
    '131KK', '140BH', '150BR', '151TV', '160AG', '170EP', '170SP', '171HP', '180IV', '181BF', ...
    '210JJ', '211DB', '240KS', '240SI', '240SS', '240TN', '260MR', '271IM', '280SS', '290SR', '291GZ', '300IJ'};

% subjectNames = {'170SP'};

% ANGLES


for subj = 1:length(subjectNames)
    all_responses = [];
    vis_responses = [];
    trial_type = [];
    if exist([resultDir filesep subjectNames{subj} filesep 'Angles' filesep 'SkittlesResult_' subjectNames{subj} '_Angle_block_5.mat'], 'file') == 2
        allBlocks = load([resultDir filesep subjectNames{subj} filesep 'Angles' filesep 'SkittlesResult_' subjectNames{subj} '_Angle_block_5.mat']);
    else
        allBlocks = load([resultDir filesep subjectNames{subj} filesep 'Angles' filesep 'SkittlesResult_' subjectNames{subj} '_Angle_block_4.mat']);
    end
    subjectNames{subj} % this is just printing the current subjectName to chescack how far we are in the analyses
    
    % fill the data into resultData
    resultDataAngles =   get_Data_Angles(allBlocks);
    
    %loop over blocks, type 1 response per block
    blocks = allBlocks.resultDataAngle;
    
    subjectNames{subj} % this is printing the current subjectName to check how far we are in the analyses
    
    for b=1:length(blocks.type2)
        response_string = [blocks.type1(b).response{:}];
        all_responses = [all_responses response_string];
        trial_type = [trial_type blocks.block(b).blindTrials'];
        v_responses = [blocks.visual.type1(b).response{:}];
      
    end
    
    vm_trials = find(trial_type==0);
    m_trials = find(trial_type==1);
    
    vm_responses = all_responses(vm_trials);
    m_responses = all_responses(m_trials);
    
    lr_ratio.angles.vm(subj) = sum(vm_responses=='x')/sum(vm_responses=='c');
    lr_ratio.angles.m(subj) = sum(m_responses=='x')/sum(m_responses=='c');
    lr_ratio.angles.v(subj) = sum(v_responses=='x')/sum(v_responses=='c');
    
    % Angles choice bias?
    % Real angle
    real_angle = [blocks.block(:).releaseAngle];
    % Distractor angle
    distractor_angle = [blocks.block(:).releaseAngle]+[blocks.type1.adiff];
    
    lever_position = [];
    lever_position_v = [];
    for b=1:length(blocks.type2)
        lever_position = [lever_position blocks.type1(b).leverPosition.actualAngle'];
        lever_position_v = [lever_position blocks.type1(b).leverPosition.actualAngle'];
    end
    
    for t=1:length(real_angle)
        if lever_position(t) == 1
            left_angle(t) = real_angle(t);
            right_angle(t) = distractor_angle(t);
        elseif lever_position(t) == 2
            left_angle(t) = distractor_angle(t);
            right_angle(t) = real_angle(t);
        end
    end
    
    % VISUOMOTOR
    
    vm_left_angle = left_angle(vm_trials)
    vm_right_angle = right_angle(vm_trials)
    
    chose_bigger_vm = [];
    for f=1:length(vm_responses)
        if (vm_responses(f)=='x' & left_angle(f)>right_angle(f))
            chose_bigger_vm = [chose_bigger_vm 1]
        elseif vm_responses(f)=='x' && left_angle(f)<right_angle(f)
            chose_bigger_vm = [chose_bigger_vm 0]
        elseif (vm_responses(f)=='c' & left_angle(f)<right_angle(f))
            chose_bigger_vm = [chose_bigger_vm 1]
        elseif vm_responses(f)=='c' && left_angle(f)>right_angle(f)
            chose_bigger_vm = [chose_bigger_vm 0]
        end
    end
    
    chose_bigger_bias_vm(subj) = sum(chose_bigger_vm)/(length(chose_bigger_vm)-sum(chose_bigger_vm))
    
    % MOTOR
    
    m_left_angle = left_angle(m_trials)
    m_right_angle = right_angle(m_trials)
    
    chose_bigger_m = [];
    for f=1:length(m_responses)
        if (m_responses(f)=='x' & left_angle(f)>right_angle(f))
            chose_bigger_m = [chose_bigger_m 1]
        elseif vm_responses(f)=='x' && left_angle(f)<right_angle(f)
            chose_bigger_m = [chose_bigger_m 0]
        elseif (vm_responses(f)=='c' & left_angle(f)<right_angle(f))
            chose_bigger_m = [chose_bigger_m 1]
        elseif vm_responses(f)=='c' && left_angle(f)>right_angle(f)
            chose_bigger_m = [chose_bigger_m 0]
        end
    end
    
    chose_bigger_bias_m(subj) = sum(chose_bigger_m)/(length(chose_bigger_m)-sum(chose_bigger_m))
    
    % VISUAL
    
    
    v_chosen_lever = [blocks.visual.type1(:).chosenLeverPosition];
    adiff_v = [blocks.type1(:).adiff];
    chose_bigger_v = [];
    
    for f=1:length(v_responses)
        if v_responses(f)=='x'
            if lever_position_v(f)==1 && adiff_v(f)<0
                chose_bigger_v = [chose_bigger_v 1];
            elseif lever_position_v(f)==2 && adiff_v(f)>0
                chose_bigger_v = [chose_bigger_v 1];
            elseif lever_position_v(f)==1 && adiff_v(f)>0
                chose_bigger_v = [chose_bigger_v 0];               
            elseif lever_position_v(f)==2 && adiff_v(f)<0
                chose_bigger_v = [chose_bigger_v 0];
            end
        elseif v_responses(f)=='c'
            if lever_position_v(f)==2 && adiff_v(f)<0
                chose_bigger_v = [chose_bigger_v 1];
            elseif lever_position_v(f)==1 && adiff_v(f)>0
                chose_bigger_v = [chose_bigger_v 1];
            elseif lever_position_v(f)==2 && adiff_v(f)>0
                chose_bigger_v = [chose_bigger_v 0];
            elseif lever_position_v(f)==1 && adiff_v(f)<0
                chose_bigger_v = [chose_bigger_v 0];
            end
        end
    end
    
     chose_bigger_bias_v(subj) = sum(chose_bigger_v)/(length(chose_bigger_v)-sum(chose_bigger_v))
end

% TRAJECTORIES

all_responses = [];
vis_responses = [];
trial_type = [];
for subj = 1:length(subjectNames)
    
    if exist([resultDir filesep subjectNames{subj} filesep 'Trajectories' filesep 'SkittlesResult_' subjectNames{subj} '_Traj_block_5.mat'], 'file') == 2
        allBlocks = load([resultDir filesep subjectNames{subj} filesep 'Trajectories' filesep 'SkittlesResult_' subjectNames{subj} '_Traj_block_5.mat']);
    else
        allBlocks = load([resultDir filesep subjectNames{subj} filesep 'Trajectories' filesep 'SkittlesResult_' subjectNames{subj} '_Traj_block_4.mat']);
    end
    subjectNames{subj} % this is just printing the current subjectName to chescack how far we are in the analyses
    
    % fill the data into resultData
    resultDataAngles =   get_Data_Traj(allBlocks);
    
    %loop over blocks, type 1 response per block
    blocks = allBlocks.resultDataTraj;
    
    subjectNames{subj} % this is printing the current subjectName to check how far we are in the analyses
    
    for b=1:length(blocks.type2)
        response_string = [blocks.type1(b).response{:}];
        all_responses = [all_responses response_string];
        trial_type = [trial_type blocks.block(b).blindTrials'];
        v_responses = [blocks.visual.type1(b).response{:}];
    end
    
    vm_trials = find(trial_type==0);
    m_trials = find(trial_type==1);
    
    vm_responses = all_responses(vm_trials);
    m_responses = all_responses(m_trials);

    lr_ratio.traj.vm(subj) = sum(vm_responses=='x')/sum(vm_responses=='c');
    lr_ratio.traj.m(subj) = sum(m_responses=='x')/sum(m_responses=='c');
    lr_ratio.traj.v(subj) = sum(v_responses=='x')/sum(v_responses=='c');
end

lr_ratio.angles.vm_mean = mean(lr_ratio.angles.vm)
lr_ratio.angles.m_mean = mean(lr_ratio.angles.m)
lr_ratio.angles.v_mean = mean(lr_ratio.angles.v)

lr_ratio.traj.vm_mean = mean(lr_ratio.traj.vm)
lr_ratio.traj.m_mean = mean(lr_ratio.traj.m)
lr_ratio.traj.v_mean = mean(lr_ratio.traj.v)



cd(saveDir);

save('response_bias_Exp2.mat', 'lr_ratio', 'chose_bigger_bias_vm', 'chose_bigger_bias_m', 'chose_bigger_bias_v','-v7.3')