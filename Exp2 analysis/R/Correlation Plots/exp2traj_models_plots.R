# Polina Arbuzova, type2 regression lines

library(here)
library(yarrr)
################################
# Experiment 2: Trajectories   #
################################

# Exp2: load data

Traj_Bayes_VM_V <- read.csv("Traj_Bayes_VM_V.csv")
Traj_Bayes_VM_M <- read.csv("Traj_Bayes_VM_M.csv")
Traj_Bayes_V_M <- read.csv("Traj_Bayes_V_M.csv")
Traj_Bayes_VSk_VD <- read.csv("Bayes_VSk_VD.csv")

Traj_outliers_VM_V <- read.csv("Traj_outliers_VM_V.csv")
Traj_outliers_VM_M <- read.csv("Traj_outliers_VM_M.csv")
Traj_outliers_V_M <- read.csv("Traj_outliers_V_M.csv")

# Experiment 2, aka Trajectories2): run models
Traj_vm_v.model <- lmodel2(visual~visuomotor, data = Traj_Bayes_VM_V)
Traj_vm_m.model <- lmodel2(motor~visuomotor, data = Traj_Bayes_VM_M) # only one line
Traj_v_m.model <- lmodel2(motor~visual, data = Traj_Bayes_V_M)

# Experiment 2 Traj: Plots
# Visuomotor vs visual
pdf(file = "traj_vm_v.pdf",  useDingbats=FALSE,  # The directory you want to save the file in
    width = 3.5, # The width of the plot in inches
    height = 4) # The height of the plot in inches
plot(Traj_vm_v.model,       
     xlim=c(-1, 2), ylim=c(-1, 3),   
     xlab="visuomotor", ylab="visual",
     las=1, pch= 19, xaxt='n', yaxt='n', main="")
points(x = Traj_outliers_VM_V$visuomotor,  
       y = Traj_outliers_VM_V$visual,
       pch = 21)
xlabel <- seq(-1, 2, by = 1)
ylabel <- seq(-1, 2, by = 1)
axis(1, at = xlabel, las = 1)
axis(2, at = ylabel, las = 1)
dev.off()

# Visuomotor vs motor
pdf(file = "traj_vm_m.pdf",  useDingbats=FALSE,  # The directory you want to save the file in
    width = 3.5, # The width of the plot in inches
    height = 4) # The height of the plot in inches
plot(Traj_vm_m.model,       
     xlim=c(-1, 2), ylim=c(-1, 2),   
     xlab="visuomotor", ylab="motor",
     las=1, pch= 19, xaxt='n', yaxt='n', main="")
points(x = Traj_outliers_VM_M$visuomotor,
       y = Traj_outliers_VM_M$motor,
       pch = 21)
xlabel <- seq(-1, 2, by = 1)
ylabel <- seq(-1, 2, by = 1)
axis(1, at = xlabel, las = 1)
axis(2, at = ylabel, las = 1)
dev.off()

# Visual vs motor
pdf(file = "traj_v_m.pdf",  useDingbats=FALSE,  # The directory you want to save the file in
    width = 3.5, # The width of the plot in inches
    height = 4) # The height of the plot in inches
plot(Traj_v_m.model,       
     xlim=c(-1, 3), ylim=c(-1, 2),   
     xlab="visual", ylab="motor",
     las=1, pch= 19, xaxt='n', yaxt='n', main="")
points(x = Traj_outliers_V_M$visual,
       y = Traj_outliers_V_M$motor,
       pch = 21)
xlabel <- seq(-1, 2, by = 1)
ylabel <- seq(-1, 2, by = 1)
axis(1, at = xlabel, las = 1)
axis(2, at = ylabel, las = 1)
dev.off()