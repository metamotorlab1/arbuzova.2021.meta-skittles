# (c) Polina Arbuzova
library(dplyr)
library(ggpubr)
library(reshape2)
library(ez)
library(sjstats)

# Analysis of MRatio differences in Skittles Parameters (within task and across tasks), to check if there's any interaction between tasks and conditions. 

# Put the working directory on the path
setwd("/Users/polinaarbuzova/Dropbox/PhDs/Polina/R Analysis/SkittlesParameters")

# Load the results file
skittles_results = read.csv("SkParams_ALL_for_ANOVA_new.csv", na.strings ="NaN", stringsAsFactors=FALSE)

# Set condition and 
skittles_results$task <- factor(skittles_results$task)
skittles_results$condition <- factor(skittles_results$condition)
skittles_results$mratio <- as.numeric(as.character(skittles_results$mratio))
skittles_results$da <- as.numeric(as.character(skittles_results$da))
skittles_results$confidence <- as.numeric(as.character(skittles_results$confidence))

ggboxplot(skittles_results, x = "condition", y = "mratio", color = "task",
          palette = c("#00AFBB", "#E7B800"))

#res.aov2 <- aov(mratio ~ task + condition + Error(subject/condition), data = skittles_results)
#summary(res.aov2)

###############
### D-PRIME ###
###############

skittles_results_wide <- dcast(skittles_results, subject ~ condition + task, value.var = "da")
skittles_results_complete <- skittles_results_wide[complete.cases(skittles_results_wide),]
skittles_results <- melt(skittles_results_complete, id.vars = "subject", value.name = "da")
vars <- colsplit(skittles_results$variable, "_", names=c("condition", "task"))
skittles_results <- cbind(skittles_results, vars)

skittles_results$da <- as.numeric(as.character(skittles_results$da))
skittles_results$task <- factor(skittles_results$task)
skittles_results$condition <- factor(skittles_results$condition)

ezANOVA(
  data = skittles_results
  , dv = da
  , wid = subject
  , within = .(task,condition)
)

skittles_traj = subset(skittles_results, task == "Traj")
pairwise.t.test(skittles_traj$da, skittles_traj$condition,
                p.adjust.method = "bonferroni")

skittles_angles = subset(skittles_results, task == "Angles")
#pairwise.t.test(skittles_angles$da, skittles_angles$condition,
#                p.adjust.method = "bonferroni")

pairwise.t.test.with.t.and.df <- function (x, g, p.adjust.method = p.adjust.methods, pool.sd = !paired, 
                                           paired = FALSE, alternative = c("two.sided", "less", "greater"), 
                                           ...) 
{
  if (paired & pool.sd) 
    stop("pooling of SD is incompatible with paired tests")
  DNAME <- paste(deparse(substitute(x)), "and", deparse(substitute(g)))
  g <- factor(g)
  p.adjust.method <- match.arg(p.adjust.method)
  alternative <- match.arg(alternative)
  if (pool.sd) {
    METHOD <- "t tests with pooled SD"
    xbar <- tapply(x, g, mean, na.rm = TRUE)
    s <- tapply(x, g, sd, na.rm = TRUE)
    n <- tapply(!is.na(x), g, sum)
    degf <- n - 1
    total.degf <- sum(degf)
    pooled.sd <- sqrt(sum(s^2 * degf)/total.degf)
    compare.levels <- function(i, j) {
      dif <- xbar[i] - xbar[j]
      se.dif <- pooled.sd * sqrt(1/n[i] + 1/n[j])
      t.val <- dif/se.dif
      if (alternative == "two.sided") 
        2 * pt(-abs(t.val), total.degf)
      else pt(t.val, total.degf, lower.tail = (alternative == 
                                                 "less"))
    }
    compare.levels.t <- function(i, j) {
      dif <- xbar[i] - xbar[j]
      se.dif <- pooled.sd * sqrt(1/n[i] + 1/n[j])
      t.val = dif/se.dif 
      t.val
    }       
  }
  else {
    METHOD <- if (paired) 
      "paired t tests"
    else "t tests with non-pooled SD"
    compare.levels <- function(i, j) {
      xi <- x[as.integer(g) == i]
      xj <- x[as.integer(g) == j]
      t.test(xi, xj, paired = paired, alternative = alternative, 
             ...)$p.value
    }
    compare.levels.t <- function(i, j) {
      xi <- x[as.integer(g) == i]
      xj <- x[as.integer(g) == j]
      t.test(xi, xj, paired = paired, alternative = alternative, 
             ...)$statistic
    }
    compare.levels.df <- function(i, j) {
      xi <- x[as.integer(g) == i]
      xj <- x[as.integer(g) == j]
      t.test(xi, xj, paired = paired, alternative = alternative, 
             ...)$parameter
    }
  }
  PVAL <- pairwise.table(compare.levels, levels(g), p.adjust.method)
  TVAL <- pairwise.table.t(compare.levels.t, levels(g), p.adjust.method)
  if (pool.sd) 
    DF <- total.degf
  else
    DF <- pairwise.table.t(compare.levels.df, levels(g), p.adjust.method)           
  ans <- list(method = METHOD, data.name = DNAME, p.value = PVAL, 
              p.adjust.method = p.adjust.method, t.value = TVAL, dfs = DF)
  class(ans) <- "pairwise.htest"
  ans
}
pairwise.table.t <- function (compare.levels.t, level.names, p.adjust.method) 
{
  ix <- setNames(seq_along(level.names), level.names)
  pp <- outer(ix[-1L], ix[-length(ix)], function(ivec, jvec) sapply(seq_along(ivec), 
                                                                    function(k) {
                                                                      i <- ivec[k]
                                                                      j <- jvec[k]
                                                                      if (i > j)
                                                                        compare.levels.t(i, j)               
                                                                      else NA
                                                                    }))
  pp[lower.tri(pp, TRUE)] <- pp[lower.tri(pp, TRUE)]
  pp
}

result_da_traj <- pairwise.t.test.with.t.and.df(skittles_traj$da, skittles_traj$condition, p.adjust.method= "bonf",paired=TRUE)
result_da_traj
# Print t-values
result_da_traj[[5]]

# Print dfs
result_da_traj[[6]]

##################
### CONFIDENCE ###
##################

skittles_results_wide <- dcast(skittles_results, subject ~ condition + task, value.var = "confidence")
skittles_results_complete <- skittles_results_wide[complete.cases(skittles_results_wide),]
skittles_results <- melt(skittles_results_complete, id.vars = "subject", value.name = "confidence")
vars <- colsplit(skittles_results$variable, "_", names=c("condition", "task"))
skittles_results <- cbind(skittles_results, vars)


ezANOVA(
  data = skittles_results
  , dv = confidence
  , wid = subject
  , within = .(task,condition)
)

skittles_traj = subset(skittles_results, task == "Traj")
#pairwise.t.test(skittles_traj$confidence, skittles_traj$condition,
#                p.adjust.method = "bonferroni")
result_conf_traj <- pairwise.t.test.with.t.and.df(skittles_traj$confidence, skittles_traj$condition, p.adjust.method= "bonf",paired=TRUE)
result_conf_traj
# Print t-values
result_conf_traj[[5]]
# Print dfs
result_conf_traj[[6]]

skittles_angles = subset(skittles_results, task == "Angles")
#pairwise.t.test(skittles_angles$confidence, skittles_angles$condition,
#                p.adjust.method = "bonferroni")
result_conf_ang <- pairwise.t.test.with.t.and.df(skittles_angles$confidence, skittles_traj$condition, p.adjust.method= "bonf",paired=TRUE)
result_conf_ang
# Print t-values
result_conf_ang[[5]]
# Print dfs
result_conf_ang[[6]]
###############
### M-RATIO ###
###############

skittles_results_wide <- dcast(skittles_results, subject ~ condition + task, value.var = "mratio")
skittles_results_complete <- skittles_results_wide[complete.cases(skittles_results_wide),]
skittles_results <- melt(skittles_results_complete, id.vars = "subject", value.name = "mratio")
vars <- colsplit(skittles_results$variable, "_", names=c("condition", "task"))
skittles_results <- cbind(skittles_results, vars)
skittles_results$mratio <- as.numeric(as.character(skittles_results$mratio))
skittles_results$task <- factor(skittles_results$task)
skittles_results$condition <- factor(skittles_results$condition)


res.aov = ezANOVA(
  data = skittles_results
  , dv = mratio
  , wid = subject
  , within = .(task,condition)
)

###############################################
### M-RATIOS IN MOTOR CONDITION ACROS TASKS ###
###############################################
skittles_traj_m = subset(skittles_traj, condition == "motor")
skittles_angles_m = subset(skittles_angles, condition == "motor")

t.test(skittles_traj_m$mratio, skittles_angles_m$mratio, paired = TRUE, alternative = "two.sided")



