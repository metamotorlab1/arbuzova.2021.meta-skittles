# Arbuzova.2021.Meta-Skittles

To accompany the paper by Arbuzova, P., Peters, C., Röd, L., Koß, C., Maurer, H., Maurer, L. K., Müller, H., Verrel, J., & Filevich, E. (2021). Measuring metacognition of direct and indirect parameters of voluntary movement. Journal of Experimental Psychology: General, 150(11), 2208–2229. https://doi.org/10.1037/xge0000892

https://psycnet.apa.org/record/2021-39818-001


Material (experimental scrips, analysis scripts) copied from the two OSF repositories:

https://osf.io/sy342/

https://osf.io/kyhu7/

The data are too large (and inconvenient) to download, so they are only on the OSF
