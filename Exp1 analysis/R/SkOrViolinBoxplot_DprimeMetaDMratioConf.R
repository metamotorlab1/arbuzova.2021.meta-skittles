# (c) Polina Arbuzova, Oct 2019
# This script makes violin + boxplots for Skittles Original + Visual Dots task (Experiment 1).
# 

# Load ggplot.
library(ggplot2)
library(ggpubr)
library(tidyr)
library(RColorBrewer)
library(plyr)
library(gridExtra) 
library(sjstats)

# Put the working directory on the path
setwd("/Users/polinaarbuzova/Dropbox/PhDs/Polina/R Analysis/SkittlesOriginal")

# Add some data
skittles = read.csv('SkOr_Mratios_Conf.csv')

# Set the right format of the data
skittles$condition <- factor(skittles$condition, levels = c("visuomotor", "motor", "visual_skittles", "visual_dots"))
skittles$da <- as.numeric(as.character(skittles$da))
skittles$meta_da <- as.numeric(as.character(skittles$meta_da))
skittles$mratio <- as.numeric(as.character(skittles$mratio))
skittles$confidence <- as.numeric(as.character(skittles$confidence))


###################
### STATS TESTS ###
###################
### D' ###

# Compute the analysis of variance
res.aov <- aov(da ~ condition, data = skittles)
# Summary of the analysis
summary(res.aov)
pairwise.t.test(skittles$da, skittles$condition,
                p.adjust.method = "bonferroni")

pairwise.t.test.with.t.and.df <- function (x, g, p.adjust.method = p.adjust.methods, pool.sd = !paired, 
                                           paired = FALSE, alternative = c("two.sided", "less", "greater"), 
                                           ...) 
{
  if (paired & pool.sd) 
    stop("pooling of SD is incompatible with paired tests")
  DNAME <- paste(deparse(substitute(x)), "and", deparse(substitute(g)))
  g <- factor(g)
  p.adjust.method <- match.arg(p.adjust.method)
  alternative <- match.arg(alternative)
  if (pool.sd) {
    METHOD <- "t tests with pooled SD"
    xbar <- tapply(x, g, mean, na.rm = TRUE)
    s <- tapply(x, g, sd, na.rm = TRUE)
    n <- tapply(!is.na(x), g, sum)
    degf <- n - 1
    total.degf <- sum(degf)
    pooled.sd <- sqrt(sum(s^2 * degf)/total.degf)
    compare.levels <- function(i, j) {
      dif <- xbar[i] - xbar[j]
      se.dif <- pooled.sd * sqrt(1/n[i] + 1/n[j])
      t.val <- dif/se.dif
      if (alternative == "two.sided") 
        2 * pt(-abs(t.val), total.degf)
      else pt(t.val, total.degf, lower.tail = (alternative == 
                                                 "less"))
    }
    compare.levels.t <- function(i, j) {
      dif <- xbar[i] - xbar[j]
      se.dif <- pooled.sd * sqrt(1/n[i] + 1/n[j])
      t.val = dif/se.dif 
      t.val
    }       
  }
  else {
    METHOD <- if (paired) 
      "paired t tests"
    else "t tests with non-pooled SD"
    compare.levels <- function(i, j) {
      xi <- x[as.integer(g) == i]
      xj <- x[as.integer(g) == j]
      t.test(xi, xj, paired = paired, alternative = alternative, 
             ...)$p.value
    }
    compare.levels.t <- function(i, j) {
      xi <- x[as.integer(g) == i]
      xj <- x[as.integer(g) == j]
      t.test(xi, xj, paired = paired, alternative = alternative, 
             ...)$statistic
    }
    compare.levels.df <- function(i, j) {
      xi <- x[as.integer(g) == i]
      xj <- x[as.integer(g) == j]
      t.test(xi, xj, paired = paired, alternative = alternative, 
             ...)$parameter
    }
  }
  PVAL <- pairwise.table(compare.levels, levels(g), p.adjust.method)
  TVAL <- pairwise.table.t(compare.levels.t, levels(g), p.adjust.method)
  if (pool.sd) 
    DF <- total.degf
  else
    DF <- pairwise.table.t(compare.levels.df, levels(g), p.adjust.method)           
  ans <- list(method = METHOD, data.name = DNAME, p.value = PVAL, 
              p.adjust.method = p.adjust.method, t.value = TVAL, dfs = DF)
  class(ans) <- "pairwise.htest"
  ans
}
pairwise.table.t <- function (compare.levels.t, level.names, p.adjust.method) 
{
  ix <- setNames(seq_along(level.names), level.names)
  pp <- outer(ix[-1L], ix[-length(ix)], function(ivec, jvec) sapply(seq_along(ivec), 
                                                                    function(k) {
                                                                      i <- ivec[k]
                                                                      j <- jvec[k]
                                                                      if (i > j)
                                                                        compare.levels.t(i, j)               
                                                                      else NA
                                                                    }))
  pp[lower.tri(pp, TRUE)] <- pp[lower.tri(pp, TRUE)]
  pp
}

result <- pairwise.t.test.with.t.and.df(skittles$da, skittles$condition,p.adjust.method= "bonf",paired=TRUE)

# Print t-values
result[[5]]

# Print dfs
result[[6]]

### CONFIDENCE ###
# Compute the analysis of variance
res.aov <- aov(confidence ~ condition, data = skittles)
# Summary of the analysis
summary(res.aov)
pairwise.t.test(skittles$confidence, skittles$condition,
                p.adjust.method = "bonferroni")

result2 = pairwise.t.test.with.t.and.df(skittles$confidence, skittles$condition,p.adjust.method= "bonf",paired=TRUE)

# Print t-values
result2[[5]]

# Print dfs
result2[[6]]

### M-RATIOS ###
# Compute the analysis of variance
res.aov <- aov(mratio ~ condition, data = skittles)
# Summary of the analysis
summary(res.aov)
pairwise.t.test(skittles$mratio, skittles$condition,
                p.adjust.method = "bonferroni")

# Set some parameters for the plots

# Define colors
color_vm = '#00AFBB'; # ocean blue
color_v = '#E7B800'; # mustard yellow
color_m = '#A234BC'; # purple
color_vd = '#FFA500'; # orange

my_colors = c(color_vm, color_v, color_m, color_vd)

my_xlabels = c("visuomotor" = "visuomotor", "motor" = "motor",
               "visual_skittles" = "visual \n(Skittles)", "visual_dots" = "visual \n(Dots)")

# Define the theme
my_theme = theme(
  plot.title = element_text(size=16, face="bold"),
  plot.margin = unit(c(1, 0, 0.3, 1.4), "lines"),
  axis.title.x = element_text(margin = margin(t = 0, b = 0), size=16, face="bold", color = "white"),
  axis.text.x = element_text(size = 14),
  axis.title.y = element_text(margin = margin(r = 10), size=16, face="bold"),
  axis.text.y = element_text(size=14),
  legend.title = element_text(size = 14),
  legend.text = element_text(size = 14),
  # Change legend key size and key width
  legend.key.size = unit(1.5, "cm"),
  legend.key.width = unit(1,"cm"),
  legend.position = "bottom", 
  legend.spacing = unit(0.4, "cm")
)

#######################
###        D'      ####
#######################
y_ax = c(0, 2) # Define common y axis scale

p1 <- ggviolin(skittles, x = "condition", y = "da",
               add = "boxplot", add.params = list(fill = "white"),
               width = 0.9, 
               fill = "condition", palette = my_colors,
               ylim = y_ax)
p1 = p1 + theme_minimal() 
p1 = p1 + labs(title="A", x = "Trajectories", y = "D'", fill = "Conditions")
p1 = p1 + my_theme
p1 = p1 + theme(legend.position="none") + scale_x_discrete(expand=c(0,1), labels=my_xlabels)
p1

#######################
###      META-D'    ###
#######################
y_ax = c(-1, 3) # Define common y axis scale
p2 <- ggviolin(skittles, x = "condition", y = "meta_da",
               add = "boxplot", add.params = list(fill = "white"),
               width = 0.9, 
               fill = "condition", palette = my_colors,
               ylim = y_ax)
p2 = p2 + theme_minimal() 
p2 = p2 + labs(title="B", x = "Trajectories", y = "Meta-d'", fill = "Conditions")
p2 = p2 + my_theme
p2 = p2 + theme(legend.position="none") + scale_x_discrete(expand=c(0,1), labels=my_xlabels)
p2

#######################
###      M-RATIO    ###
#######################

y_ax = c(-1, 2) # Define common y axis scale
p3 <- ggviolin(skittles, x = "condition", y = "mratio",
              add = "boxplot", add.params = list(fill = "white"),
              width = 0.9, 
              fill = "condition", palette = my_colors,
              ylim = y_ax)
p3 = p3 + theme_minimal() 
p3 = p3 + labs(title="", x = "Trajectories", y = "M-ratio", fill = "Conditions")
p3 = p3 + my_theme
p3 = p3 + theme(legend.position="none") + scale_x_discrete(expand=c(0,1), labels=my_xlabels)
p3

#######################
###    CONFIDENCE  ####
#######################

y_ax = c(0, 100) # Define common y axis scale
p4 <- ggviolin(skittles, x = "condition", y = "confidence",
              add = "boxplot", add.params = list(fill = "white"),
              width = 0.9, 
              fill = "condition", palette = my_colors,
              ylim = y_ax)
p4 = p4 + theme_minimal() 
p4 = p4 + labs(title="B", x = "Trajectories", y = "Confidence", fill = "Conditions")
p4 = p4 + my_theme
p4 = p4 + theme(legend.position="none") + scale_x_discrete(expand=c(0,1), labels=my_xlabels)
p4

########################
### COMBINE THEM ALL ###
########################

# Combine two plots into a single figure
#grid.arrange( p1, p2, p3, p4, nrow = 2)

# Combine two plots into a single figure
grid.arrange(p1, p4, nrow = 1)

########################
## DESCRIPTIVE STATS ###
########################
vm = subset(skittles, condition == "visuomotor")
m = subset(skittles, condition == "motor")
vSk = subset(skittles, condition == "visual_skittles")
vD = subset(skittles, condition == "visual_dots")

# D'
# MEANS
d_vm = mean(vm$da, na.rm=TRUE)
print(round(d_vm, digits = 2))
d_m = mean(m$da, na.rm=TRUE)
print(round(d_m, digits = 2))
d_vSk = mean(vSk$da, na.rm=TRUE)
print(round(d_vSk, digits = 2))
d_vD = mean(vD$da, na.rm=TRUE)
print(round(d_vD, digits = 2))

#SDs
sd_d_vm = sd(vm$da, na.rm=TRUE)
print(round(sd_d_vm, digits = 2))
sd_d_m = sd(m$da, na.rm=TRUE)
print(round(sd_d_m, digits = 2))
sd_d_vSk = sd(vSk$da, na.rm=TRUE)
print(round(sd_d_vSk, digits = 2))
sd_d_vD = sd(vD$da, na.rm=TRUE)
print(round(sd_d_vD, digits = 2))

# Meta-d'
# MEANS
md_vm = mean(vm$meta_da, na.rm=TRUE)
print(round(md_vm, digits = 2))
md_m = mean(m$meta_da, na.rm=TRUE)
print(round(md_m, digits = 2))
md_vSk = mean(vSk$meta_da, na.rm=TRUE)
print(round(md_vSk, digits = 2))
md_vD = mean(vD$meta_da, na.rm=TRUE)
print(round(md_vD, digits = 2))

#SDs
sd_md_vm = sd(vm$meta_da, na.rm=TRUE)
print(round(sd_md_vm, digits = 2))
sd_md_m = sd(m$meta_da, na.rm=TRUE)
print(round(sd_md_m, digits = 2))
sd_md_vSk = sd(vSk$meta_da, na.rm=TRUE)
print(round(sd_md_vSk, digits = 2))
sd_md_vD = sd(vD$meta_da, na.rm=TRUE)
print(round(sd_md_vD, digits = 2))

# M-ratio
# MEANS
mr_vm = mean(vm$mratio, na.rm=TRUE)
print(round(mr_vm, digits = 2))
mr_m = mean(m$mratio, na.rm=TRUE)
print(round(mr_m, digits = 2))
mr_vSk = mean(vSk$mratio, na.rm=TRUE)
print(round(mr_vSk, digits = 2))
mr_vD = mean(vD$mratio, na.rm=TRUE)
print(round(mr_vD, digits = 2))

#SDs
sd_mr_vm = sd(vm$mratio, na.rm=TRUE)
print(round(sd_mr_vm, digits = 2))
sd_mr_m = sd(m$mratio, na.rm=TRUE)
print(round(sd_mr_m, digits = 2))
sd_mr_vSk = sd(vSk$mratio, na.rm=TRUE)
print(round(sd_mr_vSk, digits = 2))
sd_mr_vD = sd(vD$mratio, na.rm=TRUE)
print(round(sd_mr_vD, digits = 2))

# Confidence
# MEANS
conf_vm = mean(vm$confidence, na.rm=TRUE)
print(round(conf_vm, digits = 2))
conf_m = mean(m$confidence, na.rm=TRUE)
print(round(conf_m, digits = 2))
conf_vSk = mean(vSk$confidence, na.rm=TRUE)
print(round(conf_vSk, digits = 2))
conf_vD = mean(vD$confidence, na.rm=TRUE)
print(round(conf_vD, digits = 2))

#SDs
sd_conf_vm = sd(vm$confidence, na.rm=TRUE)
print(round(sd_conf_vm, digits = 2))
sd_conf_m = sd(m$confidence, na.rm=TRUE)
print(round(sd_conf_m, digits = 2))
sd_conf_vSk = sd(vSk$confidence, na.rm=TRUE)
print(round(sd_conf_vSk, digits = 2))
sd_conf_vD = sd(vD$confidence, na.rm=TRUE)
print(round(sd_conf_vD, digits = 2))