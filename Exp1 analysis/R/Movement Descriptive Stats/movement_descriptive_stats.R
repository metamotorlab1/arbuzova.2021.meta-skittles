# Polina Arbuzova, September 2020.
# This plot calculates the descriptive stats for the movements data
library(plyr)
library(tidyverse)
library(formattable)
library(here)
library(apa)
library(ggplot2)

# load libraries

# Add some data
df = read.csv('skittles_table_exp1_extra_mov.csv')
df$task = revalue(df$task, c("trajectories"="trajectories1"))


df2 = read.csv('skittles_table_exp2_extra_mov.csv')

df2$task = revalue(df2$task, c("trajectories"="trajectories2"))
df2 = df2[c(1:18)] # Remove empty columns that are created by writetable in Matlab (for no visible reason)

df3 <- rbind(df, df2)

df3 <- df3 %>%
  filter(difficulty != 0.5) # remove post hits

detach("package:plyr", unload=TRUE) # otherwise there's a conflict between two summarise functions


# Remove visual condition
df = df %>% 
  filter(condition==c('visuomotor', 'motor'))

summary1 <- df %>% 
  group_by(subject, condition) %>%
  summarise(
    mean_angle = mean(angle), sd_angle = sd(angle),
    mean_speed = mean(speed), sd_speed = sd(speed),
    mean_mindist = mean(difficulty), sd_minDist = sd(difficulty), # difficulty because I mixed up the columns when saving the dataframe...
    mean_amplitude = mean(amplitude), sd_amplitude = sd(amplitude),
    mean_peak_vel = mean(peak_velocity), sd_peak_vel = sd(peak_velocity),
    mean_duration = mean(duration), sd_duration = sd(duration),
    N = n())

summary2 = summary1 %>% 
  group_by(condition) %>%
  summarise(
    group_mean_angle = mean(mean_angle), group_sd_angle = sd(mean_angle),
    group_mean_speed = mean(mean_speed), group_sd_speed = sd(mean_speed),
    group_mean_mindist = mean(mean_mindist), group_sd_minDist = sd(mean_mindist),
    group_mean_amplitude = mean(mean_amplitude), group_sd_amplitude = sd(mean_amplitude),
    group_mean_peak_vel = mean(mean_peak_vel), group_sd_peak_vel = sd(mean_peak_vel),
    group_mean_duration = mean(mean_duration), group_sd_duration = sd(mean_duration)) %>% 
  mutate(across(is.numeric, round, 2)) # round to 2 decimals

# Create a nice table
formattable(summary2)
transposed_summary2 = t(summary2)

#### Try all with a combined df

# Remove visual condition
df3$condition <- factor(df3$condition, levels = c("visuomotor", "motor", "visual"))
df3$task <- factor(df3$task, levels = c("trajectories1", "trajectories2", "angles"))

df3 = df3 %>% 
  filter(condition==c('visuomotor', 'motor'))

summary3 = df3 %>% 
  group_by(subject, condition, task) %>%
  summarise(
    mean_angle = mean(angle), sd_angle = sd(angle),
    mean_speed = mean(speed), sd_speed = sd(speed),
    mean_mindist = mean(difficulty), sd_minDist = sd(difficulty), # mindist and difficulty are mixed up in the data frame
    mean_amplitude = mean(amplitude), sd_amplitude = sd(amplitude),
    mean_peak_vel = mean(peak_velocity), sd_peak_vel = sd(peak_velocity),
    mean_duration = mean(duration), sd_duration = sd(duration))

summary4 = summary3 %>% 
  group_by(condition, task) %>%
  summarise(
    group_mean_angle = mean(mean_angle), group_sd_angle = sd(mean_angle),
    group_mean_speed = mean(mean_speed), group_sd_speed = sd(mean_speed),
    group_mean_mindist = mean(mean_mindist), group_sd_minDist = sd(mean_mindist),
    group_mean_amplitude = mean(mean_amplitude), group_sd_amplitude = sd(mean_amplitude),
    group_mean_peak_vel = mean(mean_peak_vel), group_sd_peak_vel = sd(mean_peak_vel),
    group_mean_duration = mean(mean_duration), group_sd_duration = sd(mean_duration)) %>% 
  mutate(across(is.numeric, round, 2)) # round to 2 decimals

# Create a nice table
formattable(summary4)
summary4 = data.frame(summary4)
summary4$condition <- factor(summary4$condition, levels = c("visuomotor", "motor", "visual"))
summary4$task <- factor(summary4$task, levels = c("trajectories1", "trajectories2", "angles"))
transposed_summary4 = t(summary4)
transposed_summary4 = data.frame(transposed_summary4)
formattable(transposed_summary4)
