%%response bias analysis, Carina 08/2018
%hit run to get respone bias for each condition, over time(over blocks),
%sorted bias over all blocks
% if you want a pie chart for each participant uncomment 417 ff
%response bias more prominent in visual condition and in the last block
%no idea why

clear all;

global resultData;


user = 'polina'


switch user
    case 'carina_home'
        resultDir = 'C:\Users\Ihr Name\Dropbox\Data\Skittles_Visual\';
        userpath = 'C:\Users\Ihr Name\Dropbox\HiWis\Carina\skittles_analysis\';
    case 'MAC'
        resultDir = '/Users/metamotorlab/Dropbox/Data/Skittles_Visual/';
        userpath = '/Users/metamotorlab/HiWis/Carina/skittles_analysis/';
    case 'LINUX'
        resultDir = '/home/carina/Dropbox/Data/Skittles_Visual/';
        userpath = '/home/carina/Dropbox/HiWis/Carina/skittles_analysis/';
    case 'elisa'
        resultDir = '~/Dropbox/2_juniorGroup_BCCN/_metamotorlab/Data/Skittles_Visual/';
        userpath = '~/Dropbox/2_juniorGroup_BCCN/_metamotorlab/HiWis/Carina/skittles_analysis/';
    case 'polina'
        resultDir = '/Users/polinaarbuzova/Documents/DataFromServer/Skittles_Visual/';
        userpath = '/Users/polinaarbuzova/HiWis/Carina/skittles_analysis/';
end


cd(resultDir);

addpath(strcat(userpath, 'functions'));


%subjectsnames has to be a cell like this: 
%subjectNames = {'030VS','060RK','130MK','150RB'};
%loop through subject names and create a list with all subjects
%list with all subjects, 07.2018


%this is a new idea: the selection of the trajectories starts at
%the first lever position. Then the selection depends on how far
%the lever is moved left or right and changes every e.g. 10?? (specified in SkitSet.response1Selevtion)

%leftTrajectorySelected = sum(angleNow>startAngle+SkitSet.response1Selection*[-7:2:7] & angleNow<startAngle+SkitSet.response1Selection*[-6:2:8])>=1; %selection of the left trajectory
%
%1 = left, 0 = right


%subjectNames = {};

subjectNames =  {'020SM','030GY', '030VS', '050FJ', '050IN', '060UA', '070EE', ...
    '080MH', '090MK', '110SRb', '130MK', '140HN', '150RB', ...
    '170SA', '180CH', '181GJ', '180IG', '210KC', '211PM', ...
    '240AM', '240ME',  '260AK', '260EA', '270FS', '290SD',...
    '290US', '300BA', '220PH', '190SZ', '071SM', '130BH',...
    '300MR', '221II', '060HD', '310AU', '050GN', '221NV',...
    '280EB', '081MC', '130BY'};

if isempty(subjectNames)
 subjectNames= dir;
 subjectNames={subjectNames.name};
 for i=1:length(subjectNames)
     %delete folders that have not 5 letter names
     if length(subjectNames{length(subjectNames)-i+1})~=5; subjectNames{length(subjectNames)-i+1}=[]; end
 end
 subjectNames=subjectNames(~cellfun('isempty',subjectNames));
end

nTrialsVisuoMotor = 72;
nTrialsVisual = 36;
xtrials = 1:1:nTrialsVisuoMotor;
xtrialscond = 1:1:36;

%loop over all subjects or specific subjects

for subj = 1:length(subjectNames)
   
    %load  result Data per subject(separate blocks)
    
    allBlocks = load([resultDir subjectNames{subj} filesep 'SkittlesResult_' subjectNames{subj} '.mat']);
    
    resultData = get_Data(allBlocks); %get Data for all blocks(1-3);
    
    %clean correct responses per subject over all blocks
    %correctcleanallblocks = resultData.type1.correctClean; probably right,
    %ask Elisa, how about allBlocks is correct already "clean"?
    % hitsclean = correctcleanallblocks(correctcleanallblocks ==1);
    %sumhitsclean = sum(hitsclean);
    %FAclean = correctcleanallblocks(correctcleanallblocks ==0);
    %sumFAclean = sum(FAclean);
    
    %hits (correct type1) over all blocks(blind and non blind trials)
    correctallblocks = resultData.type1.correct;
    
    %hits over all blocks, visual trials
    correctallblocksvis = resultData.visual.type1.correct;
    
    %loop over blocks, type 1 response per block
    blocks = allBlocks.resultData.type1;
    
    subjectNames{subj} % this is printing the current subjectName to check how far we are in the analyses
    
    %empty variables to get data for all blocks
    loopoverallblocks = [];
    loopoverallblocksvis = [];
    loopoverallblocksblind = [];
    loopoverallblocksnonblind = [];
    loopoverallblockscorrectb = [];
    loopoverallblockscorrectnb = [];

    %loop over all blocks
    
    for b = 1:length(blocks)
        
        %response and correct per block
        
        response = blocks(b).response; %response blind and non blind trials, left or right trajectory
        
        responsevis = allBlocks.resultData.visual.type1(b).response; % left or right trajectory visual cond.
        
        correct = blocks(b).correct; %correct per block
        
        correctvis = allBlocks.resultData.visual.type1(b).correct; %hit or FA type 1 %correct vis per block
        
        %blind trials
        
        blindT = allBlocks.resultData.block(b).blindTrials; %blindtrials vector ==1, blind trial
        
        % get response for blind trials
        
        responseblind = response(blindT==1);
        responsenonblind = response(blindT==0);
        
        %get correct for blind vs. non blind trials, using blindT vector
        
        correctblind = correct(blindT==1);
        correctnonblind = correct(blindT==0);
        
        %response over all blocks
        
        loopoverallblocks = [loopoverallblocks response];
        
        loopoverallblocksvis = [loopoverallblocksvis responsevis];
        
        %hits vs. FA, left vs. right trajectory selected, per block, per
        %subject
        
        lefthit(b,subj) = sum(correct & response);
        righthit(b,subj) = sum(correct & ~response);
        leftFA(b,subj) = sum(~correct & response);
        rightFA(b,subj) = sum(~correct & ~response);
        
        %get response for blind and non blind trials over all blocks
        
        loopoverallblocksblind = [loopoverallblocksblind responseblind];
        
        loopoverallblocksnonblind = [loopoverallblocksnonblind responsenonblind];
        
        %get correct for blind vs. non blind over all blocks
        
        loopoverallblockscorrectb = [loopoverallblockscorrectb correctblind];
        loopoverallblockscorrectnb = [loopoverallblockscorrectnb correctnonblind];
        
        %nonblind and blind trials
        
        left(b) = sum(response==1); %sum left trajectory per block, 3 values
        right(b) = sum(response~=1); %sum right trajectory per block, 3 values
        bias(b,subj) = left/right; %response bias per block, 3 values
        
        biaspersubject(subj) = left/right;
        sortedbias = sort(biaspersubject, 'ascend');
        
        %blind trials
        
        leftblind(b) = sum(responseblind==1);
        rightblind(b) = sum(responseblind==0);
        
        biasblind(b,subj) = leftblind/rightblind;
        
        biasblindpersubject(subj) = leftblind/rightblind;
        sortedbiasblind = sort(biasblindpersubject, 'ascend');
        
        %nonblind trials
        
        leftnonblind = sum(responsenonblind==1);
        rightnonblind = sum(responsenonblind ==0);
        
        biasnonblind(b,subj) = leftnonblind/rightnonblind;
        
        biasnonblindpersubject(subj) = leftnonblind/rightnonblind;
        sortedbiasnonblind = sort(biasnonblindpersubject, 'ascend');
       
        %visual condition
        
        leftvis(b) = sum(responsevis==1); %sum left trajectory per block, 3 values
        rightvis(b) = sum(responsevis~=1); %sum right trajectory per block, 3 values
        biasvis(b,subj) = leftvis/rightvis; %response bias per block, 3 values
        
        biasvispersubject(subj) = leftvis/rightvis;
        
        sortedbiasvis = sort(biasvispersubject, 'ascend');
        
        %response per block, one value
        
        leftperblock = sum(response==1);
        rightperblock = sum(response==0);
        
        %vector trajectory bias per block
        
        leftvsright = [leftperblock, rightperblock];
        
        %correct vs. incorrect type1 (hits vs. false alarms)
        
        %hits and FA blind trials per block
        
        correctblind(b) = sum(correctblind==1); % sum correct response per block
        incorrectblind(b) = sum(correctblind==0); % sum correct response per block
        
        %Hits and FA nonblind trials per block
        
        correctnon(b) = sum(correctnonblind ==1);
        incorrectnon(b)= sum(correctnonblind ==0);
   
    end
    
    %trajectory and hits/FA over all blocks per condition
      responsepersubject(subj,:) = loopoverallblocks;
      responsepersubjectvisual(subj,:) = loopoverallblocksvis;
      responsepersubjectblind(subj,:) = loopoverallblocksblind;
      responsepersubjectnonblind(subj,:) = loopoverallblocksnonblind;
      correctb = loopoverallblockscorrectb;
      correctnb = loopoverallblockscorrectnb;
      
      %trajectory sum per condition over all blocks
      
      %non blind and blind trials trajectory over all blocks(sum)
      
      sumleft = sum(loopoverallblocks==1);
      sumright = sum(loopoverallblocks==0);
      
      
      %clean blocks, vector per subject over all blocks left vs. right
      %
      %           sumhitsclean(subj,:) = sumhitsclean;
%       %           sumFAclean(subj,:) = sumFAclean;
%       sumleft(subj,:) = sumleft;
%       sumright(subj,:) = sumright;


    %blind trials only

    sumrightblind = sum(loopoverallblocksblind==0);
    sumleftblind = sum(loopoverallblocksblind==1);

    %non blind trials

    sumrightnonblind = sum(loopoverallblocksnonblind==0);
    sumleftnonblind = sum(loopoverallblocksnonblind==1);

    %visual condition

    sumleftvis = sum(loopoverallblocksvis==1);
    sumrightvis = sum(loopoverallblocksvis==0);
    
    %hits/FA sum over all blocks per condition

    %blind and non blind trials
    
    sumhitspersubject = sum(correctallblocks==1);
    sumFApersubject = sum(correctallblocks==0);
    
    %blind trials
    
    sumhitspersubjectblind = sum(correctb==1);
    sumFApersubjectblind = sum(correctb==0);
    
    %non blind trials
    
    sumhitspersubjectnonblind = sum(correctnb==1);
    sumFApersubjectnonblind = sum(correctnb==0);
    
    %visual condition
    
    sumhitspersubjectvis = sum(correctallblocksvis==1);
    sumFApersubjectvis = sum(correctallblocksvis==0);
    
    %hits vs. trajectory per condition

    %blind and non blind trials
    
    lefthitallb = sum(loopoverallblocks & correctallblocks);
    righthitallb = sum(~loopoverallblocks & correctallblocks);
    leftFAallb = sum(loopoverallblocks & ~correctallblocks);
    rightFAallb = sum(~loopoverallblocks & ~correctallblocks);
    
    %blind trials
    
    lefthitallbblind = sum(loopoverallblocksblind & correctb);
    righthitallbblind = sum(~loopoverallblocksblind & correctb);
    leftFAallbblind = sum(loopoverallblocksblind & ~correctb);
    rightFAallbblind = sum(~loopoverallblocksblind & ~correctb);
    
    %non blind trials
    
    lefthitallbnonblind = sum(loopoverallblocksnonblind & correctnb);
    righthitallbnonblind = sum(~loopoverallblocksnonblind & correctnb);
    leftFAallbnonblind = sum(loopoverallblocksnonblind & ~correctnb);
    rightFAallbnonblind = sum(~loopoverallblocksnonblind & ~correctnb);
    
    %visual condition
    
    lefthitallbvis = sum(loopoverallblocksvis & correctallblocksvis);
    righthitallbvis = sum(~loopoverallblocksvis & correctallblocksvis);
    leftFAallbvis = sum(loopoverallblocksvis & ~correctallblocksvis);
    rightFAallbvis = sum(~loopoverallblocksvis & ~correctallblocksvis);

    
    %vector per condition hits, FA
    %blind and non blind
    
    hitsallb = [lefthitallbvis, righthitallbvis];
    
    %blind trials
    
    hitsallbblind = [lefthitallbblind, righthitallbblind];
    
    %non blind trials
    
    hitsallbnonblind = [lefthitallbnonblind, righthitallbnonblind];
    
    %visual condition
    
    hitsallbvis = [lefthitallbvis, righthitallbvis];
    
    %blind and non blind
    
    FAallb = [leftFAallb, rightFAallb];
    
    %blind trials
    
    FAallbblind = [leftFAallbblind, rightFAallbblind];
    
    %non blind
    
    FAallbnonblind = [leftFAallbnonblind, rightFAallbnonblind];
    
    % visual condition
    
    FAallbvis = [leftFAallbvis, rightFAallbvis];
    
    %sum hits/FA vector
    
    %blind and non blind
    
    sumhits = lefthitallb + righthitallb;
    sumFA = leftFAallb + rightFAallb;
    
    %blind trials
    
    sumhitsblind = lefthitallbblind + righthitallbblind;
    sumFAblind = leftFAallbblind + rightFAallbblind;
    
    %non blind trials
    
    sumhitsnonblind = lefthitallbnonblind + righthitallbnonblind;
    sumFAnonblind = leftFAallbnonblind + rightFAallbnonblind;
    
    %visual condition
    
    sumhitsvis = lefthitallbvis + righthitallbvis;
    sumFAvis = leftFAallbvis + rightFAallbvis;
    
    %percent hits vs. FA
    
    
    %blind and non blind
    
    percleftinhits = lefthitallb/sumhits;
    percleftinFA = leftFAallb/sumFA;
    
    %blind trials
    
    percleftinhitsblind = lefthitallbblind/sumhitsblind;
    percleftinFAblind = leftFAallbblind/sumFAblind;
    
    %non blind trials
    
    percleftinhitsnonblind = lefthitallbnonblind/sumhitsnonblind;
    percleftinFAnonblind = leftFAallbnonblind/sumFAnonblind;
    
    %visual condition
    
    percleftinhitsvis = lefthitallbvis/sumhitsvis;
    percleftinFAvis = leftFAallbvis/sumFAvis;

    %perecent per subject vector
    
    %blind and non blind
    
    percleftallsubFA(subj) = percleftinFA;
    percleftallsub(subj) = percleftinhits;
    
    %blind trials
    
    percleftallsubFAblind(subj) = percleftinFAblind;
    percleftallsubblind(subj) = percleftinhitsblind;
    
    %nonblind trials
    
    
    percleftallsubFAnonblind(subj) = percleftinFAnonblind;
    percleftallsubnonblind(subj) = percleftinhitsnonblind;
    
    %visual condition
    
    percleftallsubFAvis(subj) = percleftinFAvis;
    percleftallsubvis(subj) = percleftinhitsvis;
    
    %pie chart per subject, percentage left vs. right in hits vs FA
    
%     piehits = figure('Name', 'Hits');
%     labels = {'left trajectory', 'right trajectory'};
%     pie(hitsallb, labels);
%     pieFA = figure('Name', 'FA');
%     labels = {'left trajectory', 'right trajectory'};
%     pie(FAallb, labels);

    %confidence binings per condition

    Nratings = 6; %define how many confidence levels we want
    
    
    % Binning for NONBLIND (aka VISUOMOTOR)
    confvector_nonblind = resultData.type2.confClean(~resultData.type1.blindTrialsClean);
    resultData.type2.confClean_binned_nonblind = quantileranks(confvector_nonblind, Nratings);
    binconfnon = resultData.type2.confClean_binned_nonblind;
    
    %use that as vector for bias
    
    % Binning for BLIND (aka MOTOR)
    confvector_blind = resultData.type2.confClean(logical(resultData.type1.blindTrialsClean));
    resultData.type2.confClean_binned_blind = quantileranks(confvector_blind, Nratings);
    binblind = resultData.type2.confClean_binned_blind;
    
    % Binning for VISUAL:
    confvector_visual = resultData.visual.type2.confClean;
    resultData.visual.type2.confClean_binned = quantileranks(confvector_visual, Nratings);
    binvis = resultData.visual.type2.confClean_binned;

end


%% plot hits and false alarms vs. left/right (left bias) for all participant over all blocks (blind and non blind trials)
%%plots start here

%plot blind and non blind trials, left bias hits vs. FA

percleft = [percleftallsub; percleftallsubFA;]';

subjects = 1:1:40;

%shows in which subjects percentage of left trajectory in hits is bigger than FA

% bar(subjects, percleftallsub)
% hold on;
% bar(subjects, percleftallsubFA)

%shows bars hits vs. FA next to each other instead of stacked

%subplot, both plots in one figure

biasoversubjects = figure('Name', 'left bias over subjects blind and non blind');
ax1 = subplot(2,1,1);
bar(ax1,percleft);  %FA is red bar, blue is hit bar
title('left bias in % in hits(blue) vs. FA(red)');
xlabel('subjects');
ylabel('% of left trajectory');
ax2 = subplot(2,1,2); 
bar(ax2,percleft,'stacked')
xlabel('subjects');
ylabel('% of left trajectory');

%blind trials only

percleftblind = [percleftallsubblind; percleftallsubFAblind]';

biasoversubjectsblind = figure('Name', 'left bias over subjects blind trials');
ax2 = subplot(2,1,1);
bar(ax2, percleftblind); %FA is red bar, blue is hit bar
title('left bias in % in hits(blue) vs. FA(red)');
xlabel('subjects');
ylabel('% of left trajectory');
ax2 = subplot(2,1,2); 
bar(ax2,percleftblind,'stacked')
xlabel('subjects');
ylabel('% of left trajectory');

%non blind trials

percleftnonblind = [percleftallsubnonblind; percleftallsubFAnonblind]';

biasoversubjectsnonblind = figure('Name', 'left bias over subjects non blind');
ax3 = subplot(2,1,1);
bar(ax3, percleftnonblind); %FA is red bar, blue is hit bar
title('left bias in hits(blue) vs. FA(red)');
xlabel('subjects');
ylabel('% of left trajectory');
ax3 = subplot(2,1,2); 
bar(ax3,percleftnonblind,'stacked')
xlabel('subjects');
ylabel('% of left trajectory');


%visual trials only

percleftvis = [percleftallsubvis; percleftallsubFAvis]';

biasoversubjectsvisual = figure('Name', 'left bias over subjects visual condition');
ax4 = subplot(2,1,1);
bar(ax4, percleftvis); %FA is red bar, blue is hit bar
title('left bias in hits(blue) vs. FA(red)');
xlabel('subjects');
ylabel('% of left trajectory');
ax4 = subplot(2,1,2); 
bar(ax4,percleftvis,'stacked')
xlabel('subjects');
ylabel('% of left trajectory');

% bias is greater in FA?, bias greater in visual condition and non blind

%hits and FA per block per subject??? ask if we need that?

%%plot hits/FA vs. left/right trajectory, doesn't work so far
%%
%answering bias development over time(blocks)
%Look at the answering bias development over time course of the experiment (by blocks or halves of the session)
%Split data per block and check if the answering bias emerges/stays the same/disappears
%Do that for each condition separately (visuomotor/motor/visual)(16/08/18)

%define bias
%left vs. right ~= 0.5
% left / right > 1, left bias
% left / right < 1, right bias
% left / right = 1, no bias


% allBlocks.resultData.block(b).blindtrials, 1 = blind trials, 0 = non
% blind trials

%response bias blind and non blind trials

fig1 = figure('name', 'response bias blind and non blind trials');
block = [1,2,3];
hold on;
bar(block, bias);
title('response bias development over blocks');
xlabel('block');
ylabel('bias = left/right');

%sorted bias

fig1 = figure('name', 'sorted response bias blind and non blind trials');
block = [1,2,3];
hold on;
bar(subjects, sortedbias);
title('response bias development over blocks');
xlabel('block');
ylabel('bias = left/right');

%blind trials

fig2 = figure('name', 'response bias motor condition (blind trials)');
block = [1,2,3];
hold on;
bar(block, biasblind);
title('response bias development over blocks');
xlabel('block');
ylabel('bias = left/right');

%sorted bias

fig2 = figure('name', 'response bias motor condition (blind trials)');
block = [1,2,3];
hold on;
bar(subjects, sortedbiasblind);
title('response bias development over blocks');
xlabel('block');
ylabel('bias = left/right');



%nonblind trials

fig3 = figure('name', 'response bias motor-visual condition (nonblind trials)');
block = [1,2,3];
hold on;
bar(block, biasnonblind);
title('response bias development over blocks');
xlabel('block');
ylabel('bias = left/right');

%sorted


fig3 = figure('name', 'response bias motor-visual condition (nonblind trials)');
block = [1,2,3];
hold on;
bar(subjects, sortedbiasnonblind);
title('response bias development over blocks');
xlabel('block');
ylabel('bias = left/right');

%visual trials

fig4 = figure('name', 'response bias visual');
hold on;
bar(block, biasvis);
title('response bias visual condition over blocks');
xlabel('block');
ylabel('bias = left/right');

%sorted

fig4 = figure('name', 'response bias visual');
hold on;
bar(subjects, sortedbiasvis);
title('response bias visual condition over blocks');
xlabel('block');
ylabel('bias = left/right');