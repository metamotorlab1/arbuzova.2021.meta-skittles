function [distanceToTarget, angleRange, speedRange, targetHit] = getLandscape2(SkitSet)

% Get the corresponding settings 
%addpath('~/git/SkittlesMatlabLJ/SkittlesMatlab/getCollisionLandscape/functions/')

%SkitSet = set_SkitSet_toPlot;

%value to show in the plot as succesful ball-target a collission flag.
%It will be discontinuous. Set to NaN if you don't want to flag this in any
%specific way
collisionFlagValue = NaN; %-.1; 

angleRange = 40:.01:130; %from 0 to 180 it makes more sense but for now we just want to replicate their plots
speedRange = 0.9:.001:1.8; %This is in m/sec, as in Mueller & Sternad 2004 (JEP: HPP)

[releaseAngle,releaseSpeed] = meshgrid(angleRange,speedRange);
%preallocate
distanceToTarget = zeros(size(releaseAngle));


s = 0;
for speed = speedRange
    a = 0;
    s = s+1;
    for angle = angleRange
        a = a+1;

        BRP = calculate_release_parameters_toPlot( angle, speed, SkitSet, collisionFlagValue);

        distanceToTarget(s,a) = BRP.minDist;
        targetHit(s,a) = ~isnan(BRP.x_collision);
        
    end
end


% From Mueller & Sternad 2004 (JEP: HPP)
% The two branches pertain to the two major angle ranges in which the ball 
% can be hit. The two branches are not symmetrical because at 90? the 
% distance to the center post is smaller than it is at 90?.
