function [ allEqual, equal ] = posthitChecker( resultData )
%POSTHITCHECKER Summary of this function goes here
%   Detailed explanation goes here
blocks=length(resultData.type1);
trials=length(resultData.type1(1).correct);
equal=zeros(1,blocks*trials);
for block=1:blocks
    for trial=1:trials
        if resultData.block(block).BRP(trial).posthit==resultData.block(block).BRP_alt(trial).posthit
            equal(trial+(block-1)*trials)=1;
        end
    end
end
if sum(equal)==length(equal); allEqual=1;else;allEqual=0;end

