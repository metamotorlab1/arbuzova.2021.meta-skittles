function resultData=get_Data(allBlocks)


resultData.type1.errorTrials  = [allBlocks.resultData.type1.errorTrials];
resultData.type1.response     = [allBlocks.resultData.type1.response];
resultData.type1.RT           = [allBlocks.resultData.type1.RT];
resultData.type1.correct      = [allBlocks.resultData.type1.correct];
resultData.type1.blindTrials  = [allBlocks.resultData.block.blindTrials];
resultData.type2.conf         = [allBlocks.resultData.type2.conf];
resultData.type1.whichStair   = [allBlocks.resultData.type1.whichStair];
resultData.type1.vdiff        = [allBlocks.resultData.type1.vdiff];
BRPs                          = [allBlocks.resultData.block.BRP];
BRPs_alt                      = [allBlocks.resultData.block.BRP_alt];


if isfield(allBlocks.resultData.type1,'unequalPosthit')
    resultData.type1.unequalPosthit = [allBlocks.resultData.type1.unequalPosthit];
else
    %this is a work-around for older resultData structs without the unequal
    %posthit flag
    resultData.type1.unequalPosthit =([BRPs.posthit]&~[BRPs_alt.posthit])|(~[BRPs.posthit]&[BRPs_alt.posthit]);
    warning('no unequal posthit array')
end
resultData.posthit            = [BRPs.posthit];
resultData.posthitAfterCross  = [BRPs.posthitAfterCross];
resultData.velRelease_actual  = [BRPs.v];

resultData.velRelease_alt     = [BRPs_alt.v];
resultData.angleRelease       = [BRPs.angle];

if isfield(allBlocks.resultData.type1,'TrajectoriesDifferentDirections')
    resultData.type1.differentDirections = [allBlocks.resultData.type1.TrajectoriesDifferentDirections];
else
    %workaround for old data
    resultData.type1.differentDirections = ([BRPs.v]>0 & [BRPs_alt.v]<0) | ([BRPs.v]<0 & [BRPs_alt.v]>0);
    warning('no trajectories different directions array')
end

% set (only) here the condition for clean trials
resultData.cleanIndexes           = find(resultData.type1.RT <= 8 & resultData.type1.RT >= 0.3 & ~resultData.type1.errorTrials & ~resultData.type1.differentDirections & (resultData.type1.blindTrials | ~resultData.type1.unequalPosthit) );
resultData.type1.responseClean    = resultData.type1.response(resultData.cleanIndexes);
resultData.type1.RTclean          = resultData.type1.RT(resultData.cleanIndexes);
resultData.type1.correctClean     = resultData.type1.correct(resultData.cleanIndexes);
resultData.type2.confClean        = resultData.type2.conf(resultData.cleanIndexes);
resultData.type1.blindTrialsClean = resultData.type1.blindTrials(resultData.cleanIndexes);
resultData.posthitClean           = resultData.posthit(resultData.cleanIndexes);
resultData.posthitAfterCrossClean = resultData.posthitAfterCross(resultData.cleanIndexes);
resultData.vdiffClean             = resultData.type1.vdiff(resultData.cleanIndexes);

%% visual condition

resultData.visual.type1.errorTrials     = [allBlocks.resultData.visual.type1.errorTrials];
resultData.visual.type1.response        = [allBlocks.resultData.visual.type1.response];
resultData.visual.type1.correct         = [allBlocks.resultData.visual.type1.correct];
resultData.visual.type2.conf            = [allBlocks.resultData.visual.type2.conf];
resultData.visual.type1.RT              = [allBlocks.resultData.visual.type1.RT];
resultData.visual.type1.vdiff           = [allBlocks.resultData.visual.type1.vdiff];
BRPs                                    = [allBlocks.resultData.visual.movementData.BRP];
resultData.visual.posthit               = [BRPs.posthit];
resultData.visual.posthitAfterCross     = [BRPs.posthitAfterCross];
resultData.visual.velRelease_actual     = [BRPs.v];
BRPs_alt                                = [allBlocks.resultData.visual.movementData.BRP_alt];
resultData.visual.velRelease_alt        = [BRPs_alt.v];
resultData.visual.angleRelease          = [BRPs.angle];

% work around for old data in else blocks
if isfield(allBlocks.resultData.visual.type1,'unequalPosthit')
    resultData.visual.type1.unequalPosthit = [allBlocks.resultData.visual.type1.unequalPosthit];
else
    resultData.visual.type1.unequalPosthit = ([BRPs.posthit]&~[BRPs_alt.posthit])|(~[BRPs.posthit]&[BRPs_alt.posthit]);
    warning('no unequal posthits in visual condition')
end

if isfield(allBlocks.resultData.visual.type1,'TrajectoriesDifferentDirections')
    resultData.visual.type1.differentDirections   = [allBlocks.resultData.visual.type1.TrajectoriesDifferentDirections];
else
    warning('no trajectories different directions in visual condition')
    resultData.visual.type1.differentDirections   =([BRPs.v]>0 & [BRPs_alt.v]<0) | ([BRPs.v]<0 & [BRPs_alt.v]>0);
end

%TODO: clean indexes
% set (only) here the condition for clean trials
resultData.visual.cleanIndexes           = find(resultData.visual.type1.RT <= 8 & resultData.visual.type1.RT >= 0.3 & ~(resultData.visual.type1.differentDirections|resultData.visual.type1.unequalPosthit));
resultData.visual.type1.responseClean    = resultData.visual.type1.response(resultData.visual.cleanIndexes);
%resultData.visual.cleanIndexes           = find(~resultData.visual.type1.errorTrials &  (~(resultData.visual.type1.differentDirections| resultData.visual.type1.unequalPosthit)));
resultData.visual.type1.RTclean          = resultData.visual.type1.RT(resultData.visual.cleanIndexes);
resultData.visual.type1.correctClean     = resultData.visual.type1.correct(resultData.visual.cleanIndexes);
resultData.visual.type2.confClean        = resultData.visual.type2.conf(resultData.visual.cleanIndexes);
resultData.visual.posthitClean           = resultData.visual.posthit(resultData.visual.cleanIndexes);
resultData.visual.posthitAfterCrossClean = resultData.visual.posthitAfterCross(resultData.visual.cleanIndexes);
resultData.visual.vdiffClean             = resultData.visual.type1.vdiff(resultData.visual.cleanIndexes);

end