function [allSubjs] = step1_describeSkittles(subjectNames, user, make_plots)


global resultData
clear resultData
switch user
    case {'office'}
%         userpath='~/Dropbox/HiWis/Lukas/';
        resultDir=['~/Dropbox/Data/Skittles_Visual/'];
       % resultDir=strcat(userpath, 'SkittlesMatlab/Results/');
        userpath=['~/Dropbox/HiWis/Lukas/'];
    case {'elisa'}
        userpath='~/git/SkittlesMatlabLJ/';
        %resultDir='~/Dropbox/2_juniorGroup_BCCN/_metamotorlab/HiWis/Lukas/SkittlesMatlab/Results/';
        resultDir='~/Dropbox/2_juniorGroup_BCCN/_metamotorlab/Data/Skittles_Visual/';
    case('lukasHome')
        userpath='C:/Users/lukas/Documents/Uni/Arbeit/GitLab/'; 
        resultDir=strcat(userpath, 'SkittlesMatlab/Results/data/');
    case {'christina'}
        userpath='C:/Users/User/Dropbox/Metamotor/HiWis/Lukas/';
        resultDir=strcat(userpath, 'SkittlesMatlab/Results/');
    case {'polina'}
        userpath=['~/Dropbox/PhDs/Polina/'];
        resultDir=['~/Dropbox/Data/Skittles_Visual/'];
end
addpath(strcat(userpath, 'SkittlesMatlab/Analysis/fx'));
addpath(strcat(userpath, 'SkittlesMatlab/BasicVariables'));
addpath(strcat(userpath, 'SkittlesMatlab/getCollisionLandscape/functions/'));

cd(resultDir)

% subjectsnames has to be a cell like this: 
% subjectNames = {'CZ101','281LB', '151NY', '160SA', '191MA', '211EH2b'};

 % if left empty a list of all folders with 5 letters length is created 
if isempty(subjectNames)
 subjectNames= dir;
 subjectNames={subjectNames.name};
 for i=1:length(subjectNames)
     %delete folders that have not 5 letter names
     if length(subjectNames{length(subjectNames)-i+1})~=5; subjectNames{length(subjectNames)-i+1}=[]; end;
 end
 subjectNames=subjectNames(~cellfun('isempty',subjectNames));

end
subplotSet.rows = 4;
subplotSet.cols = 4;

for subj = 1:length(subjectNames)
    
    allBlocks = load([resultDir subjectNames{subj} filesep 'SkittlesResult_' subjectNames{subj} '.mat']);
    
    if make_plots == 1 || make_plots == 2
        figHandle = figure('Name',subjectNames{subj},'NumberTitle','off'); hold on
    else
        figHandle = figure('visible','off');
    end
    subjectNames{subj}
    
     %Get the landscape description once (it takes time and it will be the same
     %for all subjects)
     if subj == 1
         SkitSet = allBlocks.resultData.SkitSet;
         [distanceToTarget, angleRange, speedRange, targetHit] = getLandscape(SkitSet);
     end
     
     % fill the data into resultData
     resultData=   get_Data(allBlocks);
     allSubjs.nSlowTrials(subj)        = length(resultData.type1.RT) - length(resultData.cleanIndexes);
     allSubjs.nSlowTrialsVisual(subj)  = length(resultData.visual.type1.RT) - length(resultData.visual.cleanIndexes);
     
     %% calculate and plot stuff
     
     %Overlay the velocity pairs for each trial on the landscape
     
     %awful, but the vectorized solution is awful too and error-prone
     visibleTrials           = ~resultData.type1.blindTrials;
     blindTrials             = logical(resultData.type1.blindTrials);
     angle_visibleTrials     = resultData.angleRelease(visibleTrials);
     angle_blindTrials       = resultData.angleRelease(blindTrials);
     angle_visualTrials      = resultData.visual.angleRelease;
     velActual_VisibleTrials = resultData.velRelease_actual(visibleTrials);
     velAlt_VisibleTrials    = resultData.velRelease_alt(visibleTrials);
     velActual_blindTrials   = resultData.velRelease_actual(blindTrials);
     velAlt_blindTrials      = resultData.velRelease_alt(blindTrials);
     velActual_visualTrials  = resultData.visual.velRelease_actual;
     velAlt_visualTrials     = resultData.visual.velRelease_alt;
     % For the unequal target hit analysis
     angle_visuomotor_motorTrials        = resultData.angleRelease(visibleTrials | blindTrials);
     velActual_visuomotor_motorTrials    = resultData.velRelease_actual(visibleTrials | blindTrials);
     velAlt_visuomotor_motorTrials       = resultData.velRelease_alt(visibleTrials | blindTrials);
     
     if make_plots == 2 % for landscape plots
         figure('units','normalized','outerposition',[0 0 1 .5], 'Name',subjectNames{subj}); hold on
         subplot(1,3,1);
         plotLandscape(SkitSet, angleRange, speedRange, distanceToTarget, targetHit);
     end
     [crossing_visible, ~] = filter_crossing(angleRange, speedRange, targetHit, distanceToTarget, angle_visibleTrials, velActual_VisibleTrials, velAlt_VisibleTrials);
     allSubjs.crossPercent_nonBlind(subj) = sum(crossing_visible)/length(crossing_visible)*100;
     
     if make_plots == 2
         for t = 1:length(angle_visibleTrials)
             if ~crossing_visible(t)
                 plot([angle_visibleTrials(t) angle_visibleTrials(t)], [velActual_VisibleTrials(t) velAlt_VisibleTrials(t)], 'g-');
             else
                 plot([angle_visibleTrials(t) angle_visibleTrials(t)], [velActual_VisibleTrials(t) velAlt_VisibleTrials(t)], 'r-');
             end
         end
         title('visible')
         
         subplot(1,3,2);
         plotLandscape(SkitSet, angleRange, speedRange, distanceToTarget, targetHit);
     end
     
     [crossing_blind, ~] = filter_crossing(angleRange, speedRange, targetHit, distanceToTarget, angle_blindTrials, velActual_blindTrials, velAlt_blindTrials);
     allSubjs.crossPercent_blind(subj) = sum(crossing_blind)/length(crossing_blind)*100;
     
     if make_plots == 2
         for t = 1:length(angle_visibleTrials)
             if ~crossing_blind(t)
                 plot([angle_blindTrials(t) angle_blindTrials(t)], [velActual_blindTrials(t) velAlt_blindTrials(t)], 'g-');
             else
                 plot([angle_blindTrials(t) angle_blindTrials(t)], [velActual_blindTrials(t) velAlt_blindTrials(t)], 'r-');
             end
         end
         title('blind')
         subplot(1,3,3);
         plotLandscape(SkitSet, angleRange, speedRange, distanceToTarget, targetHit);
     end
     
     [crossing_visual, ~] = filter_crossing(angleRange, speedRange, targetHit, distanceToTarget, angle_visualTrials, velActual_visualTrials, velAlt_visualTrials);
     allSubjs.crossPercent_visual(subj) = sum(crossing_visual)/length(crossing_visual)*100;
     
     if make_plots == 2
         for t = 1:length(angle_visibleTrials)
             if ~crossing_visual(t)
                 plot([angle_visualTrials(t) angle_visualTrials(t)], [velActual_visualTrials(t) velAlt_visualTrials(t)], 'g-');
             else
                 plot([angle_visualTrials(t) angle_visualTrials(t)], [velActual_visualTrials(t) velAlt_visualTrials(t)], 'r-');
             end
         end
         title('visual')
     end
     
     
     [crossing_visuomotor_motor, ~] = filter_crossing(angleRange, speedRange, targetHit, distanceToTarget, angle_visuomotor_motorTrials, velActual_visuomotor_motorTrials, velAlt_visuomotor_motorTrials);
     
     
     
     try
         if make_plots == 1 || make_plots == 2
             subplotSet.position = [1 5];
             getStaircase(figHandle, subplotSet,resultData)
             
             subplotSet.position = [2 6];
         end
         allSubjs.percentCorrect(subj,:) = getPercentCorrect(figHandle, subplotSet,resultData, make_plots);
         % To see who performed below and above the performance thresholds
         allSubjs.percentCorrect
         all_pc = allSubjs.percentCorrect < 60 | allSubjs.percentCorrect > 80; % thresholds as in pre-registration
         all_pc_ind = find(allSubjs.percentCorrect < 60 | allSubjs.percentCorrect > 80);
         [I,J] = ind2sub(size(all_pc),all_pc_ind);
         subjectNames{1,I'}; % returns subject names
         J; % and problematic condition number
         if make_plots == 1 || make_plots == 2
             subplotSet.position = [9 11];
             getReactionTimes(figHandle, subplotSet,resultData)
             
             subplotSet.position = [13 15];
             getConfidenceDistributions(figHandle, subplotSet,resultData)
             
             subplotSet.position = [3 7];
             confVsRT(figHandle, subplotSet,resultData)
             
             subplotSet.position = [12 16];
             
         end
        
        
    [allSubjs.SSEfit_nonBlind(subj), allSubjs.SSEfit_blind(subj), allSubjs.SSEfit_visual(subj), allSubjs.SSEfit_blind_uneq(subj), allSubjs.SSEfit_blind_eq(subj), resultData, nR_S1_nonBlind{subj}, nR_S2_nonBlind{subj}, nR_S1_blind{subj}, nR_S2_blind{subj}, nR_S1_visual{subj}, nR_S2_visual{subj}, nR_S1_blind_eq{subj}, nR_S2_blind_eq{subj}, nR_S1_blind_uneq{subj}, nR_S2_blind_uneq{subj}] = metadPrime(figHandle, subplotSet,resultData, crossing_visuomotor_motor, crossing_visual, make_plots);
    % Calculate even if you have bins with 0 count. Ok for now - when we run
    % the real experiment, we need to be careful
    %if isfield(SSEfit(subj), 'da')
    %    warning(['zeros in one of the nR_vectors of subj ' num2str(subj)])
    %end
     
    subplotSet.position = [4 8];
    
%     [allSubjs.AUROC2_nonBlind(subj), allSubjs.AUROC2_blind(subj), allSubjs.AUROC2_visual(subj),resultData] = Aroc(figHandle, subplotSet,resultData, make_plots);
    
    catch e
        e.message
        % warning(['error message in ' subjectNames(subj) ': ' e.message])
    end
    
    %TODO: get the time the experiment took for each subject
    
    
end

 end


%%




%% functions
function getStaircase(figHandle, subplotSet,resultData)

%global resultData

staircaseColours = 'rbg';

figure(figHandle)
subplot(subplotSet.rows,subplotSet.cols,subplotSet.position); hold on
title('staircase')

for s = 1:max(resultData.type1.whichStair)
    plot(abs( resultData.type1.vdiff(~resultData.type1.differentDirections & resultData.type1.whichStair==s)), [staircaseColours(s) '-o'])
end
plot(abs(resultData.visual.type1.vdiff(~resultData.visual.type1.differentDirections)), [staircaseColours(3) '-o'])

xlabel('trial')
ylabel('v0 difference')

end

%%
function percentCorrect = getPercentCorrect(figHandle, subplotSet,resultData, make_plots)

percentCorrect = [sum(~resultData.type1.blindTrialsClean & resultData.type1.correctClean)/...
    sum(~resultData.type1.blindTrialsClean)*100, ...
    sum(resultData.type1.blindTrialsClean & resultData.type1.correctClean)/...
    sum(resultData.type1.blindTrialsClean)*100, ...
    sum(resultData.visual.type1.correctClean)/length(resultData.visual.type1.correctClean)*100];

if make_plots == 1 || make_plots == 2
    figure(figHandle)
    subplot(subplotSet.rows,subplotSet.cols,subplotSet.position); hold on
    title('type1 task')
    b = bar([sum(~resultData.type1.blindTrialsClean & resultData.type1.correctClean) /sum(~resultData.type1.blindTrialsClean), ...
        sum(resultData.type1.blindTrialsClean  & resultData.type1.correctClean) /sum(resultData.type1.blindTrialsClean),...
        sum(resultData.visual.type1.correctClean)/length(resultData.visual.type1.correctClean);...
        sum(~resultData.type1.blindTrialsClean & ~resultData.type1.correctClean)/sum(~resultData.type1.blindTrialsClean),...
        sum(resultData.type1.blindTrialsClean  & ~resultData.type1.correctClean)/sum(resultData.type1.blindTrialsClean),...
        sum(resultData.visual.type1.correctClean==0)/length(resultData.visual.type1.correctClean)]);
    b(1).FaceColor = 'r';
    b(2).FaceColor = 'b';
    b(3).FaceColor = 'g';
    set(gca, 'XTickLabel', {'correct', 'incorrect'}, 'XTick', 1:2)
    legend('non-blind trials', 'blind trials','visual-only')
    ylabel('% trials')
else
    figHandle = figure('visible','off');
end
end

%%
function getReactionTimes(figHandle, subplotSet,resultData)

%global resultData

figure(figHandle)
subplot(subplotSet.rows,subplotSet.cols,subplotSet.position(1)); hold on
title('Non-blind trials')
nbins = 10;
histogram(resultData.type1.RTclean(resultData.type1.correctClean>0 & ~resultData.type1.blindTrialsClean), min(resultData.type1.RTclean(~resultData.type1.blindTrialsClean)):range(resultData.type1.RTclean(~resultData.type1.blindTrialsClean))/nbins:max(resultData.type1.RTclean(~resultData.type1.blindTrialsClean)), 'FaceColor', [0 .5 0]);
histogram(resultData.type1.RTclean(resultData.type1.correctClean<1 & ~resultData.type1.blindTrialsClean), min(resultData.type1.RTclean(~resultData.type1.blindTrialsClean)):range(resultData.type1.RTclean(~resultData.type1.blindTrialsClean))/nbins:max(resultData.type1.RTclean(~resultData.type1.blindTrialsClean)), 'FaceColor', [.5 0 0]);
legend('correct', 'incorrect')
xlabel('type1 RT (sec)')
ylabel('count')

subplot(subplotSet.rows,subplotSet.cols,subplotSet.position(1)+1); hold on
title('Blind trials')
nbins = 10;
histogram(resultData.type1.RTclean(resultData.type1.correctClean>0 & resultData.type1.blindTrialsClean), min(resultData.type1.RTclean(logical(resultData.type1.blindTrialsClean))):range(resultData.type1.RTclean(logical(resultData.type1.blindTrialsClean)))/nbins:max(resultData.type1.RTclean(logical(resultData.type1.blindTrialsClean))), 'FaceColor', [0 .5 0]);
histogram(resultData.type1.RTclean(resultData.type1.correctClean<1 & resultData.type1.blindTrialsClean), min(resultData.type1.RTclean(logical(resultData.type1.blindTrialsClean))):range(resultData.type1.RTclean(logical(resultData.type1.blindTrialsClean)))/nbins:max(resultData.type1.RTclean(logical(resultData.type1.blindTrialsClean))), 'FaceColor', [.5 0 0]);
xlabel('type1 RT (sec)')

subplot(subplotSet.rows,subplotSet.cols,subplotSet.position(2)); hold on
title('Visual trials')
nbins = 10;
histogram(resultData.visual.type1.RTclean(resultData.visual.type1.correctClean>0), min(resultData.visual.type1.RTclean):range(resultData.visual.type1.RTclean)/nbins:max(resultData.visual.type1.RTclean), 'FaceColor', [0 .5 0]);
histogram(resultData.visual.type1.RTclean(resultData.visual.type1.correctClean<1), min(resultData.visual.type1.RTclean):range(resultData.visual.type1.RTclean)/nbins:max(resultData.visual.type1.RTclean), 'FaceColor', [.5 0 0]);
xlabel('type1 RT (sec)')

end


function getConfidenceDistributions(figHandle, subplotSet,resultData)

%global resultData;

figure(figHandle)
subplot(subplotSet.rows,subplotSet.cols,subplotSet.position(1)); hold on
title('Non-blind trials')
histogram(resultData.type2.confClean(resultData.type1.correctClean>0 & ~resultData.type1.blindTrialsClean), 0:5:100, 'FaceColor', [0 .5 0]);
histogram(resultData.type2.confClean(resultData.type1.correctClean<1 & ~resultData.type1.blindTrialsClean), 0:5:100, 'FaceColor', [.5 0 0]);
legend('correct', 'incorrect')
xlabel('confidence')
ylabel('count')

subplot(subplotSet.rows,subplotSet.cols,subplotSet.position(1)+1); hold on
title('Blind trials')
histogram(resultData.type2.confClean(resultData.type1.correctClean>0 & resultData.type1.blindTrialsClean), 0:5:100, 'FaceColor', [0 .5 0]);
histogram(resultData.type2.confClean(resultData.type1.correctClean<1 & resultData.type1.blindTrialsClean), 0:5:100, 'FaceColor', [.5 0 0]); 
xlabel('confidence')

subplot(subplotSet.rows,subplotSet.cols,subplotSet.position(2)); hold on
title('Visual trials')
histogram(resultData.visual.type2.confClean(resultData.visual.type1.correctClean>0), 0:5:100, 'FaceColor', [0 .5 0]);
histogram(resultData.visual.type2.confClean(resultData.visual.type1.correctClean<1), 0:5:100, 'FaceColor', [.5 0 0]);
xlabel('confidence')

end


function confVsRT(figHandle, subplotSet,resultData)

%global resultData

figure(figHandle)
subplot(subplotSet.rows,subplotSet.cols,subplotSet.position); hold on
title('Confidence vs type1RT')
scatter(resultData.type1.RTclean(~resultData.type1.blindTrialsClean), ...
    resultData.type2.confClean(~resultData.type1.blindTrialsClean), 'r')
lsline
scatter(resultData.type1.RTclean(logical(resultData.type1.blindTrialsClean)), ...
    resultData.type2.confClean(logical(resultData.type1.blindTrialsClean)), 'b')
lsline
scatter(resultData.visual.type1.RTclean, ...
    resultData.visual.type2.confClean, 'g')
lsline
xlabel('RT (secs)')
ylabel('confidence')


end


function [SSEfit_nonBlind, SSEfit_blind, SSEfit_visual,  SSEfit_blind_uneq, SSEfit_blind_eq, resultData, nR_S1_nonBlind, nR_S2_nonBlind, nR_S1_blind, nR_S2_blind, nR_S1_visual, nR_S2_visual, nR_S1_blind_eq, nR_S2_blind_eq,nR_S1_blind_uneq, nR_S2_blind_uneq] = metadPrime(figHandle, subplotSet,resultData, crossing_visuomotor_motor, crossing_visual, make_plots)

%global resultData
if make_plots == 1 || make_plots == 2
    figure(figHandle)
else
    figHandle = figure('visible','off');
end

Nratings = 6; %define how many confidence levels we want

%resultData.type2.confClean_binned = discretize(resultData.type2.confClean, Nratings);
% for Matlab 2016a the line above does not work. So we use Christina's
% solution:

% Binning for NONBLIND (aka VISUOMOTOR)
confvector_nonblind = resultData.type2.confClean(~resultData.type1.blindTrialsClean);
%resultData.type2.confClean_binned_nonblind = discretize(confvector, min(confvector):range(confvector)/Nratings:max(confvector)); 
resultData.type2.confClean_binned_nonblind = quantileranks(confvector_nonblind, Nratings);

% Binning for BLIND (aka MOTOR)
confvector_blind = resultData.type2.confClean(logical(resultData.type1.blindTrialsClean));
%resultData.type2.confClean_binned_blind = discretize(confvector, min(confvector):range(confvector)/Nratings:max(confvector)); 
resultData.type2.confClean_binned_blind = quantileranks(confvector_blind, Nratings);


% Binning for VISUAL:
confvector_visual = resultData.visual.type2.confClean;
%resultData.visual.type2.confClean_binned = discretize(resultData.visual.type2.confClean, Nratings);
resultData.visual.type2.confClean_binned = quantileranks(confvector_visual, Nratings); 


% Get the clean VDIFF 
% for nonblind and blind (aka visomotor and motor)
resultData.type1.vdiffClean = resultData.type1.vdiff(resultData.cleanIndexes); 
% for visual
resultData.visual.type1.vdiffClean = resultData.visual.type1.vdiff(resultData.visual.cleanIndexes);
%%
% Get the clean unequal target hits
length_ind = max(resultData.cleanIndexes);
clean_ind_visible_motor = zeros(1, length_ind);
clean_ind_visible_motor(resultData.cleanIndexes) = 1; % let's get a logical array indicating 'clean' trials for both visuomotor and motor trials

crossing_visuomotor_motor = crossing_visuomotor_motor(logical(clean_ind_visible_motor));
crossing_visual = crossing_visual(resultData.visual.cleanIndexes);
%% 
% I take arbitrarily resultData.type1.vdiff<0 to be S1 and >0 to be
% S2, think if we want to define this differently- TODO: Maybe, given
% the restriction to not have the two trajectories as one hitting the
% post and the other one not at all


% Let's try to check if we get the same bin counts with Maniscalco's script
response_vm = [];
stimID_vm = [];
response_m = [];
stimID_m = [];
response_v = [];
stimID_v = [];

response_button_VM = resultData.type1.responseClean(find(~resultData.type1.blindTrialsClean));
response_button_M = resultData.type1.responseClean(find(resultData.type1.blindTrialsClean));
vdiff_displayed_VM = resultData.vdiffClean(find(~resultData.type1.blindTrialsClean));
vdiff_displayed_M = resultData.vdiffClean(find(resultData.type1.blindTrialsClean));

% VISUOMOTOR
for m = 1:length(response_button_VM)
    if response_button_VM(m) == 1
        response_vm(m) = 0;
    else
        response_vm(m) = 1;
    end
    if vdiff_displayed_VM(m) > 0
        stimID_vm(m) = 0;
    else 
        stimID_vm(m) = 1;
    end
end
% MOTOR
for m = 1:length(response_button_M)
    if response_button_M(m) == 1
        response_m(m) = 0;
    else
        response_m(m) = 1;
    end
    if vdiff_displayed_M(m) > 0
        stimID_m(m) = 0;
    else 
        stimID_m(m) = 1;
    end
end

% VISUAL
for m = 1:length(resultData.visual.type1.responseClean)
    if resultData.visual.type1.responseClean(m) == 1
        response_v(m) = 0;
    else
        response_v(m) = 1;
    end
    if resultData.visual.vdiffClean(m) > 0
        stimID_v(m) = 0;
    else
        stimID_v(m) = 1;
    end
end

% Use "trials2counts" function
% (http://www.columbia.edu/~bsm2105/type2sdt/trials2counts.m ) to double
% check that our loop-based solution works fine
[nR_S1_VM, nR_S2_VM] = trials2counts(stimID_vm, response_vm, resultData.type2.confClean_binned_nonblind, Nratings, 0, 0);
[nR_S1_M, nR_S2_M] = trials2counts(stimID_m, response_m, resultData.type2.confClean_binned_blind, Nratings, 0, 0);
[nR_S1_V, nR_S2_V] = trials2counts(stimID_v, response_v, resultData.visual.type2.confClean_binned, Nratings, 0, 0);


nR_S1_nonBlind = zeros(2*Nratings,1);
nR_S2_nonBlind = zeros(2*Nratings,1);
nR_S1_blind    = zeros(2*Nratings,1);
nR_S2_blind    = zeros(2*Nratings,1);
nR_S1_visual   = zeros(2*Nratings,1);
nR_S2_visual   = zeros(2*Nratings,1);

for i = 1:Nratings
    nR_S1_nonBlind(i)          = sum(resultData.type1.correctClean(~resultData.type1.blindTrialsClean)  & resultData.vdiffClean(~resultData.type1.blindTrialsClean)>0 & resultData.type2.confClean_binned_nonblind == Nratings+1-i);
    nR_S1_nonBlind(Nratings+i) = sum(~resultData.type1.correctClean(~resultData.type1.blindTrialsClean)  &  resultData.vdiffClean(~resultData.type1.blindTrialsClean)>0 & resultData.type2.confClean_binned_nonblind == i);
    nR_S2_nonBlind(Nratings+i) = sum(resultData.type1.correctClean(~resultData.type1.blindTrialsClean) & resultData.vdiffClean(~resultData.type1.blindTrialsClean)<=0 & resultData.type2.confClean_binned_nonblind == i);
    nR_S2_nonBlind(i)          = sum(~resultData.type1.correctClean(~resultData.type1.blindTrialsClean)  & resultData.vdiffClean(~resultData.type1.blindTrialsClean)<=0 & resultData.type2.confClean_binned_nonblind == Nratings+1-i);
    
    nR_S1_blind(i)             = sum(resultData.type1.correctClean(logical(resultData.type1.blindTrialsClean))  &  resultData.vdiffClean(logical(resultData.type1.blindTrialsClean))>0 & resultData.type2.confClean_binned_blind == Nratings+1-i);
    nR_S1_blind(Nratings+i)    = sum(~resultData.type1.correctClean(logical(resultData.type1.blindTrialsClean))  &  resultData.vdiffClean(logical(resultData.type1.blindTrialsClean))>0 & resultData.type2.confClean_binned_blind == i);
    nR_S2_blind(Nratings+i)    = sum(resultData.type1.correctClean(logical(resultData.type1.blindTrialsClean)) & resultData.vdiffClean(logical(resultData.type1.blindTrialsClean))<=0 & resultData.type2.confClean_binned_blind == i);
    nR_S2_blind(i)             = sum(~resultData.type1.correctClean(logical(resultData.type1.blindTrialsClean))  & resultData.vdiffClean(logical(resultData.type1.blindTrialsClean))<=0 & resultData.type2.confClean_binned_blind == Nratings+1-i);
    
    nR_S1_visual(i)            = sum(resultData.visual.type1.correctClean  & resultData.visual.vdiffClean>0 & resultData.visual.type2.confClean_binned == Nratings+1-i);
    nR_S1_visual(Nratings+i)   = sum(~resultData.visual.type1.correctClean & resultData.visual.vdiffClean>0 & resultData.visual.type2.confClean_binned == i);
    nR_S2_visual(Nratings+i)   = sum(resultData.visual.type1.correctClean & resultData.visual.vdiffClean<=0 & resultData.visual.type2.confClean_binned == i);
    nR_S2_visual(i)            = sum(~resultData.visual.type1.correctClean & resultData.visual.vdiffClean<=0 & resultData.visual.type2.confClean_binned == Nratings+1-i);
end

% % To compare the outputs
% 'Visuomotor'
% nR_S1_VM
% nR_S1_nonBlind'
% nR_S2_VM
% nR_S2_nonBlind'
% 'Motor'
% nR_S1_M
% nR_S1_blind'
% nR_S2_M
% nR_S2_blind'
% 'Visual'
% nR_S1_V
% nR_S1_visual'
% nR_S2_V
% nR_S2_visual'


Nratings = 2;

nR_S1_nonBlind_uneq = zeros(2*Nratings,1);
nR_S2_nonBlind_uneq = zeros(2*Nratings,1);
nR_S1_blind_uneq    = zeros(2*Nratings,1);
nR_S2_blind_uneq    = zeros(2*Nratings,1);
nR_S1_visual_uneq   = zeros(2*Nratings,1);
nR_S2_visual_uneq   = zeros(2*Nratings,1);

nR_S1_nonBlind_eq = zeros(2*Nratings,1);
nR_S2_nonBlind_eq = zeros(2*Nratings,1);
nR_S1_blind_eq    = zeros(2*Nratings,1);
nR_S2_blind_eq    = zeros(2*Nratings,1);
nR_S1_visual_eq   = zeros(2*Nratings,1);
nR_S2_visual_eq   = zeros(2*Nratings,1);

% Alternative loop for unequal target hits analysis. Get the bin
% counts for unequal target hits

% Check the number of unequal target hits and equal target hits
cross_visuomotor_uneq = sum(crossing_visuomotor_motor(~resultData.type1.blindTrialsClean));
cross_motor_uneq = sum(crossing_visuomotor_motor(logical(resultData.type1.blindTrialsClean)));
cross_visual_uneq = sum(crossing_visual);

cross_visuomotor_eq = sum(~crossing_visuomotor_motor(~resultData.type1.blindTrialsClean));
cross_motor_eq = sum(~crossing_visuomotor_motor(logical(resultData.type1.blindTrialsClean)));
cross_visual_eq = sum(~crossing_visual);



for i = 1:Nratings
    nR_S1_nonBlind_uneq(i)          = sum(crossing_visuomotor_motor(~resultData.type1.blindTrialsClean) & resultData.type1.correctClean(~resultData.type1.blindTrialsClean)  & resultData.vdiffClean(~resultData.type1.blindTrialsClean)>0 & resultData.type2.confClean_binned_nonblind == Nratings+1-i);
    nR_S1_nonBlind_uneq(Nratings+i) = sum(crossing_visuomotor_motor(~resultData.type1.blindTrialsClean) & ~resultData.type1.correctClean(~resultData.type1.blindTrialsClean)  &  resultData.vdiffClean(~resultData.type1.blindTrialsClean)>0 & resultData.type2.confClean_binned_nonblind == i);
    nR_S2_nonBlind_uneq(Nratings+i) = sum(crossing_visuomotor_motor(~resultData.type1.blindTrialsClean) & resultData.type1.correctClean(~resultData.type1.blindTrialsClean) & resultData.vdiffClean(~resultData.type1.blindTrialsClean)<=0 & resultData.type2.confClean_binned_nonblind == i);
    nR_S2_nonBlind_uneq(i)          = sum(crossing_visuomotor_motor(~resultData.type1.blindTrialsClean) & ~resultData.type1.correctClean(~resultData.type1.blindTrialsClean)  & resultData.vdiffClean(~resultData.type1.blindTrialsClean)<=0 & resultData.type2.confClean_binned_nonblind == Nratings+1-i);
    
    nR_S1_blind_uneq(i)             = sum(crossing_visuomotor_motor(logical(resultData.type1.blindTrialsClean)) & resultData.type1.correctClean(logical(resultData.type1.blindTrialsClean))  &  resultData.vdiffClean(logical(resultData.type1.blindTrialsClean))>0 & resultData.type2.confClean_binned_blind == Nratings+1-i);
    nR_S1_blind_uneq(Nratings+i)    = sum(crossing_visuomotor_motor(logical(resultData.type1.blindTrialsClean)) & ~resultData.type1.correctClean(logical(resultData.type1.blindTrialsClean))  &  resultData.vdiffClean(logical(resultData.type1.blindTrialsClean))>0 & resultData.type2.confClean_binned_blind == i);
    nR_S2_blind_uneq(Nratings+i)    = sum(crossing_visuomotor_motor(logical(resultData.type1.blindTrialsClean)) & resultData.type1.correctClean(logical(resultData.type1.blindTrialsClean)) & resultData.vdiffClean(logical(resultData.type1.blindTrialsClean))<=0 & resultData.type2.confClean_binned_blind == i);
    nR_S2_blind_uneq(i)             = sum(crossing_visuomotor_motor(logical(resultData.type1.blindTrialsClean)) & ~resultData.type1.correctClean(logical(resultData.type1.blindTrialsClean))  & resultData.vdiffClean(logical(resultData.type1.blindTrialsClean))<=0 & resultData.type2.confClean_binned_blind == Nratings+1-i);
    
    nR_S1_visual_uneq(i)            = sum(crossing_visual & resultData.visual.type1.correctClean  & resultData.visual.vdiffClean>0 & resultData.visual.type2.confClean_binned == Nratings+1-i);
    nR_S1_visual_uneq(Nratings+i)   = sum(crossing_visual & ~resultData.visual.type1.correctClean & resultData.visual.vdiffClean>0 & resultData.visual.type2.confClean_binned == i);
    nR_S2_visual_uneq(Nratings+i)   = sum(crossing_visual & resultData.visual.type1.correctClean & resultData.visual.vdiffClean<=0 & resultData.visual.type2.confClean_binned == i);
    nR_S2_visual_uneq(i)            = sum(crossing_visual & ~resultData.visual.type1.correctClean & resultData.visual.vdiffClean<=0 & resultData.visual.type2.confClean_binned == Nratings+1-i);
end

%... and for the equal targets hits
for i = 1:Nratings
    nR_S1_nonBlind_eq(i)          = sum(~crossing_visuomotor_motor(~resultData.type1.blindTrialsClean) & resultData.type1.correctClean(~resultData.type1.blindTrialsClean)  & resultData.vdiffClean(~resultData.type1.blindTrialsClean)>0 & resultData.type2.confClean_binned_nonblind == Nratings+1-i);
    nR_S1_nonBlind_eq(Nratings+i) = sum(~crossing_visuomotor_motor(~resultData.type1.blindTrialsClean) & ~resultData.type1.correctClean(~resultData.type1.blindTrialsClean)  &  resultData.vdiffClean(~resultData.type1.blindTrialsClean)>0 & resultData.type2.confClean_binned_nonblind == i);
    nR_S2_nonBlind_eq(Nratings+i) = sum(~crossing_visuomotor_motor(~resultData.type1.blindTrialsClean) & resultData.type1.correctClean(~resultData.type1.blindTrialsClean) & resultData.vdiffClean(~resultData.type1.blindTrialsClean)<=0 & resultData.type2.confClean_binned_nonblind == i);
    nR_S2_nonBlind_eq(i)          = sum(~crossing_visuomotor_motor(~resultData.type1.blindTrialsClean) & ~resultData.type1.correctClean(~resultData.type1.blindTrialsClean)  & resultData.vdiffClean(~resultData.type1.blindTrialsClean)<=0 & resultData.type2.confClean_binned_nonblind == Nratings+1-i);
    
    nR_S1_blind_eq(i)             = sum(~crossing_visuomotor_motor(logical(resultData.type1.blindTrialsClean)) & resultData.type1.correctClean(logical(resultData.type1.blindTrialsClean))  &  resultData.vdiffClean(logical(resultData.type1.blindTrialsClean))>0 & resultData.type2.confClean_binned_blind == Nratings+1-i);
    nR_S1_blind_eq(Nratings+i)    = sum(~crossing_visuomotor_motor(logical(resultData.type1.blindTrialsClean)) & ~resultData.type1.correctClean(logical(resultData.type1.blindTrialsClean))  &  resultData.vdiffClean(logical(resultData.type1.blindTrialsClean))>0 & resultData.type2.confClean_binned_blind == i);
    nR_S2_blind_eq(Nratings+i)    = sum(~crossing_visuomotor_motor(logical(resultData.type1.blindTrialsClean)) & resultData.type1.correctClean(logical(resultData.type1.blindTrialsClean)) & resultData.vdiffClean(logical(resultData.type1.blindTrialsClean))<=0 & resultData.type2.confClean_binned_blind == i);
    nR_S2_blind_eq(i)             = sum(~crossing_visuomotor_motor(logical(resultData.type1.blindTrialsClean)) & ~resultData.type1.correctClean(logical(resultData.type1.blindTrialsClean))  & resultData.vdiffClean(logical(resultData.type1.blindTrialsClean))<=0 & resultData.type2.confClean_binned_blind == Nratings+1-i);
    
    nR_S1_visual_eq(i)            = sum(~crossing_visual & resultData.visual.type1.correctClean  & resultData.visual.vdiffClean>0 & resultData.visual.type2.confClean_binned == Nratings+1-i);
    nR_S1_visual_eq(Nratings+i)   = sum(~crossing_visual & ~resultData.visual.type1.correctClean & resultData.visual.vdiffClean>0 & resultData.visual.type2.confClean_binned == i);
    nR_S2_visual_eq(Nratings+i)   = sum(~crossing_visual & resultData.visual.type1.correctClean & resultData.visual.vdiffClean<=0 & resultData.visual.type2.confClean_binned == i);
    nR_S2_visual_eq(i)            = sum(~crossing_visual & ~resultData.visual.type1.correctClean & resultData.visual.vdiffClean<=0 & resultData.visual.type2.confClean_binned == Nratings+1-i);
end

% Compute d' and meta-d's
SSEfit_nonBlind_uneq = type2_SDT_SSE(nR_S1_nonBlind_uneq, nR_S2_nonBlind_uneq); %requires optimization toolbox
SSEfit_blind_uneq    = type2_SDT_SSE(nR_S1_blind_uneq, nR_S2_blind_uneq);
SSEfit_visual_uneq   = type2_SDT_SSE(nR_S1_visual_uneq, nR_S2_visual_uneq);

SSEfit_nonBlind_eq = type2_SDT_SSE(nR_S1_nonBlind_eq, nR_S2_nonBlind_eq); %requires optimization toolbox
SSEfit_blind_eq    = type2_SDT_SSE(nR_S1_blind_eq, nR_S2_blind_eq);
SSEfit_visual_eq   = type2_SDT_SSE(nR_S1_visual_eq, nR_S2_visual_eq);


% Suggested adjustment for zero values in nRs
% adj_f = 1/length(nR_S1);
% nR_S1_adj = nR_S1 + adj_f;
% nR_S2_adj = nR_S2 + adj_f;

adj_f = 1/length(nR_S1_nonBlind);

nR_S1_nonBlind_adj = nR_S1_nonBlind + adj_f;
nR_S2_nonBlind_adj = nR_S2_nonBlind + adj_f;

nR_S1_blind_adj    = nR_S1_blind + adj_f;
nR_S2_blind_adj    = nR_S2_blind + adj_f;

nR_S1_visual_adj   = nR_S1_visual + adj_f;
nR_S2_visual_adj   = nR_S2_visual + adj_f;

% Adjustment for unequal target hits analysis
adj_f = 1/length(nR_S1_nonBlind_uneq);

nR_S1_blind_uneq_adj    = nR_S1_blind_uneq + adj_f;
nR_S2_blind_uneq_adj    = nR_S2_blind_uneq + adj_f;


nR_S1_blind_eq_adj    = nR_S1_blind_eq + adj_f;
nR_S2_blind_eq_adj    = nR_S2_blind_eq + adj_f;


%if all(nR_S1_nonBlind) && all(nR_S2_nonBlind) && all(nR_S1_blind) && all(nR_S2_blind)
SSEfit_nonBlind = type2_SDT_SSE(nR_S1_nonBlind, nR_S2_nonBlind); %requires optimization toolbox
SSEfit_blind    = type2_SDT_SSE(nR_S1_blind, nR_S2_blind);
SSEfit_visual   = type2_SDT_SSE(nR_S1_visual, nR_S2_visual);

% Meta-d' for unequal/equal target hits for motor condition 

SSEfit_blind_uneq    = type2_SDT_SSE(nR_S1_blind_uneq_adj, nR_S2_blind_uneq_adj);
SSEfit_blind_eq    = type2_SDT_SSE(nR_S1_blind_eq_adj, nR_S2_blind_eq_adj);

%Plot if it worked
if make_plots == 1 || make_plots == 2
    figure(figHandle)
    subplot(subplotSet.rows,subplotSet.cols,subplotSet.position); hold on
    title('Meta-d analysis')
    b = bar([SSEfit_nonBlind.meta_da, SSEfit_blind.meta_da,SSEfit_visual.meta_da;
        SSEfit_nonBlind.da,       SSEfit_blind.da,SSEfit_visual.da;
        SSEfit_nonBlind.M_ratio,  SSEfit_blind.M_ratio,SSEfit_visual.M_ratio;
        SSEfit_nonBlind.M_diff,    SSEfit_blind.M_diff,SSEfit_visual.M_diff]);
    b(1).FaceColor = 'r';
    b(2).FaceColor = 'b';
    b(3).FaceColor = 'g';
    
    set(gca, 'XTickLabel', {'Meta-d', 'd', 'Mratio', 'Mdiff'}, 'XTick', 1:4)
end
% else
%     SSEfit_nonBlind = NaN;
% end


end


function [auroc2_nonBlind, auroc2_blind, auroc2_visual,resultData] = Aroc(figHandle, subplotSet,resultData, make_plots)

%global resultData

if make_plots == 1 || make_plots == 2
    figure(figHandle)
else
    figHandle = figure('visible','off');
end

Nratings = 6; %define how many confidence levels we want
%resultData.type2.confClean_binned = discretize(resultData.type2.confClean, Nratings);

% non-blind trials (aka VISUOMOTOR)
confvector = resultData.type2.confClean(~resultData.type1.blindTrialsClean);
% resultData.type2.confClean_binned = discretize(confvector,min(confvector):range(confvector)/Nratings:max(confvector));
resultData.type2.confClean_binned_nonblind = quantileranks(confvector, Nratings);
correct = resultData.type1.correctClean(~resultData.type1.blindTrialsClean);

conf = resultData.type2.confClean_binned_nonblind;
[auroc2_nonBlind, cum_FA2_nonBlind, cum_H2_nonBlind] = type2roc(correct, conf, Nratings);

% blind trials (aka MOTOR)
confvector = resultData.type2.confClean(logical(resultData.type1.blindTrialsClean));
resultData.type2.confClean_binned_blind = quantileranks(confvector, Nratings);
correct = resultData.type1.correctClean(logical(resultData.type1.blindTrialsClean));
conf = resultData.type2.confClean_binned_blind;
[auroc2_blind, cum_FA2_blind, cum_H2_blind] = type2roc(correct, conf, Nratings);

% visual 
%resultData.visual.type2.confClean_binned = discretize(resultData.visual.type2.confClean, Nratings);
confvector = resultData.visual.type2.confClean;
%resultData.visual.type2.confClean_binned = discretize(confvector, min(confvector):range(confvector)/Nratings:max(confvector));
resultData.visual.type2.confClean_binned = quantileranks(confvector, Nratings);

correct = resultData.visual.type1.correctClean;
conf = resultData.visual.type2.confClean_binned;
[auroc2_visual, cum_FA2_visual, cum_H2_visual] = type2roc(correct, conf, Nratings);

if make_plots == 1 || make_plots == 2
    figure(figHandle)
    subplot(subplotSet.rows,subplotSet.cols,subplotSet.position); hold on
    color = 'r'; % VISUOMOTOR - RED
    plot(cum_FA2_nonBlind,cum_H2_nonBlind, color);
    plot(cum_FA2_nonBlind,cum_H2_nonBlind,[color, 'o']);
    color = 'b'; % MOTOR - BLUE
    plot(cum_FA2_blind,cum_H2_blind, color);
    plot(cum_FA2_blind,cum_H2_blind,[color, 'o']);
    color = 'g'; % VISUAL - GREEN
    plot(cum_FA2_visual,cum_H2_visual, color);
    plot(cum_FA2_visual,cum_H2_visual,[color, 'o']);
    axis square
    set(gca,'YLim',[0 1]);
    set(gca,'XLim',[0 1]);
    title ('type 2 ROC')
    xlabel('FA2'); ylabel('HITS2')
end

end

function [nR_S1, nR_S2] = trials2counts(stimID, response, rating, nRatings, padCells, padAmount)

% [nR_S1, nR_S2] = trials2counts(stimID, response, rating, nRatings, padCells, padAmount)
%
% Given data from an experiment where an observer discriminates between two
% stimulus alternatives on every trial and provides confidence ratings,
% converts trial by trial experimental information for N trials into response 
% counts.
%
% INPUTS
% stimID:   1xN vector. stimID(i) = 0 --> stimulus on i'th trial was S1.
%                       stimID(i) = 1 --> stimulus on i'th trial was S2.
%
% response: 1xN vector. response(i) = 0 --> response on i'th trial was "S1".
%                       response(i) = 1 --> response on i'th trial was "S2".
%
% rating:   1xN vector. rating(i) = X --> rating on i'th trial was X.
%                       X must be in the range 1 <= X <= nRatings.
%
% N.B. all trials where stimID is not 0 or 1, response is not 0 or 1, or
% rating is not in the range [1, nRatings], are omitted from the response
% count.
%
% nRatings: total # of available subjective ratings available for the
%           subject. e.g. if subject can rate confidence on a scale of 1-4,
%           then nRatings = 4
%
% optional inputs
%
% padCells: if set to 1, each response count in the output has the value of
%           padAmount added to it. Padding cells is desirable if trial counts 
%           of 0 interfere with model fitting.
%           if set to 0, trial counts are not manipulated and 0s may be
%           present in the response count output.
%           default value for padCells is 0.
%
% padAmount: the value to add to each response count if padCells is set to 1.
%            default value is 1/(2*nRatings)
%
%
% OUTPUTS
% nR_S1, nR_S2
% these are vectors containing the total number of responses in
% each response category, conditional on presentation of S1 and S2.
%
% e.g. if nR_S1 = [100 50 20 10 5 1], then when stimulus S1 was
% presented, the subject had the following response counts:
% responded S1, rating=3 : 100 times
% responded S1, rating=2 : 50 times
% responded S1, rating=1 : 20 times
% responded S2, rating=1 : 10 times
% responded S2, rating=2 : 5 times
% responded S2, rating=3 : 1 time
%
% The ordering of response / rating counts for S2 should be the same as it
% is for S1. e.g. if nR_S2 = [3 7 8 12 27 89], then when stimulus S2 was
% presented, the subject had the following response counts:
% responded S1, rating=3 : 3 times
% responded S1, rating=2 : 7 times
% responded S1, rating=1 : 8 times
% responded S2, rating=1 : 12 times
% responded S2, rating=2 : 27 times
% responded S2, rating=3 : 89 times


%% sort inputs

% check for valid inputs
if ~( length(stimID) == length(response) && length(stimID) == length(rating) )
    error('stimID, response, and rating input vectors must have the same lengths')
end

% filter bad trials
f = (stimID == 0 | stimID == 1) & (response == 0 | response == 1) & (rating >=1 & rating <= nRatings);
stimID   = stimID(f);
response = response(f);
rating   = rating(f);


% set input defaults
if ~exist('padCells','var') || isempty(padCells)
    padCells = 0;
end

if ~exist('padAmount','var') || isempty(padAmount)
    padAmount = 1 / (2*nRatings);
end


%% compute response counts

nR_S1 = [];
nR_S2 = [];

% S1 responses
for r = nRatings : -1 : 1 % r iterates over nRatings till 1, with increments of -1 (first iteration when nRatings = 6: 6, second: 5 and so on)
    nR_S1(end+1) = sum(stimID==0 & response==0 & rating==r); % first iteration: first entry of nR_S1 is HIT with highest rating (and so on)
    nR_S2(end+1) = sum(stimID==1 & response==0 & rating==r); % first iteration: first entry of nR_S2 is FA with highest rating 
end

% S2 responses
for r = 1 : nRatings
    nR_S1(end+1) = sum(stimID==0 & response==1 & rating==r); % S1 FA
    nR_S2(end+1) = sum(stimID==1 & response==1 & rating==r); % S2 HIT
end


% pad response counts to avoid zeros
if padCells
    nR_S1 = nR_S1 + padAmount;
    nR_S2 = nR_S2 + padAmount;
end
end
 