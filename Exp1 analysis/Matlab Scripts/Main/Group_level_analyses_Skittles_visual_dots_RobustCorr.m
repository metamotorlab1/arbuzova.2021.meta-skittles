clear all 
close all

%% Before doing this analysis, execute run_correlateTasks. 
% This will save a .mat file that will be loaded here for analyses (so you only need to run it ones and you can do the analyses on the .mat file). 
% saveDir must be the same path you used to save the .mat file in
% run_correlateTasks

%% User

user = 'polina';

switch user
    case {'caroline'}
        saveDir = '/Users/Caroline/Dropbox/PhDs/Caroline/Analysis_skittles_behavioral/data_matrices';
    case {'polina'}
        saveDir = '/Users/polinaarbuzova/Dropbox/PhDs/Polina/SkittlesResults';
end


%% compute correlations and t-tests

cd(saveDir);

load('metaRatios.mat')

% plot correlation matrix
% figure;
% pairs(metaRatios, {'Skittles/NonBlind' 'Skittles/Blind' 'Skittles/Visual' 'Dots/Visual'})

'Skittles visual / Dots visual'

% Data preparation: exclude people with NaN either in visual
% skittles or visual dots condition --> first find indices (same for any other pair of conditions)

indeces_NaN_visual_visual = find((~isnan(Skittles_visual)) & (~isnan(Dots_visual)));
Skittles_visual_clean = Skittles_visual(indeces_NaN_visual_visual);
Dots_visual_clean = Dots_visual(indeces_NaN_visual_visual);

% prepare csv files for JASP

% visual_visual = [Skittles_visual_clean Dots_visual_clean];
% xlswrite('meta_ratios_visual_visual_40p',visual_visual);

% normal correlation 
[r, p] = corr(Skittles_visual_clean, Dots_visual_clean)

% robust correlation (toolbox) visual Skittles/ visual Dots
correlation_results_visual = robust_correlation(Skittles_visual_clean, Dots_visual_clean);
[r,t,h,outid,hboot,CI] =  skipped_correlation_changedPlots(Skittles_visual_clean, Dots_visual_clean);


% t-test
% first test if normally distributed
h_visual_skittles_normal = lillietest(Skittles_visual_clean)
h_visual_dots_normal = lillietest(Dots_visual_clean)

[h, p, ci, stats] = ttest(Skittles_visual_clean, Dots_visual_clean)


'visuomotor / Skittles visual'

indeces_NaN_visuomotor_Skittles_visual = find((~isnan(Skittles_visuomotor)) & (~isnan(Skittles_visual)));

Skittles_visuomotor_clean = Skittles_visuomotor(indeces_NaN_visuomotor_Skittles_visual);
Skittles_visual_clean = Skittles_visual(indeces_NaN_visuomotor_Skittles_visual);


% normal correlation 
[r, p] = corr(Skittles_visuomotor_clean, Skittles_visual_clean)

% robust correlation (toolbox) visuomotor Skittles/ visual Skittles
correlation_results_visuomotor_visual = robust_correlation(Skittles_visuomotor_clean, Skittles_visual_clean);

% t-test
% first test if normally distributed
h_visuomotor_normal = lillietest(Skittles_visuomotor_clean)
h_visual_skittles_normal = lillietest(Skittles_visual_clean)

[h, p, ci, stats] = ttest(Skittles_visuomotor_clean, Skittles_visual_clean)


'visuomotor / motor'

indeces_NaN_visuomotor_motor = find((~isnan(Skittles_visuomotor)) & (~isnan(Skittles_motor)));

Skittles_visuomotor_clean = Skittles_visuomotor(indeces_NaN_visuomotor_motor);
Skittles_motor_clean = Skittles_motor(indeces_NaN_visuomotor_motor);


% normal correlation 
[r, p] = corr(Skittles_visuomotor_clean, Skittles_motor_clean)

% robust correlation (toolbox) visuomotor Skittles/ motor Skittles
correlation_results_visuomotor_motor = robust_correlation(Skittles_visuomotor_clean, Skittles_motor_clean);

% t-test
% first test if normally distributed
h_visuomotor_normal = lillietest(Skittles_visuomotor_clean)
h_motor_normal = lillietest(Skittles_motor_clean)

[h, p, ci, stats] = ttest(Skittles_visuomotor_clean, Skittles_motor_clean)


'visual Skittles / motor'

indeces_NaN_visual_Skittles_motor = find((~isnan(Skittles_visual)) & (~isnan(Skittles_motor)));

Skittles_visual_clean = Skittles_visual(indeces_NaN_visual_Skittles_motor);
Skittles_motor_clean = Skittles_motor(indeces_NaN_visual_Skittles_motor);


% normal correlation 
[r, p] = corr(Skittles_visual_clean, Skittles_motor_clean)

% robust correlation (toolbox) visual Skittles/ motor Skittles
correlation_results_visual_Skittles_motor = robust_correlation(Skittles_visual_clean, Skittles_motor_clean);

% t-test
% first test if normally distributed
h_visual_normal = lillietest(Skittles_visual_clean)
h_motor_normal = lillietest(Skittles_motor_clean)

[h, p, ci, stats] = ttest(Skittles_visual_clean, Skittles_motor_clean)


% correlation of visuomotor/visual dots --> necessary to be able to perform
% a comparison of the correlations skittles visual/dots visual and
% visuomotor/visual skittles because they are dependent and there is a
% function in R which deals with that. This function (paired.r) needs r of
% all three combinationsso also the combination visuomotor/visual dots

'visuomotor / visual dots'

indeces_NaN_visual_dots_visuomotor = find((~isnan(Dots_visual)) & (~isnan(Skittles_visuomotor)));

Dots_visual_clean = Dots_visual(indeces_NaN_visual_dots_visuomotor);
Skittles_visuomotor_clean = Skittles_visuomotor(indeces_NaN_visual_dots_visuomotor);

% normal correlation 
[r, p] = corr(Dots_visual_clean, Skittles_visuomotor_clean)

% robust correlation (toolbox) visual Skittles/ motor Skittles
correlation_results_visual_Skittles_motor = robust_correlation(Dots_visual_clean, Skittles_visuomotor_clean);

% t-test
% first test if normally distributed
h_visuomotor_normal = lillietest(Skittles_visuomotor_clean)
h_visual_dots_normal = lillietest(Dots_visual_clean)

[h, p, ci, stats] = ttest(Dots_visual_clean, Skittles_visuomotor_clean)


'motor / visual dots'

indeces_NaN_visual_dots_motor = find((~isnan(Dots_visual)) & (~isnan(Skittles_motor)));

Dots_visual_clean = Dots_visual(indeces_NaN_visual_dots_motor);
Skittles_motor_clean = Skittles_motor(indeces_NaN_visual_dots_motor);

% normal correlation 
[r, p] = corr(Dots_visual_clean, Skittles_motor_clean)

% robust correlation (toolbox) visual dots/ motor Skittles
correlation_results_visual_Skittles_motor = robust_correlation(Dots_visual_clean, Skittles_motor_clean);

% t-test
% first test if normally distributed
h_motor_normal = lillietest(Skittles_motor_clean)
h_visual_dots_normal = lillietest(Dots_visual_clean)

[h, p, ci, stats] = ttest(Dots_visual_clean, Skittles_motor_clean)



