close all
clear all

% user = 'elisa'; %or 'lukas' or whatever you like
user = 'polina';
% user = 'caroline';

switch user
    case {'elisa'}
        addpath('~/git/Metasensory_Experiment/analysis_matlab')
    case {'polina'}
        saveDir = '/Users/polinaarbuzova/Dropbox/PhDs/Polina/SkittlesResults';
        addpath(genpath('/Users/polinaarbuzova/Dropbox/PhDs/Polina/Skittles_Visual'));
        addpath(genpath('/Users/polinaarbuzova/Dropbox/PhDs/Polina/SkittlesMatlabLJ/SkittlesMatlab'));
    case {'caroline'}
        addpath('/Users/Caroline/Dropbox/PhDs/Caroline/Skittles_Visual/Metasensory_dots/analysis_matlab')
        saveDir = '/Users/Caroline/Dropbox/PhDs/Caroline/Analysis_skittles_behavioral/data_matrices';
end

%% Exclusion control

% Decide if you want to exclude people with 0s and/or 1s in HR
% and/or FAR 1/2 or not. 0 = don't exclude people; 1 = exclude people .
% Excluding is performed similar to ?Seth, Barrett, Schwartzman and Bor
% (2018). Exclusion is performed for people with FAR or HR of 0 or 1 in either type 1 or type 2 tasks when dividing the data into 2 bins. 
% If FAR1/HR1 = 0 or 1, estimation of d' failes. 
% If FAR2/HR2 = 0 or 1, meta-d' is not reliable since the estimation cannot be done properly on these extreme values.
% You can also change the boundaries for exclusion of bad/too good
% performance here (lower and upper boundary, given in % correct)

exclusion = 0;

lower_boundary = 60;
upper_boundary = 80;

%% Adjustment control (if exclusion = 0)

% If you do not want to exclude people (so exclusion = 0), you can decide to
% add an adjustemnt factor (adj_f = 1/length(nR_S1)) to ALL data (type 1 and type 2) according to 
% 
% Hautus, M. J. (1995). Corrections for extreme proportions and their biasing 
%     effects on estimated values of d'. Behavior Research Methods, Instruments, 
%     & Computers, 27, 46-51.
%
% This accounts, up to a certain degree, for estimation issues with cells conatining 0s.
% However, in extreme cases results may still be unreliabe.
% Normally, it only makes sense to perform either exclusion OR adjustment.
% 0 = don't adjust; 1 = adjust 

adjustment = 0;

%% Plotting control (for individual plots)
% 0 - no plots, 1 - make only individual performance plots, 2 - make all individual plots (including landscape plots and performance plots in Skittles task) 

make_plots = 0;

%% Participant info


subjectNames = {'020SM','030GY', '030VS', '050FJ', '050IN', '060UA', '070EE', ...
   '080MH', '090MK', '110SR', '130MK', '140HN', '150RB', ...
   '170SA', '180CH', '181GJ', '180IG', '210KC', '211PM', ...
   '240AM', '240ME',  '260AK', '260EA', '270FS', '290SD',...
   '290US', '300BA', '220PH', '190SZ', '071SM', '130BH', '300MR', '221II', '060HD', '310AU', '050GN', '221NV', '280EB', '081MC', '130BY'}; 

% subjectNames = {'211PM'}

% Subject 060RK - to be excluded 

%% Analysis skittles/visual dots
skittlesResults = step1_describeSkittles_blindNonBlind_hmeta(subjectNames, user, make_plots);%, exclusion, adjustment);

visualMetaResults = step1_descriptionAndMetacog(subjectNames, user, make_plots, exclusion, adjustment);

%% Matrix of values applying strict exclusion of people with extreme values in FAR or HR
% Creating a matrix containing the metaRatios of the visual dots and the skittles
% experiments. 
% There is NaNs in cases where FAR2 and HR2 was 0 or 1 or when d_1 could not be estimated (d_1 = Inf, this happens in step1_describeSkittles_blindNonBlind) or when performance was
% below 60 % or above 80 %.

% extract the relevant variables for meta_ratios with 'normal' meta-d'
% estimation
Skittles_visuomotor = [skittlesResults.SSEfit_nonBlind.M_ratio]';
Skittles_motor = [skittlesResults.SSEfit_blind.M_ratio]'; 
Skittles_visual = [skittlesResults.SSEfit_visual.M_ratio]'; 
Dots_visual = [visualMetaResults.SSEfit.M_ratio]';

% exclude too low or too high performance people. If you want, change lower and/or upper boundary for exlusion at the top of this code 

for i = 1:length(subjectNames)
    if skittlesResults.percentCorrect(i,1) < lower_boundary || skittlesResults.percentCorrect(i,1) > upper_boundary
        Skittles_visuomotor(i) = NaN;
        fprintf('performance outside boundaries in visuomotor condition for subject: %s\n', subjectNames{i});    
    end

    if skittlesResults.percentCorrect(i,2) < lower_boundary || skittlesResults.percentCorrect(i,2) > upper_boundary
        Skittles_motor(i) = NaN;
        fprintf('performance outside boundaries in motor condition for subject: %s\n', subjectNames{i}); 
    end

    if skittlesResults.percentCorrect(i,3) < lower_boundary || skittlesResults.percentCorrect(i,3) > upper_boundary
        Skittles_visual(i) = NaN;
        fprintf('performance outside boundaries in visual skittles condition for subject: %s\n', subjectNames{i}); 
    end

    if visualMetaResults.percentCorrect(i,1) < lower_boundary || visualMetaResults.percentCorrect(i,1) > upper_boundary
        Dots_visual(i) = NaN;
        fprintf('performance outside boundaries in visual dots condition for subject: %s\n', subjectNames{i}); 
    end
end


% put everything in one matrix and save .mat

metaRatios = [Skittles_visuomotor  Skittles_motor  Skittles_visual  Dots_visual]; 

cd(saveDir);
save metaRatios.mat

% for visual inspection (e.g. finding out for which particpant we have
% missing values) use the following matrix which includes subjectNames as a
% column (this cannot be used for further analyses):

metaRatios_with_subjectID = [string(subjectNames)' Skittles_visuomotor  Skittles_motor  Skittles_visual  Dots_visual];

cd(saveDir);
save('skittlesResults_normal_all.mat');








