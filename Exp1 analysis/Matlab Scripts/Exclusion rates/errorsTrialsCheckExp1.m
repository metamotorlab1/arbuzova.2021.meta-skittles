% (c) Polina Arbuzova, December 2019.
% This script checks the proportion of error (skipped) trials in Experiment 1
% (Skittles Original)
% 
clear all;
%% User

user = 'polina';

switch user
    case {'polina'}
        addpath(genpath('/Users/polinaarbuzova/Dropbox/PhDs/Polina/MatLab Toolboxes/Corr_toolbox_v2'));
        addpath(genpath('/Users/polinaarbuzova/Dropbox/PhDs/Polina/MatLab Toolboxes/jags-3.4.0'));
        saveDir = '/Users/polinaarbuzova/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/Analysis';
        userpath=['~/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/'];
        resultDir=['/Users/polinaarbuzova/Documents/SkittlesOriginalData'];
end
addpath(strcat(userpath, 'Analysis/fx'));
addpath(strcat(userpath, 'BasicVariables'));

cd(saveDir);

subjectNames = {'020SM','030GY', '030VS', '050FJ', '050IN', '060UA', '070EE', ...
   '080MH', '090MK', '110SRb', '130MK', '140HN', '150RB', ...
   '170SA', '180CH', '181GJ', '180IG', '210KC', '211PM', ...
   '240AM', '240ME',  '260AK', '260EA', '270FS', '290SD',...
   '290US', '300BA', '220PH', '190SZ', '071SM', '130BH', '300MR', '221II', '060HD', '310AU', '050GN', '221NV', '280EB', '081MC', '130BY'}; 


% Experiment 1

for subj = 1:length(subjectNames)
    
    allBlocks = load([resultDir filesep subjectNames{subj} filesep 'SkittlesResult_' subjectNames{subj} '.mat']);
    

subjectNames{subj} % this is just printing the current subjectName to check how far we are in the analyses
    
    % fill the data into resultData
    resultData =   get_Data(allBlocks);
    
    error_vm(subj) = sum(resultData.type1.errorTrials(resultData.type1.blindTrials==1))/length(resultData.type1.errorTrials(resultData.type1.blindTrials==1));
    error_m(subj) = sum(resultData.type1.errorTrials(resultData.type1.blindTrials==0))/length(resultData.type1.errorTrials(resultData.type1.blindTrials==0));
    error_v(subj) = sum(resultData.visual.type1.errorTrials)/length(resultData.visual.type1.errorTrials);
    error_all(subj) = (sum(resultData.type1.errorTrials)+sum(resultData.visual.type1.errorTrials))/(length(resultData.type1.errorTrials+length(resultData.visual.type1.errorTrials)));
end

% Get descriptives
% ANGLES
all_errors = error_all; % don't ask...
% number of people with zero error trials
error.null = sum(all_errors == 0)
error.min_nonzero = min(all_errors(all_errors~=0))
error.max = max(all_errors)
error.mean_nonzero = mean(all_errors(all_errors~=0))
error.mean_wzero = mean(all_errors)
error.meadian_nonzero = median(all_errors(all_errors~=0))
error.median_wzero = median(all_errors)

save('errorTrialsExp1.mat', 'error', '-v7.3')
