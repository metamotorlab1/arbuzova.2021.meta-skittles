% (c) Polina Arbuzova, December 2019.
% This script checks the proportion of trials within the RT expected range
% in Experiment 1 and rate of post hits
% (Skittles Original)
% 
clear all;
%% User

user = 'polina';

switch user
    case {'polina'}
        addpath(genpath('/Users/polinaarbuzova/Dropbox/PhDs/Polina/MatLab Toolboxes/Corr_toolbox_v2'));
        addpath(genpath('/Users/polinaarbuzova/Dropbox/PhDs/Polina/MatLab Toolboxes/jags-3.4.0'));
        saveDir = '/Users/polinaarbuzova/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/Analysis';
        userpath=['~/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/'];
        resultDir=['/Users/polinaarbuzova/Documents/SkittlesOriginalData'];
end
addpath(strcat(userpath, 'Analysis/fx'));
addpath(strcat(userpath, 'BasicVariables'));

cd(saveDir);

subjectNames = {'020SM','030GY', '030VS', '050FJ', '050IN', '060UA', '070EE', ...
   '080MH', '090MK', '110SRb', '130MK', '140HN', '150RB', ...
   '170SA', '180CH', '181GJ', '180IG', '210KC', '211PM', ...
   '240AM', '240ME',  '260AK', '260EA', '270FS', '290SD',...
   '290US', '300BA', '220PH', '190SZ', '071SM', '130BH', '300MR', '221II', '060HD', '310AU', '050GN', '221NV', '280EB', '081MC', '130BY'}; 

lower_threshold = 0.3;
higher_threshold = 8;


% Experiment 1

for subj = 1:length(subjectNames)
    
    allBlocks = load([resultDir filesep subjectNames{subj} filesep 'SkittlesResult_' subjectNames{subj} '.mat']);
    

subjectNames{subj} % this is just printing the current subjectName to check how far we are in the analyses
    
    % fill the data into resultData
    resultData =   get_Data(allBlocks);
    
    RTerror_vm(subj) = sum((resultData.type1.RT(resultData.type1.blindTrials==1)<lower_threshold)|(resultData.type1.RT(resultData.type1.blindTrials==1)>higher_threshold));
    RTerror_m(subj) = sum((resultData.type1.RT(resultData.type1.blindTrials==0)<lower_threshold)|(resultData.type1.RT(resultData.type1.blindTrials==0)>higher_threshold));
    RTerror_v(subj) = sum((resultData.visual.type1.RT<lower_threshold)|(resultData.visual.type1.RT>higher_threshold));
    RTerror_all(subj) = RTerror_vm(subj) + RTerror_m(subj) + RTerror_v(subj);
    
    % Percentage
    perc_RTerror_all(subj) = RTerror_all(subj)/(length(resultData.type1.RT)+length(resultData.visual.type1.RT));
    
    %%% Posthits
    
    posthit_vm(subj) = sum(resultData.posthit(resultData.type1.blindTrials==0))/length(resultData.posthit(resultData.type1.blindTrials==0));
    posthit_v(subj) = sum(resultData.visual.posthit)/length(resultData.visual.posthit);
    
    
   
end

% % number of people with zero error trials
RTerror.null = sum(RTerror_all == 0);
RTerror.min = min(RTerror_all(RTerror_all~=0))
RTerror.max = max(RTerror_all(RTerror_all~=0))
RTerror.median_nonzero = median(RTerror_all(RTerror_all~=0))

% % percentage of such trials
RTerror.perc_min = min(perc_RTerror_all(perc_RTerror_all~=0))
RTerror.perc_max = max(perc_RTerror_all(perc_RTerror_all~=0))
RTerror.perc_median_nonzero = median(perc_RTerror_all(RTerror_all~=0))

%%% Poshits

posthit.vm.null = sum(posthit_vm == 0);
posthit.vm.min = min(posthit_vm(posthit_vm~=0))
posthit.vm.max = max(posthit_vm(posthit_vm~=0))
posthit.vm.median_nonzero = median(posthit_vm(posthit_vm~=0))

posthit.v.null = sum(posthit_v == 0);
posthit.v.min = min(posthit_v(posthit_v~=0))
posthit.v.max = max(posthit_v(posthit_v~=0))
posthit.v.median_nonzero = median(posthit_v(posthit_v~=0))

save('RTerrorPosthitTrialsExp1.mat', 'posthit', 'RTerror', '-v7.3')