% (c) Polina Arbuzova, Novemeber 2019.
% This script is to extract the TRAINING data for plotting staircases in Skittles Original (Trajectories 1). 

% 
clear all;
%% User

user = 'polina';

switch user
    case {'polina'}
        addpath(genpath('/Users/polinaarbuzova/Dropbox/PhDs/Polina/MatLab Toolboxes/Corr_toolbox_v2'));
        addpath(genpath('/Users/polinaarbuzova/Dropbox/PhDs/Polina/MatLab Toolboxes/jags-3.4.0'));
        saveDir = '/Users/polinaarbuzova/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/Analysis';
        userpath=['~/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/'];
        resultDir=['~/Dropbox/Data/Skittles_Visual'];
end
addpath(strcat(userpath, 'Analysis/fx'));
addpath(strcat(userpath, 'BasicVariables'));

cd(saveDir);

subjectNames = {'020SM','030GY', '030VS', '050FJ', '050IN', '060UA', '070EE', ...
   '080MH', '090MK', '130MK', '140HN', '150RB', ...
   '170SA', '180CH', '181GJ', '180IG', '210KC', '211PM', ...
   '240AM', '240ME',  '260AK', '260EA', '270FS', '290SD',...
   '290US', '300BA', '220PH', '190SZ', '071SM', '130BH', '300MR', '221II', ...
   '060HD', '310AU', '050GN', '221NV', '280EB', '081MC', '130BY'}; 

% '110SR' was excluded as no staircase was found

% Experiment 1

for subj = 1:length(subjectNames)
    
    allBlocks = load([resultDir filesep subjectNames{subj} filesep 'SkittlesTrainingResult_' subjectNames{subj} ]);
    
    subjectNames{subj} % this is just printing the current subjectName to check how far we are in the analyses
    
    % fill the data into resultData
    resultData =   get_Data_Training(allBlocks);
    % For mean difficulty 
    vdiff_vm{subj} = abs(resultData.type1.vdiff(resultData.type1.whichStair==1));
    vdiff_m{subj} = abs(resultData.type1.vdiff(resultData.type1.whichStair==2));
    
end
save('staircasesExp1Training.mat', 'vdiff_vm', '-v7.3')